# TODO

## Abschluss Java 2

* Für einige neu hinzugekommene Grundlagen fehlt noch das "commit", folgt heute
    * Authentifizierung
    * Pagination
* REST-Controller
    * Authentifizierung nutzen, sodass immer ein angemeldeter Nutzer für die Methoden zu Verfügung steht
    * Fehlende Methoden ergänzen
    * Pagination einbauen (Größe & Position des Ergebnisfensters)
    * Einfache HTTP-Tests
        * für jeden REST-Controller
        * HTTP-Requests in Reihenfolge des Anklickens sortieren
* Java-Doc prüfen & ergänzen
    * einheitliches Bild
    * mühselige Sache
    * kann ich übernehmen (das und die abschließende Durchsicht des Codes)
* Dokumentation
    * Die alten Inhalte können bleiben, müssen ergänzen werden
    * Welche Diagramme können neu aufgenommen werden (vor Allem für die neuen Abschnitte)
    * Bewertungkriterien: Java-2-Skript 1, Folien 12 bis 26
* Optional:
    * Werkzeug zur Code-Ergänzung
    * eigenständiger minimaler Client für die REST-API

## Vorbereitung PME

* Server wechseln:
    * Einige ausprobiert - Probleme beim Einrichten & Konfigurieren: Payara, Kumuluz
* POST-Methoden namens `get` mit Filtern als Parameter
    * noch nicht überall eingebaut - vielleicht auch nicht nötig
* Mapping über XML-Dateien konfigurieren: hat nicht funktioniert, wurde "nicht gefunden"
* Docker-Image erstellt
* Hosting eingerichtet
    * Anbieter: Azure, "https://jumblexsale.azurewebsites.net/api/v1"
    * DB: MS-SQL-Server, "wudefhe.database.windows.net"
