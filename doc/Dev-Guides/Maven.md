# Maven

Maven ist ein Build-Werkzeug, das im Java-Umfeld üblich ist.

## Voraussetzungen

* Java-SDK 1.8 muss installiert sein

## Installation

* Maven 3.6.0 herunterladen (http://maven.apache.org/download.cgi):
    - "Binary tar.gz archive" oder "Binary zip archive"
* Maven kann unter Windows nur manuell installiert werden, also...
* den Inhalt des Archivs in einem Ordner ohne Leerzeichen im Pfad auspacken
    - die Nutzlast ist mit Inhalt gemeint (Ordner "bin", Datei "readme.txt", ...)
    - Bsp.: "C:\Application\Maven"

## Einrichtung

* den im Beispiel genannten Ordner "C:\Application\Maven\bin" in die Umgebungsvariable "PATH" aufnehmen (zur Nutzung von Maven über die Kommandozeile)
    - ![alt text](../Images/Windows-10-PATH-finden-1.png "Windows 10: Umgebungsvariable 'PATH' finden, Teil 1")
    - ![alt text](../Images/Windows-10-PATH-finden-2.png "Windows 10: Umgebungsvariable 'PATH' finden, Teil 2")
    - ![alt text](../Images/Windows-10-PATH-Maven.png "Windows 10: Umgebungsvariable 'PATH' um Maven ergänzen")

Für die Nutzung über "Visual Studio Code" benötigt:
* VS-Code-Erweiterung "Java Extension Pack" von Microsoft installieren

## Nutzung

Das Repository ist bereits so eingerichtet, dass Maven über "Visual Studio Code" genutzt werden kann.

## Kommandos

* Projekt erstellen:
    - mvn archetype:generate -DgroupId=com.jumble_x_sale -DartifactId=jumble-x-sale -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
