# PlantUML

Mit PlantUML kann man UML-Diagramme aus Skriptdateien erstellen, die wie Quellcode geschrieben sind.

## Voraussetzungen

* eine Java-Runtime muss installiert sein; es kann natürlich auch ein JDK sein
* das Programm "GraphVIZ" wird benötigt (beschrieben im nächsten Abschnitt)

## Installation

* Auf der Seite "http://graphviz.org/download/" ein "stable package" herunterladen und installieren
* Auf der Seite "http://plantuml.com/download" das "PlantUML compiled Jar" herunterladen
* Das PlantUML-JAR (Java-Archiv mit dem PlantUML-Bytecode) muss manuell "installiert" werden. Das heißt es muss in einem selbst gewählten Verzeichnis abgelegt werden und sollte nachträglich nicht mehr verschoben werden.

## Einrichtung

Die wird für das Beispiel "Visual Studio Code" beschrieben.

* VS-Code-Erweiterung "PlantUML" installieren
* die VS-Code-Einstellungen auswählen
    - ![alt text](../Images/Settings-1.png "VS Code Settings")
* die "settings.json" direkt öffnen (sie enthält die Einstellungen)
    - ![alt text](../Images/Settings-2.png "settings.json")
* die folgende Zeile mit angepasstem JAR-Pfad einfügen:
```json
"plantuml.jar": "C:/Application/PlantUML/plantuml.1.2018.6.jar",
```
* die folgenden Zeilen sind nötig, aber *bereits im Repository konfiguriert*:
```json
"plantuml.exportSubFolder": false,
"plantuml.exportFormat": "png",
"plantuml.exportOutDir": "./",
```

## Benutzung

* Das Projekt-Verzeichnis über VS Code mit "Ordner öffnen" laden
* Im Kontextmenü über dem Editor-Bereich Folgendes nutzen:
    - "Aktuelle Diagramm exportieren"
    - "Vorschau des aktuellen Diagramms (Alt+D)"
    ![alt text](../Images/Export.png "Export/Vorschau")
