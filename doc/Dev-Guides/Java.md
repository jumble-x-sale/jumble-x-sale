# Java

* Java JDK 1.8 herunterladen & installieren - zwei Alternativen:
    - [Oracle Java JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
    - [OpenJDK 1.8](https://developers.redhat.com/products/openjdk/download/) (von Redhat betreut)
* Das neue JDK muss in der Umgebungsvariablen "PATH" bekanntgemacht werden:
    - Pfad "%JAVA_HOME%\bin" am besten ***ganz vorn*** einfügen
    - ![alt text](../Images/Windows-10-PATH-finden-1.png "Windows 10: Umgebungsvariable 'PATH' finden, Teil 1")
    - ![alt text](../Images/Windows-10-PATH-finden-2.png "Windows 10: Umgebungsvariable 'PATH' finden, Teil 2")
    - ![alt text](../Images/Windows-10-PATH-Java.png "Windows 10: Umgebungsvariable 'PATH' um Java ergänzen")
