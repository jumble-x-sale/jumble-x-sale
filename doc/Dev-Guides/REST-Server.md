# REST-Server

## Jumble REST-Server

Base-URI: `/api`

Controllers & Actions:
* Users (including Members): `/user`
* User Messages (including Member Messages): `/user/message`
* Sale Offers: `/sale/offer`
* Sale Offer Messages: `/sale/offer/message`
* Completed Sales: `/sale/completed`
* Completed Sale Messages: `/sale/completed/message`
* Secret Santra Offers: `/secretSanta/offer`
* Completed Secret Santas: `/secretSanta/completed`

## Debugging the REST-Server with VS-Code & Maven 

* Make sure to run task `clean install` after each git pull or at the start of your coding session
* Run task `jetty:run`
* Hit key `F5` ("start debugging")
