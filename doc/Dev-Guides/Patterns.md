# Code-Patterns

Es gibt drei Ebenen in der Datenhaltung:
* UnitOfWork
* Repository
* Entity

## UnitOfWork

Eine "UnitOfWork" stellt den Zugriff auf eine Art "Datenbank" dar.
Sie liefert für jede "Tabelle" ein Repository.

## Repository

Ein Repository stellt den Zugriff auf eine Art "Tabelle" dar.
Über das Repository können einzelne Datensätze ("Entities") abgeholt, hinzugefügt oder gelöscht werden.

## Entity

Eine Entity stellt eine Art "Datensatz" dar.
Sie kann bei Jumble Folgendes sein:
* User: ein Benutzer
* UserMessage: eine Nachricht an einen Benutzer
* Member: ein Mitglied der Plattform (ein nicht-angestellter Nutzer)
* Trade: ein Handel/Angebot
* TradeMessage: eine Nachricht zu einem Handel/Angebot
* SecretSanta: eine Freigabe zum Wichteln
* CompeletedTrade: ein abgeschlossener Handel
* CompeletedTradeMessage: ein Nachricht zu einem abgschlossener Handel
* CompletedSecretSanta: ein abgeschlossenes Wichteln

## Beispiel

Einfacher Test:
```java
import com.jumble_x_sale.data.*;
import com.jumble_x_sale.models.*;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

class UserTests {

    // No explicit constructor needed/allowed!

    private JumbleUnitOfWork uow;

    @Before
    public void runBefore() {
        // Some kind of constructor.
        // Executed each time before a test method is executed.
        User activeUser = ...;
        this.uow = new MockJumbleUnitOfWork(activeUser);
    }

    @After
    public void runAfter() {
        // Some kind of destructor.
        // Executed each time after a test method is executed.
        this.uow.commit();
    }

    @Test
    public void test1() {
        Trade trade = this.uow.getTradeRepository().getById("...");
        UserRepository userRepo = this.uow.getUserRepository();
        User user = userRepo.getByEmail("...");
        userRepo.remove(user);
        user = userRepo.create(...);
    }

}
```
