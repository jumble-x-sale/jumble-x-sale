#!/bin/sh

#-------------------------------------------------------------------------------
# Azure
image=jumble-x-sale-api
tag=v1
hub=wudefhe
hubserver=$hub.azurecr.io

az acr login --name $hub
docker tag $image $hubserver/$image:$tag
docker push $hubserver/$image:$tag

#-------------------------------------------------------------------------------
# GitLab
# image=jumble-x-sale-api
# tag=v1
# project=jumble-x-sale
# hub=jumble-x-sale
# hubserver=registry.gitlab.com/$hub/$project

# docker login registry.gitlab.com -u <username> -p <token>
# docker tag $image $hubserver/$image:$tag
# docker push $hubserver/$image:$tag
