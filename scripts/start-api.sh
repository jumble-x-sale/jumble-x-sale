#!/bin/sh

env=dev;

while getopts e: flag
do
    case "${flag}" in
        e) env=${OPTARG};;
    esac
done

# echo "env = $env";

# ----------------------------------------
# Start api with link to database and environment 'dev'
if [ $env = "dev" ]
then
    docker run --rm -p 127.0.0.1:8080:8080 -p 127.0.0.1:8433:8433 -e JAVA_OPTIONS="-Ddev=1" --link jumble-x-sale-db:database --name jumble-x-sale-api -d jumble-x-sale-api
fi

# ----------------------------------------
# Start api with link to database and environment 'prod'
if [ $env = "prod" ]
then
    docker run --rm -p 127.0.0.1:8080:8080 -p 127.0.0.1:8433:8433 --name jumble-x-sale-api -d jumble-x-sale-api
fi
