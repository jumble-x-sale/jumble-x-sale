--------------------------------------------------------------------------------

use master;
go
create database jumblexsale;
go
create login jumblexsale with password = 'PME_SS20';
go

use jumblexsale;
go
create user jumblexsale for login jumblexsale with default_schema = dbo;
go
grant all on database::jumblexsale to jumblexsale;
go
grant control on database::jumblexsale to jumblexsale;
go

--------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[from_uuid] (@uuid_hex varchar(36))
returns binary(16)
as
begin
  return convert(binary(16), convert(uniqueidentifier, dbo.twist_uuid(@uuid_hex)));
end

GO

create function [dbo].[to_uuid] (@uuid_bin binary(16))
returns varchar(36)
as
begin
  return dbo.twist_uuid(convert(char(36), convert(uniqueidentifier, @uuid_bin)));
end

GO

create function [dbo].[twist_uuid] (@uuid_in varchar(36))
returns varchar(36)
as
begin
  declare   @uuid_out varchar(36)

  select    @uuid_out = lower(
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 7, 2) +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 5, 2) +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 3, 2) +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 1, 2) +
                '-' +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 12, 2) +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 10, 2) +
                '-' +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 17, 2) +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 15, 2) +
                '-' +
                substring(convert(char(36), convert(uniqueidentifier, @uuid_in)), 20, 17));

  return @uuid_out;
end

GO

--------------------------------------------------------------------------------

create table CompletedSaleMessages (CompletedSaleMessageId binary(16) not null, Version numeric(19,0) not null, CreationInstant datetime not null, Message varchar(255) not null, CompletedSaleId binary(16) not null, SenderId binary(16) not null, primary key (CompletedSaleMessageId))
create table CompletedSales (CompletedSaleId binary(16) not null, Version numeric(19,0) not null, CompletionInstant datetime not null, Price numeric(19,2) not null, Category varchar(255) not null, CreationInstant datetime not null, Description varchar(255) not null, ImageUrls varbinary(255), MainImageData varbinary(max), Title varchar(255) not null, BuyerId binary(16) not null, CreatorId binary(16) not null, primary key (CompletedSaleId))
create table CompletedSecretSantas (CompletedSecretSantaId binary(16) not null, Version numeric(19,0) not null, CompletionInstant datetime not null, Category1 varchar(255), CreationInstant1 datetime, Description1 varchar(255), ImageUrls1 varbinary(255), MainImageData1 varbinary(max), Title1 varchar(255), Category2 varchar(255), CreationInstant2 datetime, Description2 varchar(255), ImageUrls2 varbinary(255), MainImageData2 varbinary(max), Title2 varchar(255), CreatorId1 binary(16) not null, CreatorId2 binary(16) not null, primary key (CompletedSecretSantaId))
create table SaleOfferMessages (SaleOfferMessageId binary(16) not null, Version numeric(19,0) not null, CreationInstant datetime not null, Message varchar(255) not null, SaleOfferId binary(16) not null, SenderId binary(16) not null, primary key (SaleOfferMessageId))
create table SaleOffers (SaleOfferId binary(16) not null, Version numeric(19,0) not null, Price numeric(19,2) not null, Category varchar(255) not null, CreationInstant datetime not null, Description varchar(255) not null, ImageUrls varbinary(255), MainImageData varbinary(max), Title varchar(255) not null, CreatorId binary(16) not null, primary key (SaleOfferId))
create table SecretSantaOffers (SecretSantaOfferId binary(16) not null, Version numeric(19,0) not null, Category varchar(255), CreationInstant datetime not null, Description varchar(255), ImageUrls varbinary(255), MainImageData varbinary(max), Title varchar(255), CreatorId binary(16) not null, primary key (SecretSantaOfferId))
create table UserMessages (UserMessageId binary(16) not null, Version numeric(19,0) not null, CreationInstant datetime not null, Message varchar(255) not null, ReceiverId binary(16) not null, SenderId binary(16) not null, primary key (UserMessageId))
create table Users (UserId binary(16) not null, Version numeric(19,0) not null, Active bit not null, CreationInstant datetime not null, Email varchar(255) not null, FirstName varchar(255) not null, IdString varchar(255), LastLoginDateTime datetime, LastName varchar(255) not null, PasswordHash varchar(255) not null, UserType char(1) not null, primary key (UserId))
alter table CompletedSaleMessages add constraint FK99ngmvtue7dmjj5swbgoq31cg foreign key (CompletedSaleId) references CompletedSales
alter table CompletedSaleMessages add constraint FK24175h2dn0h8955ya8f5rxvcv foreign key (SenderId) references Users
alter table CompletedSales add constraint FKqfpvhdpwbtcnavyj8f9ymf0ta foreign key (BuyerId) references Users
alter table CompletedSales add constraint FKhfxfkyseh3kuflsdwyl0hmugx foreign key (CreatorId) references Users
alter table CompletedSecretSantas add constraint FK5v6optpxh79l3jkxwymimo6dt foreign key (CreatorId1) references Users
alter table CompletedSecretSantas add constraint FKotflynn8ry5ox23gdy9nfoigu foreign key (CreatorId2) references Users
alter table SaleOfferMessages add constraint FKdpe0bwdqmeqe1jrlyubj41qcj foreign key (SaleOfferId) references SaleOffers
alter table SaleOfferMessages add constraint FKdfukt931t3uqsq9094q2ux4dg foreign key (SenderId) references Users
alter table SaleOffers add constraint FKh9ofdyfeb3uru3omrvng0syqd foreign key (CreatorId) references Users
alter table SecretSantaOffers add constraint FKag3mcjslhrucs74kx3ky9p06w foreign key (CreatorId) references Users
alter table UserMessages add constraint FKnvb7cvk869gwn24r4uq2vtdeq foreign key (ReceiverId) references Users
alter table UserMessages add constraint FK3bigvfgrigorfv24b2mmexyii foreign key (SenderId) references Users
