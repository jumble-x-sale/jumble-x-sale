# ----------------------------------------------------------------------------
# Jumble-X-Sale docker image

FROM jetty:9.4.31-jdk8

USER root

ADD code/server/target/server-1.0-SNAPSHOT.war /var/lib/jetty/webapps/

RUN chmod u=rx,g=rx,o=rx /var/lib/jetty/webapps/server-1.0-SNAPSHOT.war

USER jetty

ENV JAVA_OPTIONS="-Dprod=1"
