package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.models.Credentials;
import com.jumble_x_sale.models.IdCard;
import com.jumble_x_sale.models.Session;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserData;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.UserDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 * The REST-Controller for access to jumble users.
 */
@Path("/user")
public class UserController extends JumbleController {

    // region Fields

    @Context
    private UriInfo uriInfo;

    private Lazy<UserDataConverter> converter = new Lazy<>(() -> new UserDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public UserController(@Context SecurityContext securityContext, @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Create a new user.
     * 
     * @param data The data of the user to create.
     * @return The id of the newly created user.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response create(UserData data) {
        if (this.converter.get().validForEntityCreation(data)) {
            User member = this.uow.getUserRepository().create(UserType.MEMBER, data.getFirstName(),
                    data.getLastName(), data.getEmail(), data.getPassword(),
                    new IdCard(data.getIdCard()), true);
            if (member != null) {
                this.uow.commit();
                return Response.ok(member.getId().toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Update a user.
     * 
     * @param data The data to update the user with.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(UserData data) {
        User user = this.converter.get().updateEntity(data);
        if (user != null) {
            this.uow.getUserRepository().merge(user);
            this.uow.commit();
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Delete a user.
     * 
     * @param userId The id of the user to delete.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @DELETE
    @Path("/{userId}")
    public Response delete(@PathParam("userId") UUID userId) {
        if (this.uow.getUserRepository().remove(this.uow.getUserRepository().getById(userId))) {
            this.uow.commit();
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Get a user by its id.
     * 
     * @param userId The id of the user to get.
     * @return The data of the user to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{userId}")
    public Response getById(@PathParam("userId") UUID userId) {
        User user = this.uow.getUserRepository().getById(userId);
        // We do not want to give the information away that a user is not registered.
        // To gain that information, someone has to try to register.
        return Response.ok(this.converter.get().createData(user)).build();
    }

    /**
     * Get users.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the users.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getUserRepository().get(filterPagingOptions));
    }

    /**
     * Get a user by its email.
     * 
     * @param email The email of the user to get.
     * @return The data of the user to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byEmail/{email}")
    public Response getByEmail(@PathParam("email") String email) {
        User user = this.uow.getUserRepository().getByEmail(email);
        // We do not want to give the information away that a user is not registered.
        // To gain that information, someone has to try to register.
        return Response.ok(this.converter.get().createData(user)).build();
    }

    /**
     * Get a user by its name.
     * 
     * @param firstName The first name of the user to get.
     * @param lastName  The last name of the user to get.
     * @return The data of the user to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byName/{firstName}/{lastName}")
    public Response getByName(@PathParam("firstName") String firstName,
            @PathParam("lastName") String lastName) {
        User user = this.uow.getUserRepository().getByName(firstName, lastName);
        // We do not want to give the information away that a user is not registered.
        // To gain that information, someone has to try to register.
        return Response.ok(this.converter.get().createData(user)).build();
    }

    /**
     * Log the given user in.
     * 
     * @param credentials The crendentials of user to log in.
     * @return The JWT token.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response login(Credentials credentials) {
        if (this.uow.getUserRepository().login(credentials.getEmail(), credentials.getPassword())) {
            return Response.ok(new Session(this.uow.getActiveUser(), uriInfo)).build();
        }
        return Response.status(Response.Status.FORBIDDEN).build();
    }

    // endregion Methods

}
