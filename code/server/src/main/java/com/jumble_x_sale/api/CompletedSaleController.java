package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.CompletedSaleDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * The REST-Controller for access to jumble completed sales.
 */
@Path("/sale/completed")
public class CompletedSaleController extends JumbleController {

    // region Fields

    private Lazy<CompletedSaleDataConverter> converter =
            new Lazy<>(() -> new CompletedSaleDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public CompletedSaleController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Get completed sales.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the completed sales.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getCompletedSaleRepository().get(filterPagingOptions));
    }

    /**
     * Get completed sales by their creator.
     * 
     * @param creatorId           The id of creator of the completed sales to get.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the secret santa offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getByCreator/{creatorId}")
    public Response getByCreator(@PathParam("creatorId") UUID creatorId,
            FilterPagingOptions filterPagingOptions) {
        return this.createResponse(this.converter.get(),
                this.uow.getCompletedSaleRepository().getByCreator(creatorId, filterPagingOptions));
    }

    /**
     * Get completed sales excepting one creator.
     * 
     * @param creatorId           The id of the {@link User member} whose {@link CompletedSale
     *                            completed sales} to except.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the secret santa offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getExceptCreator/{creatorId}")
    public Response getExceptCreator(@PathParam("creatorId") UUID creatorId,
            FilterPagingOptions filterPagingOptions) {
        return this.createResponse(this.converter.get(), this.uow.getCompletedSaleRepository()
                .getExceptCreator(creatorId, filterPagingOptions));
    }

    /**
     * Get completed sales by their buyer.
     * 
     * @param buyerId    The id of buyer of the completed sales to get.
     * @param pageNumber The number of the result page to get.
     * @param pageSize   The size of the result page to get.
     * @return The data of the secret santa offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byBuyer/{buyerId}")
    public Response getByBuyer(@PathParam("buyerId") UUID buyerId,
            @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
        PagingOptions pagingOptions = ResultOptions.withPaging(pageNumber, pageSize);
        User buyer = this.uow.getUserRepository().getById(buyerId);

        return this.createResponse(this.converter.get(),
                this.uow.getCompletedSaleRepository().getByBuyer(buyer, pagingOptions));
    }

    // endregion Methods

}
