package com.jumble_x_sale.api;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.DeflaterInputStream;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;
import javax.ws.rs.ext.WriterInterceptor;
import javax.ws.rs.ext.WriterInterceptorContext;
import org.apache.commons.lang3.StringUtils;

/**
 * The interceptor that applies GZIP compression for requests and responses.
 */
@Provider
@Compression
public class GZIPInterceptor implements ReaderInterceptor, WriterInterceptor {

    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    public static final String CONTENT_ENCODING = "Content-Encoding";
    public static final String GZIP = "gzip";
    public static final String DEFLATE = "deflate";
    public static final String GZIP_DEFLATE = "gzip, deflate";

    @Override
    public Object aroundReadFrom(ReaderInterceptorContext context)
            throws IOException, WebApplicationException {

        MultivaluedMap<String, String> headers = context.getHeaders();
        if (headers.get(CONTENT_ENCODING).stream()
                .anyMatch(v -> StringUtils.containsIgnoreCase(v, GZIP)
                        && StringUtils.containsIgnoreCase(v, DEFLATE))) {
            final InputStream inputStream = context.getInputStream();
            context.setInputStream(new DeflaterInputStream(new GZIPInputStream(inputStream)));
        }
        return context.proceed();
    }

    @Override
    public void aroundWriteTo(WriterInterceptorContext context)
            throws IOException, WebApplicationException {

        MultivaluedMap<String, Object> headers = context.getHeaders();
        if (headers.get(CONTENT_ENCODING).stream()
                .anyMatch(v -> StringUtils.containsIgnoreCase(v.toString(), GZIP)
                        && StringUtils.containsIgnoreCase(v.toString(), DEFLATE))) {
            final OutputStream outputStream = context.getOutputStream();
            context.setOutputStream(new GZIPOutputStream(new DeflaterOutputStream(outputStream)));
        }
        context.proceed();
    }

}
