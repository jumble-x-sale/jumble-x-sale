package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.CompletedSale;
import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferData;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.SaleOfferDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.PathParam;

/**
 * The REST-Controller for access to jumble sale offers.
 */
@Path("/sale/offer")
public class SaleOfferController extends JumbleController {

    // region Fields

    private Lazy<SaleOfferDataConverter> converter =
            new Lazy<>(() -> new SaleOfferDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public SaleOfferController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Accept and complete an existing sale offer.
     * 
     * @param saleOfferId The id of the sale offer to complete.
     * @return A response with the id of the completed sale.
     */
    @Secured({UserType.MEMBER})
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/accept/{saleOfferId}")
    public Response accept(@PathParam("saleOfferId") UUID saleOfferId) {
        SaleOffer saleOffer = this.uow.getSaleOfferRepository().getById(saleOfferId);
        if (saleOffer != null && !saleOffer.getInfo().getCreator().getId()
                .equals(this.uow.getActiveUser().getId())) {
            CompletedSale completedSale = this.uow.getCompletedSaleRepository().create(saleOffer,
                    this.uow.getActiveUser());
            if (completedSale != null) {
                this.uow.commit();
                return Response.ok(completedSale.getId().toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Create a new sale offer.
     * 
     * @param data The data of the sale offer to create.
     * @return A response with the id of the newly created sale offer.
     */
    @Secured({UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response create(SaleOfferData data) {
        data.setCreatorId(this.uow.getActiveUser().getId());
        if (this.converter.get().validForEntityCreation(data)) {
            SaleOffer saleOffer = this.uow.getSaleOfferRepository().create(data.getCategory(),
                    data.getTitle(), data.getDescription(), data.getImageUrls(),
                    data.getMainImageDataBin(), data.getPrice());
            if (saleOffer != null) {
                this.uow.commit();
                return Response.ok(saleOffer.getId().toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Update a sale offer.
     * 
     * @param data The data to update the sale offer with.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(SaleOfferData data) {
        SaleOffer saleOffer = this.converter.get().updateEntity(data);
        if (saleOffer != null) {
            this.uow.getSaleOfferRepository().merge(saleOffer);
            this.uow.commit();
            return Response.ok().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Delete a sale offer.
     * 
     * @param saleOfferId The id of the sale offer to delete.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @DELETE
    @Path("/{saleOfferId}")
    public Response delete(@PathParam("saleOfferId") UUID saleOfferId) {
        SaleOffer saleOffer = this.uow.getSaleOfferRepository().getById(saleOfferId);
        if (saleOffer != null) {
            this.uow.getSaleOfferRepository().remove(saleOffer);
            this.uow.commit();
            return Response.ok().build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Get sale offers.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the sale offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getSaleOfferRepository().get(filterPagingOptions));
    }

    /**
     * Get sale offers by creator.
     * 
     * @param creatorId           The id of the {@link User member} that created the
     *                            {@link SaleOffer sale offers} to get.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the sale offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getByCreator/{creatorId}")
    public Response getByCreator(@PathParam("creatorId") UUID creatorId,
            FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getSaleOfferRepository().getByCreator(creatorId, filterPagingOptions));
    }

    /**
     * Get sale offers excepting one creator.
     * 
     * @param creatorId           The id of the {@link User member} whose {@link SaleOffer sale
     *                            offers} to except.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the sale offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getExceptCreator/{creatorId}")
    public Response getExceptCreator(@PathParam("creatorId") UUID creatorId,
            FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getSaleOfferRepository().getExceptCreator(creatorId, filterPagingOptions));
    }

    /**
     * Get a sale offer by its id.
     * 
     * @param saleOfferId The id of the sale offer to get.
     * @return The data of the sale offer to get.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{saleOfferId}")
    public Response getById(@PathParam("saleOfferId") UUID saleOfferId) {
        SaleOffer saleOffer = this.uow.getSaleOfferRepository().getById(saleOfferId);
        if (saleOffer != null) {
            return Response.ok(this.converter.get().createData(saleOffer)).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Get sale offers by its category.
     * 
     * @param category   The category of the sale offers to get.
     * @param pageNumber The number of the result page to get.
     * @param pageSize   The size of the result page to get.
     * @return The data of the sale offers.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byCategory/{category}")
    public Response getByCategory(@PathParam("category") String category,
            @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
        PagingOptions pagingOptions = ResultOptions.withPaging(pageNumber, pageSize);
        return this.createResponse(this.converter.get(),
                this.uow.getSaleOfferRepository().getByCategory(category, pagingOptions));
    }

    // endregion Methods

}
