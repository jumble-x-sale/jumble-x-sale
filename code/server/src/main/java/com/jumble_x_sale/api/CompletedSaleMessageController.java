package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.CompletedSaleMessage;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.CompletedSaleMessageDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 * The REST-Controller for access to jumble users.
 */
@Path("/sale/completed/message")
public class CompletedSaleMessageController extends JumbleController {

    // region Fields

    @Context
    private UriInfo uriInfo;

    private Lazy<CompletedSaleMessageDataConverter> converter =
            new Lazy<>(() -> new CompletedSaleMessageDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public CompletedSaleMessageController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Get a completed sales message by its id.
     * 
     * @param completedSaleMessageId The id of the completed sales message to get.
     * @return The data of the completed sales message to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{completedSaleMessageId}")
    public Response getById(@PathParam("completedSaleMessageId") UUID completedSaleMessageId) {
        CompletedSaleMessage user =
                this.uow.getCompletedSaleMessageRepository().getById(completedSaleMessageId);
        // We do not want to give the information away that a user is not registered.
        // To gain that information, someone has to try to register.
        return Response.ok(this.converter.get().createData(user)).build();
    }

    /**
     * Get completed sale messages.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the completed sale messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getCompletedSaleMessageRepository().get(filterPagingOptions));
    }

    /**
     * Get completed sale messages by sale creator.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the completed sale messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getBySaleCreator/{saleCreatorId}")
    public Response getBySaleCreator(@PathParam("saleCreatorId") UUID saleCreatorId,
            FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getCompletedSaleMessageRepository().getBySaleCreator(saleCreatorId,
                        filterPagingOptions));
    }

    /**
     * Get completed sales messages.
     * 
     * @param completedSaleId The id of the completed sale.
     * @param pageNumber      The number of the page to find.
     * @param pageSize        The size of pages to find.
     * @return The data of the completed sales message to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/bySale/{completedSaleId}")
    public Response getBySale(@PathParam("completedSaleId") UUID completedSaleId,
            @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
        PagingOptions pagingOptions = ResultOptions.withPaging(pageNumber, pageSize);
        return this.createResponse(this.converter.get(), this.uow
                .getCompletedSaleMessageRepository().getBySale(completedSaleId, pagingOptions));
    }

    // endregion Methods

}
