package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.CompletedSecretSantaDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * The REST-Controller for access to jumble completed secret santas.
 */
@Path("/secretSanta/completed")
public class CompletedSecretSantaController extends JumbleController {

    // region Fields

    private Lazy<CompletedSecretSantaDataConverter> converter =
            new Lazy<>(() -> new CompletedSecretSantaDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public CompletedSecretSantaController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Get completed secret santas.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the completed secret santas.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getCompletedSecretSantaRepository().get(filterPagingOptions));
    }

    /**
     * Get completed secret santas of a member.
     * 
     * @param memberId            The id of the member that was involved in a completed secret
     *                            santa.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the secret santa offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getByMember/{memberId}")
    public Response getByMember(@PathParam("memberId") UUID memberId,
            FilterPagingOptions filterPagingOptions) {
        User member = this.uow.getUserRepository().getById(memberId);

        return this.createResponse(this.converter.get(), this.uow
                .getCompletedSecretSantaRepository().getByMember(member, filterPagingOptions));
    }

    // endregion Methods

}
