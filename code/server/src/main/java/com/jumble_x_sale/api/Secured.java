package com.jumble_x_sale.api;

import com.jumble_x_sale.models.UserType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

/**
 * Used for marking controllers and or methods as secured.
 * 
 * <p>
 * Access to the annotated controller or method is granted only when a user of
 * one or more specific user types is logged in.
 * </p>
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
public @interface Secured {
    UserType[] value() default {};
}
