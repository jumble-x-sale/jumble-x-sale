package com.jumble_x_sale.api;

import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.SecretSantaOfferDataConverter;
import com.jumble_x_sale.util.Lazy;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.CompletedSecretSanta;
import com.jumble_x_sale.models.SecretSantaOffer;
import com.jumble_x_sale.models.SecretSantaOfferData;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

/**
 * The REST-Controller for access to jumble secret santa offers.
 */
@Path("/secretSanta/offer")
public class SecretSantaOfferController extends JumbleController {

    // region fields

    private Lazy<SecretSantaOfferDataConverter> converter =
            new Lazy<>(() -> new SecretSantaOfferDataConverter(this.uow));

    // endregion fields

    // region Constructors

    public SecretSantaOfferController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Match and complete an existing secret santa offer.
     * 
     * @param secretSantaOfferId The id of the secret santa offer to complete.
     * @return A response with the id of the completed secret santa.
     */
    @Secured({UserType.MEMBER})
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/match/{secretSantaOfferId}")
    public Response match(@PathParam("secretSantaOfferId") UUID secretSantaOfferId) {
        SecretSantaOffer secretSantaOffer =
                this.uow.getSecretSantaOfferRepository().getById(secretSantaOfferId);
        if (secretSantaOffer != null) {
            SecretSantaOffer match =
                    this.uow.getSecretSantaOfferRepository().match(secretSantaOffer);
            if (match != null) {
                CompletedSecretSanta completedSecretSanta = this.uow
                        .getCompletedSecretSantaRepository().create(secretSantaOffer, match);
                if (completedSecretSanta != null) {
                    this.uow.commit();
                    return Response.ok(completedSecretSanta.getId().toString()).build();
                }
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Create a new secret santa offer.
     * 
     * @param data The data of the secret santa offer to create.
     * @return A response with the id of the newly created secret santa offer.
     */
    @Secured({UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response create(SecretSantaOfferData data) {
        data.setCreatorId(this.uow.getActiveUser().getId());
        if (this.converter.get().validForEntityCreation(data)) {
            SecretSantaOffer secretSantaOffer = this.uow.getSecretSantaOfferRepository().create(
                    this.uow.getUserRepository().getById(data.getCreatorId()), data.getCategory(),
                    data.getTitle(), data.getDescription(), data.getImageUrls(),
                    data.getMainImageDataBin());
            if (secretSantaOffer != null) {
                this.uow.commit();
                return Response.ok(secretSantaOffer.getId().toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Update a secret santa offer.
     * 
     * @param data The data to update the secret santa offer with.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(SecretSantaOfferData data) {
        SecretSantaOffer secretSantaOffer = this.converter.get().updateEntity(data);
        if (secretSantaOffer != null) {
            this.uow.getSecretSantaOfferRepository().merge(secretSantaOffer);
            this.uow.commit();
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Delete a secret santa offer.
     * 
     * @param secretSantraOfferId The id of the secret santa offer to delete.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @DELETE
    @Path("/{secretSantraOfferId}")
    public Response delete(@PathParam("secretSantraOfferId") UUID secretSantraOfferId) {
        if (this.uow.getSecretSantaOfferRepository()
                .remove(this.uow.getSecretSantaOfferRepository().getById(secretSantraOfferId))) {
            this.uow.commit();
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Get secret santa offers.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the secret santa offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getSecretSantaOfferRepository().get(filterPagingOptions));
    }

    /**
     * Get secret santa offers by creator.
     * 
     * @param creatorId           The id of the {@link User member} that created the
     *                            {@link SecretSantaOffer secret santa offers} to get.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the secret santa offers.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getByCreator/{creatorId}")
    public Response getByCreator(@PathParam("creatorId") UUID creatorId,
            FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(), this.uow.getSecretSantaOfferRepository()
                .getByCreator(creatorId, filterPagingOptions));
    }

    /**
     * Get a secret santa offer by its id.
     * 
     * @param secretSantaOfferId The id of the secret santa offer to get.
     * @return The data of the sale offer to get.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{secretSantaOfferId}")
    public Response getById(@PathParam("secretSantaOfferId") UUID secretSantaOfferId) {
        SecretSantaOffer secretSantaOffer =
                this.uow.getSecretSantaOfferRepository().getById(secretSantaOfferId);
        if (secretSantaOffer == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(this.converter.get().createData(secretSantaOffer)).build();
    }

    /**
     * Get secret santa offers by its category.
     * 
     * @param category   The category of the secret santa offers to get.
     * @param pageNumber The number of the result page to get.
     * @param pageSize   The size of the result page to get.
     * @return The data of the secret santa offers.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byCategory/{category}")
    public Response getByCategory(@PathParam("category") String category,
            @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
        PagingOptions pagingOptions = ResultOptions.withPaging(pageNumber, pageSize);
        return this.createResponse(this.converter.get(),
                this.uow.getSecretSantaOfferRepository().getByCategory(category, pagingOptions));
    }

    /**
     * Get secret santa offers by its member.
     * 
     * @param creatorId  The id of the member that created the secret santa offers to get.
     * @param pageNumber The number of the result page to get.
     * @param pageSize   The size of the result page to get.
     * @return The data of the secret santa offers.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/byCreator/{creatorId}")
    public Response getByCreator(@PathParam("creatorId") UUID creatorId,
            @QueryParam("pageNumber") int pageNumber, @QueryParam("pageSize") int pageSize) {
        PagingOptions pagingOptions = ResultOptions.withPaging(pageNumber, pageSize);
        return createResponse(this.converter.get(),
                this.uow.getSecretSantaOfferRepository().getByCreator(creatorId, pagingOptions));
    }

    // endregion Methods

}
