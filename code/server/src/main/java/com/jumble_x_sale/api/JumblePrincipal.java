package com.jumble_x_sale.api;

import java.security.Principal;

public class JumblePrincipal implements Principal {

    // region Fields

    private final String name;

    // endregion Fields

    // region Getters

    /**
     * {@inheritDoc}
     */
    public String getName() {
        return this.name;
    }

    // endregion Getters

    // region Constructors

    public JumblePrincipal(String name) {
        this.name = name;
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean equals(Object another) {
        if (another instanceof JumblePrincipal) {
            JumblePrincipal otherPrincipal = (JumblePrincipal) another;
            return this.name.equals(otherPrincipal.name);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public String toString() {
        return this.name;
    }

    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return this.name.hashCode();
    }

    // endregion Methods

}
