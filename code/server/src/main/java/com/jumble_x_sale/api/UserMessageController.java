package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.models.UserMessage;
import com.jumble_x_sale.models.UserMessageData;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.UserMessageDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 * The REST-Controller for access to jumble users.
 */
@Path("/user/message")
public class UserMessageController extends JumbleController {

    // region Fields

    @Context
    private UriInfo uriInfo;

    private Lazy<UserMessageDataConverter> converter =
            new Lazy<>(() -> new UserMessageDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public UserMessageController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Create a new user message.
     * 
     * @param data The data of the user message to create.
     * @return The id of the newly created user message.
     */
    @Secured({UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response create(UserMessageData data) {
        data.setSenderId(this.uow.getActiveUser().getId());
        if (this.converter.get().validForEntityCreation(data)) {
            UserMessage userMessage = this.uow.getUserMessageRepository()
                    .create(data.getReceiverId(), data.getSenderId(), data.getMessage());
            if (userMessage != null) {
                this.uow.commit();
                return Response.ok(userMessage.getId().toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Update a user message.
     * 
     * @param data The data to update the user message with.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(UserMessageData data) {
        UserMessage user = this.converter.get().updateEntity(data);
        if (user != null) {
            this.uow.getUserMessageRepository().merge(user);
            this.uow.commit();
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Delete a user message.
     * 
     * @param userMessageId The id of the user message to delete.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @DELETE
    @Path("/{userMessageId}")
    public Response delete(@PathParam("userMessageId") UUID userMessageId) {
        if (this.uow.getUserRepository()
                .remove(this.uow.getUserRepository().getById(userMessageId))) {
            this.uow.commit();
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Get a user message by its id.
     * 
     * @param userMessageId The id of the user message to get.
     * @return The data of the user message to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{userMessageId}")
    public Response getById(@PathParam("userMessageId") UUID userMessageId) {
        UserMessage user = this.uow.getUserMessageRepository().getById(userMessageId);
        // We do not want to give the information away that a user is not registered.
        // To gain that information, someone has to try to register.
        return Response.ok(this.converter.get().createData(user)).build();
    }

    /**
     * Get user messages.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the user messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getUserMessageRepository().get(filterPagingOptions));
    }

    /**
     * Get {@link UserMessage user message} for a {@link User member}.
     * 
     * @param memberId            The {@link User receiver or sender} of the message.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the user messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getByMember/{memberId}")
    public Response getByMember(@PathParam("memberId") UUID memberId,
            FilterPagingOptions filterPagingOptions) {
        return this.createResponse(this.converter.get(),
                this.uow.getUserMessageRepository().getByMember(memberId, filterPagingOptions));
    }

    /**
     * Get {@link UserMessage user message} for a {@link User receiver}.
     * 
     * @param receiverId          The {@link User receiver} of the message.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the user messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getByReceiver/{receiverId}")
    public Response getByReceiver(@PathParam("receiverId") UUID receiverId,
            FilterPagingOptions filterPagingOptions) {
        return this.createResponse(this.converter.get(),
                this.uow.getUserMessageRepository().getByReceiver(receiverId, filterPagingOptions));
    }

    // endregion Methods

}
