package com.jumble_x_sale.api;

import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.JumbleWorkContext;

import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Represents config for application start and shutdown.
 */
@WebListener
public class JumbleRestApplicationInit implements ServletContextListener {

    // region Fields

    private static final Logger LOGGER = Logger.getLogger(JumbleRestApplicationInit.class.getSimpleName());

    private static JumbleWorkContext workContext;

    // endregion Fields

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        workContext = JumbleWorkContext.create();
        LOGGER.info("\"JumbleWorkContext\" created (env = " + JumbleWorkContext.getEnvironmentName() + ").");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        workContext.tryClose();
    }

    /**
     * Create a new unit of work to handle a self-contained process.
     * 
     * @return A new unit of work to handle a self-contained process.
     */
    public static JumbleUnitOfWork createUnitOfWork() {
        return workContext.createUnitOfWork();
    }

    // endregion Methods

}
