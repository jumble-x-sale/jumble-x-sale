package com.jumble_x_sale.api;

import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.SaleOfferMessageData;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.convert.SaleOfferMessageDataConverter;
import com.jumble_x_sale.util.Lazy;

import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

/**
 * The REST-Controller for access to jumble users.
 */
@Path("/sale/offer/message")
public class SaleOfferMessageController extends JumbleController {

    // region Fields

    @Context
    private UriInfo uriInfo;

    private Lazy<SaleOfferMessageDataConverter> converter =
            new Lazy<>(() -> new SaleOfferMessageDataConverter(this.uow));

    // endregion Fields

    // region Constructors

    public SaleOfferMessageController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        super(securityContext, headers);
    }

    // endregion Constructors

    // region Methods

    /**
     * Create a new sale offer message.
     * 
     * @param data The data of the sale offer message to create.
     * @return The id of the newly created sale offer message.
     */
    @Secured({UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response create(SaleOfferMessageData data) {
        data.setSenderId(this.uow.getActiveUser().getId());
        if (this.converter.get().validForEntityCreation(data)) {
            SaleOfferMessage saleOfferMessage = this.uow.getSaleOfferMessageRepository()
                    .create(data.getSaleOfferId(), data.getSenderId(), data.getMessage());
            if (saleOfferMessage != null) {
                this.uow.commit();
                return Response.ok(saleOfferMessage.getId().toString()).build();
            }
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Update a sale offer message.
     * 
     * @param data The data to update the sale offer message with.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(SaleOfferMessageData data) {
        SaleOfferMessage user = this.converter.get().updateEntity(data);
        if (user != null) {
            this.uow.getSaleOfferMessageRepository().merge(user);
            this.uow.commit();
            return Response.ok().build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    /**
     * Delete a sale offer message.
     * 
     * @param saleOfferMessageId The id of the sale offer message to delete.
     * @return An empty response.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @DELETE
    @Path("/{saleOfferMessageId}")
    public Response delete(@PathParam("saleOfferMessageId") UUID saleOfferMessageId) {
        if (this.uow.getUserRepository()
                .remove(this.uow.getUserRepository().getById(saleOfferMessageId))) {
            this.uow.commit();
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Get a sale offer message by its id.
     * 
     * @param saleOfferMessageId The id of the sale offer message to get.
     * @return The data of the sale offer message to get.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{saleOfferMessageId}")
    public Response getById(@PathParam("saleOfferMessageId") UUID saleOfferMessageId) {
        SaleOfferMessage user =
                this.uow.getSaleOfferMessageRepository().getById(saleOfferMessageId);
        // We do not want to give the information away that a user is not registered.
        // To gain that information, someone has to try to register.
        return Response.ok(this.converter.get().createData(user)).build();
    }

    /**
     * Get sale offer messages.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the sale offer messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public Response get(FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(),
                this.uow.getSaleOfferMessageRepository().get(filterPagingOptions));
    }

    /**
     * Get sale offer messages by sale creator.
     * 
     * @param saleCreatorId       The ID of the {@link User sale offer creator}.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the sale offer messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getBySaleCreator/{saleCreatorId}")
    public Response getBySaleCreator(@PathParam("saleCreatorId") UUID saleCreatorId,
            FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(), this.uow.getSaleOfferMessageRepository()
                .getBySaleCreator(saleCreatorId, filterPagingOptions));
    }

    /**
     * Get sale offer messages by sale.
     * 
     * @param saleOfferId         The ID of the {@link SaleOffer sale offer}.
     * @param filterPagingOptions The {@link FilterPagingOptions filter paging options} for the
     *                            result.
     * @return The data of the sale offer messages.
     */
    @Secured({UserType.ADMIN, UserType.CLERK, UserType.MEMBER})
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getBySale/{saleOfferId}")
    public Response getBySale(@PathParam("saleCreatorId") UUID saleOfferId,
            FilterPagingOptions filterPagingOptions) {
        // Technically we could use a "GET" with payload, but we shouldn't do that!
        // So we use a "POST" with path "/get".
        return this.createResponse(this.converter.get(), this.uow.getSaleOfferMessageRepository()
                .getBySale(saleOfferId, filterPagingOptions));
    }

    // endregion Methods

}
