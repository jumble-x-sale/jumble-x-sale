package com.jumble_x_sale.api;

import java.util.Set;
import java.util.HashSet;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/v1")
public class JumbleRestApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();

        classes.add(JumbleAuthenticationFilter.class);
        // classes.add(GZIPInterceptor.class);

        classes.add(UserController.class);
        classes.add(UserMessageController.class);
        classes.add(SaleOfferController.class);
        classes.add(SaleOfferMessageController.class);
        classes.add(SecretSantaOfferController.class);
        classes.add(CompletedSaleController.class);
        classes.add(CompletedSaleMessageController.class);
        classes.add(CompletedSecretSantaController.class);

        return classes;
    }

}
