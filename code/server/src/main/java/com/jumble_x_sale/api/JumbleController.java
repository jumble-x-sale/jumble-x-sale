package com.jumble_x_sale.api;

import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.models.Data;
import com.jumble_x_sale.models.PagingDataResult;
import com.jumble_x_sale.models.convert.DataConverter;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import org.apache.commons.lang3.StringUtils;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.SecurityContext;

import java.util.ArrayList;
import java.util.List;

/**
 * A base implementation for jumble controllers.
 */
public abstract class JumbleController implements AutoCloseable {

    // region Fields

    private static final String ACCEPT_ENCODING = "Accept-Encoding";
    private static final String CONTENT_ENCODING = "Content-Encoding";
    private static final String GZIP = "gzip";
    private static final String DEFLATE = "deflate";
    private static final String GZIP_DEFLATE = "gzip, deflate";

    /**
     * The jumble security context.
     */
    protected SecurityContext securityContext;

    /**
     * The HTTP headers.
     */
    protected HttpHeaders headers;

    /**
     * The jumble unit of work.
     */
    protected final JumbleUnitOfWork uow;

    // region Constructors

    protected JumbleController(@Context SecurityContext securityContext,
            @Context HttpHeaders headers) {
        this.securityContext = securityContext;
        this.headers = headers;
        this.uow = JumbleRestApplicationInit.createUnitOfWork();
    }

    // endregion Constructors

    // region Methods

    /**
     * Close this persistence context.
     */
    @Override
    public void close() throws Exception {
        this.uow.close();
    }

    /**
     * Create a response with a list of entities in their flat data form.
     * 
     * @param <E>           The type of entity.
     * @param <D>           The type of entity flat data.
     * @param dataConverter The entity data converter.
     * @param result        The result to convert.
     * @return A response with a list of entities in their flat data form.
     */
    private Response createResponse(Object result) {
        ResponseBuilder builder = Response.ok(result);
        if (headers.getRequestHeader(ACCEPT_ENCODING).stream()
                .anyMatch(v -> StringUtils.containsIgnoreCase(v, GZIP)
                        && StringUtils.containsIgnoreCase(v, DEFLATE))) {
            // We only set the header here. The output will be changed later by an interceptor.
            builder.header(CONTENT_ENCODING, GZIP_DEFLATE);
        }
        return builder.build();
    }

    /**
     * Create a response with a list of entities in their flat data form.
     * 
     * @param <E>           The type of entity.
     * @param <D>           The type of entity flat data.
     * @param dataConverter The entity data converter.
     * @param result        The result to convert.
     * @return A response with a list of entities in their flat data form.
     */
    protected <E extends Entity, D extends Data> Response createResponse(
            DataConverter<E, D> dataConverter, E result) {
        return createResponse(dataConverter.createData(result));
    }

    /**
     * Create a response with a list of entities in their flat data form.
     * 
     * @param <E>           The type of entity.
     * @param <D>           The type of entity flat data.
     * @param dataConverter The entity data converter.
     * @param result        The result to convert.
     * @return A response with a list of entities in their flat data form.
     */
    protected <E extends Entity, D extends Data> Response createResponse(
            DataConverter<E, D> dataConverter, PagingResult<E> result) {
        List<D> dataList = new ArrayList<>();
        result.getData().forEach(entity -> dataList.add(dataConverter.createData(entity)));
        return createResponse(new PagingDataResult<>(result.getOptions(), dataList));
    }

    // endregion Methods

}
