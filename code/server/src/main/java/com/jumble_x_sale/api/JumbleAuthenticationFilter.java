package com.jumble_x_sale.api;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.TimeUtils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.io.IOException;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Priority;
import javax.crypto.SecretKey;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.Provider;

/**
 * A jumble authentication filter.
 * <p>
 * The code was mainly reused from an <a href=
 * "https://stackoverflow.com/questions/26777083/best-practice-for-rest-token-based-authentication-with-jax-rs-and-jersey">example
 * at stackoverflow</a>.
 * </p>
 */
@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class JumbleAuthenticationFilter implements ContainerRequestFilter {

    // region Fields

    private static final String REALM = "example";
    public static final String AUTHENTICATION_SCHEME = "Bearer";
    private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    private static SecretKey key = null;

    @Context
    private ResourceInfo resourceInfo;

    @Context
    private ResourceContext resourceContext;

    @Context
    private UriInfo uriInfo;

    // endregion Fields

    // region Getters

    public static SecretKey getKey() {
        if (key == null) {
            key = Keys.secretKeyFor(SIGNATURE_ALGORITHM);
        }
        return key;
    }

    // endregion Getters

    // region Methods

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        // Get the Authorization header from the request
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Validate the Authorization header
        if (!isTokenBasedAuthentication(authorizationHeader)) {
            abortWithUnauthorized(requestContext);
            return;
        }

        // Extract the token from the Authorization header
        String token = authorizationHeader.substring(AUTHENTICATION_SCHEME.length()).trim();

        User user = null;
        try {
            user = validateToken(token);
            JumbleSecurityContext jumbleSecurityContext =
                    new JumbleSecurityContext(requestContext.getSecurityContext(), user);
            requestContext.setSecurityContext(jumbleSecurityContext);
            // The controller will be created before the authentication filter is applied,
            // so we amend the controller here.
            JumbleController jumbleController =
                    (JumbleController) resourceContext.getResource(resourceInfo.getResourceClass());
            jumbleController.securityContext = jumbleSecurityContext;
            jumbleController.uow.setActiveUser(user);
        } catch (Exception e) {
            abortWithUnauthorized(requestContext);
        }

        // Get the resource class which matches with the requested URL
        // Extract the roles declared by it
        Class<?> resourceClass = resourceInfo.getResourceClass();
        List<UserType> classRoles = extractRoles(resourceClass);

        // Get the resource method which matches with the requested URL
        // Extract the roles declared by it
        Method resourceMethod = resourceInfo.getResourceMethod();
        List<UserType> methodRoles = extractRoles(resourceMethod);

        try {

            // Check if the user is allowed to execute the method
            // The method annotations override the class annotations
            if (methodRoles.isEmpty()) {
                checkPermissions(user, classRoles);
            } else {
                checkPermissions(user, methodRoles);
            }

        } catch (NotAuthorizedException e) {
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        } catch (Exception e) {
            requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).build());
        }
    }

    /**
     * Extract the roles from the annotated element
     * 
     * @param annotatedElement The element that has been annotated.
     * @return The roles from the annotated element.
     */
    private List<UserType> extractRoles(AnnotatedElement annotatedElement) {
        if (annotatedElement == null) {
            return new ArrayList<UserType>();
        } else {
            Secured secured = annotatedElement.getAnnotation(Secured.class);
            if (secured == null) {
                return new ArrayList<UserType>();
            } else {
                UserType[] allowedRoles = secured.value();
                return Arrays.asList(allowedRoles);
            }
        }
    }

    /**
     * Check if the user contains one of the allowed roles. Throws an Exception if the user has not
     * permission to execute the method.
     * 
     * @param user         The user, if any.
     * @param allowedRoles The allowed roles.
     * @throws Exception
     */
    private void checkPermissions(User user, List<UserType> allowedRoles) throws Exception {
        if (allowedRoles.size() > 0) {
            if (user == null) {
                throw new NotAuthorizedException("Login required.");
            } else if (!allowedRoles.contains(user.getUserType())) {
                throw new ForbiddenException();
            }
        }
    }

    /**
     * Is the Authorization header is valid?
     * 
     * <p>
     * It must not be null and must be prefixed with "Bearer" plus a whitespace. The authentication
     * scheme comparison must be case-insensitive.
     * </p>
     * 
     * @param authorizationHeader The full HTTP-Authorization-Header.
     * @return Is the Authorization header is valid?
     */
    private boolean isTokenBasedAuthentication(String authorizationHeader) {
        return authorizationHeader != null && authorizationHeader.toLowerCase()
                .startsWith(AUTHENTICATION_SCHEME.toLowerCase() + " ");
    }

    /**
     * Abort the filter chain with a 401 status code response.
     * 
     * <p>
     * The WWW-Authenticate header is sent along with the response.
     * </p>
     * 
     * @param requestContext The request context touse.
     */
    private void abortWithUnauthorized(ContainerRequestContext requestContext) {
        requestContext
                .abortWith(
                        Response.status(Response.Status.UNAUTHORIZED)
                                .header(HttpHeaders.WWW_AUTHENTICATE,
                                        AUTHENTICATION_SCHEME + " realm=\"" + REALM + "\"")
                                .build());
    }

    /**
     * Validate the given JSON Web Token.
     * 
     * @param token The JSON Web Token to check.
     * @return The associated user, if any.
     * @throws Exception
     */
    private User validateToken(String token) throws Exception {
        User user = null;
        Jws<Claims> jws = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token);
        Claims claims = jws.getBody();
        Date expirationDate = claims.getExpiration();
        String issuer = claims.getIssuer();
        // int refreshLimit = claims.get("refreshLimit", Integer.class);
        // int refreshCount = claims.get("refreshCount", Integer.class);
        if (expirationDate.compareTo(Date.from(Instant.now())) > 0
                && issuer.equals(this.uriInfo.getBaseUri().toString()) // && refreshLimit >
                                                                       // refreshCount
        ) {
            String email = claims.getSubject();
            try (JumbleUnitOfWork uow = JumbleRestApplicationInit.createUnitOfWork()) {
                user = uow.getUserRepository().getByEmail(email);
            }
        }
        return user;
    }

    /**
     * Create a JSON Web Token.
     * 
     * @param uriInfo The {@link UriInfo URI info} to use as reference.
     * @param email   The email to use as reference.
     * @return A JSON Web Token.
     */
    public static String createToken(UriInfo uriInfo, String email) {
        Map<String, Object> additionalClaims = new HashMap<>();
        additionalClaims.put("refreshLimit", 10);
        additionalClaims.put("refreshCount", 0);
        String token = Jwts.builder() // builder
                .setSubject(email) // subject (email)
                .setIssuer(uriInfo.getBaseUri().toString()) // issuer
                .setIssuedAt(new Date()) // issued date
                .setExpiration(TimeUtils.toDate(LocalDateTime.now().plusMinutes(15L))) // expiration
                                                                                       // date
                .addClaims(additionalClaims) // additional claims
                .signWith(JumbleAuthenticationFilter.getKey()) // key
                .compact();
        return token;
    }

    // endregion Methods

}
