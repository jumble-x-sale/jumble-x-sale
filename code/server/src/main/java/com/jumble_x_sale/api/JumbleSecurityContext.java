package com.jumble_x_sale.api;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserType;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

public class JumbleSecurityContext implements SecurityContext {

    // region Fields

    private SecurityContext oldSecurityContext;
    private User user;
    private JumblePrincipal principal;

    // endregion Fields

    // region Getters

    public User getUser() {
        return user;
    }

    /**
     * {@inheritDoc}
     */
    public Principal getUserPrincipal() {
        return principal;
    }

    // endregion Getters

    // region Constructors

    public JumbleSecurityContext(SecurityContext oldSecurityContext, User user) {
        this.oldSecurityContext = oldSecurityContext;
        this.user = user;
        this.principal = new JumblePrincipal(user == null ? "" : user.getEmail());
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean isUserInRole(String role) {
        if (this.user != null) {
            return this.user.getUserType() == UserType.tryValueOf(role);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public boolean isSecure() {
        return this.oldSecurityContext.isSecure();
    }

    /**
     * {@inheritDoc}
     */
    public String getAuthenticationScheme() {
        return JumbleAuthenticationFilter.AUTHENTICATION_SCHEME;
    }

    // endregion Methods

}
