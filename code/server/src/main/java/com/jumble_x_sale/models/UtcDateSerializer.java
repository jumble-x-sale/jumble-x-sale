package com.jumble_x_sale.models;

import com.jumble_x_sale.util.TimeUtils;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.time.LocalDate;

/**
 * Serializer for {@link LocalDate UTC dates}.
 */
public class UtcDateSerializer implements JsonbSerializer<LocalDate>, JsonbDeserializer<LocalDate> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(LocalDate obj, JsonGenerator generator, SerializationContext ctx) {
        generator.write(TimeUtils.UTC_DATE.format(obj));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalDate deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        return LocalDate.parse(parser.getString(), TimeUtils.UTC_DATE);
    }

}
