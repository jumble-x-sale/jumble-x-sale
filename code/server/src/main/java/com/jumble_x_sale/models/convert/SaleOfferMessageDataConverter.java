package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.SaleOfferMessageData;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.Utils;

/**
 * Converter for {@link SaleOfferMessage sale offer messages} and its
 * {@link SaleOfferMessageData data class}.
 */
public class SaleOfferMessageDataConverter extends DataConverter<SaleOfferMessage, SaleOfferMessageData> {

    // region Constructors

    public SaleOfferMessageDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean validForEntityCreation(SaleOfferMessageData data) {
        return data.getSaleOfferId() != null && data.getSenderId() != null && !Utils.isNullOrEmpty(data.getMessage());
    }

    /**
     * {@inheritDoc}
     */
    public SaleOfferMessage updateEntity(SaleOfferMessageData data) {
        SaleOfferMessage entity = this.uow.getSaleOfferMessageRepository().getById(data.getId());
        if (entity != null) {
            if (updateValue(entity.getSaleOffer(), data.getSaleOfferId())) {
                SaleOffer saleOffer = this.uow.getSaleOfferRepository().getById(data.getSaleOfferId());
                entity.setSaleOffer(saleOffer);
            }
            if (updateValue(entity.getSender().getId(), data.getSenderId())) {
                User sender = this.uow.getUserRepository().getById(data.getSenderId());
                entity.setSender(sender);
            }
            if (updateValue(entity.getCreationInstant(), data.getCreationInstant())) {
                entity.setCreationInstant(data.getCreationInstant());
            }
            if (updateValue(entity.getMessage(), data.getMessage())) {
                entity.setMessage(data.getMessage());
            }
        }
        return entity;
    }

    public SaleOfferMessageData createData(SaleOfferMessage entity) {
        SaleOfferMessageData data = new SaleOfferMessageData();
        data.setId(entity.getId());
        data.setSaleOfferId(entity.getSaleOffer().getId());
        data.setSenderId(entity.getSender().getId());
        data.setCreationInstant(entity.getCreationInstant());
        data.setMessage(entity.getMessage());
        return data;
    }

    // endregion Methods

}
