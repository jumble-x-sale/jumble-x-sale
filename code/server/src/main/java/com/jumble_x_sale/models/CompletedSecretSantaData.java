package com.jumble_x_sale.models;

import java.time.Instant;
import java.util.UUID;

import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

/**
 * CompletedSale contains information about a SaleOffer and its buyer.
 */
public class CompletedSecretSantaData extends Data {

    // region Fields

    private static final long serialVersionUID = 7507990506551631899L;

    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant completionInstant;
    private UUID creatorId1;
    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant creationInstant1;
    private String category1;
    private String title1;
    private String description1;
    private UUID creatorId2;
    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant creationInstant2;
    private String category2;
    private String title2;
    private String description2;

    // endregion Fields

    // region Getters

    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCompletionInstant() {
        return this.completionInstant;
    }

    public UUID getCreatorId1() {
        return this.creatorId1;
    }

    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCreationInstant1() {
        return this.creationInstant1;
    }

    public String getCategory1() {
        return this.category1;
    }

    public String getTitle1() {
        return this.title1;
    }

    public String getDescription1() {
        return this.description1;
    }

    public UUID getCreatorId2() {
        return this.creatorId2;
    }

    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCreationInstant2() {
        return this.creationInstant2;
    }

    public String getCategory2() {
        return this.category2;
    }

    public String getTitle2() {
        return this.title2;
    }

    public String getDescription2() {
        return this.description2;
    }

    // endregion Getters

    // region Setters

    // @JsonDeserialize(using = InstantDeserializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCompletionInstant(Instant value) {
        this.completionInstant = value;
    }

    public void setCreatorId1(UUID value) {
        this.creatorId1 = value;
    }

    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCreationInstant1(Instant creationInstant1) {
        this.creationInstant1 = creationInstant1;
    }

    public void setCategory1(String value) {
        this.category1 = value;
    }

    public void setTitle1(String value) {
        this.title1 = value;
    }

    public void setDescription1(String value) {
        this.description1 = value;
    }

    public void setCreatorId2(UUID value) {
        this.creatorId2 = value;
    }

    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCreationInstant2(Instant creationInstant2) {
        this.creationInstant2 = creationInstant2;
    }

    public void setCategory2(String value) {
        this.category2 = value;
    }

    public void setTitle2(String value) {
        this.title2 = value;
    }

    public void setDescription2(String value) {
        this.description2 = value;
    }

    // endregion Setters

}
