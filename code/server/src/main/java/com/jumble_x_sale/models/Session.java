package com.jumble_x_sale.models;

import com.jumble_x_sale.api.JumbleAuthenticationFilter;

import java.io.Serializable;
import java.util.UUID;

import javax.ws.rs.core.UriInfo;

public class Session implements Serializable {

    // region Fields

    private static final long serialVersionUID = -3542569329581580782L;

    private UUID userId;
    private String accessToken;

    // endregion Fields

    // region Getters

    public UUID getUserId() {
        return userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    // endregion Getters

    // region Setters

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    // endregion Setters

    // region Constructors

    public Session(User user, UriInfo uriInfo) {
        this.userId = user.getId();
        this.accessToken = JumbleAuthenticationFilter.createToken(uriInfo, user.getEmail());
    }

    // endregion Constructors

}
