package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.util.Utils;
import com.jumble_x_sale.util.Iterables;
import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.models.Data;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;

import java.util.Arrays;
import java.util.ArrayList;

/**
 * A base class for converters between entity classes and data classes.
 * 
 * @param <E> The entity class.
 * @param <D> The data class.
 */
public abstract class DataConverter<E extends Entity, D extends Data> {

    // region Fields

    protected final JumbleUnitOfWork uow;

    // endregion Fields

    // region Constructors

    protected DataConverter(JumbleUnitOfWork uow) {
        this.uow = uow;
    }

    // endregion Constructors

    // region Methods

    /**
     * Is the data valid for creating an entity from it?
     * 
     * @param data The dat to check.
     * @return Is the data valid for creating an entity from it?
     */
    public boolean validForEntityCreation(D data) {
        // Default behaviour disallows creation.
        return false;
    }

    /**
     * Update the entity with the data.
     * 
     * @param data THe data to update the entity with.
     * @return The updated entity.
     */
    public E updateEntity(D data) {
        // Default behaviour disallows update.
        return null;
    }

    /**
     * Create a data object from an entity.
     * 
     * @param entity The entity to create the data object from.
     * @return A data object from an entity.
     */
    public abstract D createData(E entity);

    /**
     * Should the value be updated?
     * 
     * @param oldValue The old value.
     * @param newValue The new value.
     * @return Should the value be updated?
     */
    protected static boolean updateValue(Object oldValue, Object newValue) {
        if (newValue == null) {
            return false;
        }
        if (oldValue == null) {
            return true;
        }
        return !oldValue.equals(newValue);
    }

    /**
     * Should the value be updated?
     * 
     * @param oldValue The old string value.
     * @param newValue The new string value.
     * @return Should the value be updated?
     */
    protected static boolean updateValue(String oldValue, String newValue) {
        if (Utils.isNullOrWhiteSpace(newValue)) {
            return false;
        }
        if (Utils.isNullOrWhiteSpace(oldValue)) {
            return true;
        }
        return !oldValue.equals(newValue);
    }

    /**
     * Should the value be updated?
     * 
     * @param oldValues The old string values.
     * @param newValues The new string values.
     * @return Should the value be updated?
     */
    protected static boolean updateValue(String[] oldValues, String[] newValues) {
        if (newValues == null) {
            return false;
        }
        if (oldValues == null) {
            return true;
        }
        Arrays.sort(oldValues);
        Arrays.sort(newValues);
        return Iterables.any(Iterables.join(Arrays.asList(oldValues), Arrays.asList(newValues),
                false, true, false));
    }

    /**
     * Creates a joined array without duplicates.
     * 
     * The arrays have to be sorted before the call. Usage of method
     * {@link #updateValue(String[], String[]) updateValue} will assure that.
     * 
     * @param oldValues The first array.
     * @param newValues The first array.
     * @return A joined array without duplicates.
     */
    protected static String[] joined(String[] oldValues, String[] newValues) {
        Arrays.sort(oldValues);
        Arrays.sort(newValues);
        ArrayList<String> list = new ArrayList<>();
        for (String value : Iterables.join(Arrays.asList(oldValues), Arrays.asList(newValues), true,
                true, true)) {
            list.add(value);
        }
        return (String[]) list.toArray();
    }

    // endregion Methods

}
