package com.jumble_x_sale.models;

import java.util.UUID;

public class CompletedSaleMessageData extends MessageData {

    // region Fields

    private static final long serialVersionUID = -3168396719989438586L;

    private UUID completedSaleId = null;

    // endregion Fields

    // region Getters

    public UUID getCompletedSaleId() {
        return this.completedSaleId;
    }

    // endregion Getters

    // region Setters

    public void setCompletedSaleId(UUID completedSaleId) {
        this.completedSaleId = completedSaleId;
    }

    // endregion Setters

}
