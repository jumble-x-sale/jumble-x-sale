package com.jumble_x_sale.models;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Serializer for {@link OffsetDateTime offset date times}.
 */
public class OffsetDateTimeSerializer
        implements JsonbSerializer<OffsetDateTime>, JsonbDeserializer<OffsetDateTime> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(OffsetDateTime obj, JsonGenerator generator, SerializationContext ctx) {
        generator.write(obj.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OffsetDateTime deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        return OffsetDateTime.parse(parser.getString(), DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

}
