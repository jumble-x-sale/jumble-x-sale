package com.jumble_x_sale.models;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.UUID;
import java.util.Base64;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

public class SecretSantaOfferData extends Data {

    // region Fields

    private static final long serialVersionUID = 4319285403762168936L;

    private UUID creatorId;
    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant creationInstant;
    private String category;
    private String title;
    private String description;
    private String[] imageUrls;
    @JsonbProperty("mainImageData")
    private String mainImageDataBase64;

    // endregion Fields

    // region Getters

    public UUID getCreatorId() {
        return this.creatorId;
    }

    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getCategory() {
        return this.category;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String[] getImageUrls() {
        return this.imageUrls;
    }

    public String getMainImageDataBase64() {
        return this.mainImageDataBase64;
    }

    public byte[] getMainImageDataBin() {
        return this.mainImageDataBase64 == null ? null
                : Base64.getDecoder().decode(this.mainImageDataBase64);
    }

    // endregion Getters

    // region Setters

    public void setCreatorId(UUID creatorId) {
        this.creatorId = creatorId;
    }

    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCreationInstant(Instant creationInstant) {
        this.creationInstant = creationInstant;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setImageUrls(String[] imageUrls) {
        this.imageUrls = imageUrls;
    }

    public void setMainImageDataBase64(String mainImageDataBase64) {
        this.mainImageDataBase64 = mainImageDataBase64;
    }

    public void setMainImageDataBin(byte[] mainImageData) {
        this.mainImageDataBase64 = mainImageData == null ? null
                : new String(Base64.getEncoder().encode(mainImageData), StandardCharsets.UTF_8);
    }

    // endregion Setters

}
