package com.jumble_x_sale.models;

import java.time.Instant;

public class UserData extends Data {

    // region Fields

    private static final long serialVersionUID = 821462392396150821L;

    private Instant lastLoginDateTime = null;
    private String firstName = null;
    private String lastName = null;
    private String password = null;
    private String email = null;
    private String idCard = null;

    // endregion Fields

    // region Getters

    public Instant getLastLoginDateTime() {
        return lastLoginDateTime;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return this.email;
    }

    public String getIdCard() {
        return this.idCard;
    }

    // endregion Getters

    // region Setters

    public void setLastLoginDateTime(Instant lastLoginDateTime) {
        this.lastLoginDateTime = lastLoginDateTime;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    // endregion Setters

}
