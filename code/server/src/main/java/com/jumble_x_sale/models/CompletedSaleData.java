package com.jumble_x_sale.models;

import java.time.Instant;
import java.math.BigDecimal;
import java.util.UUID;

import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

/**
 * CompletedSale contains information about a SaleOffer and its buyer.
 */
public class CompletedSaleData extends Data {

    // region Fields

    private static final long serialVersionUID = -7610094233341955847L;

    private UUID creatorId;
    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant creationInstant;
    private String category;
    private String title;
    private String description;
    private String[] imageUrls;
    private BigDecimal price;
    private UUID buyerId;
    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant completionInstant;

    // endregion Fields

    // region Getters

    public UUID getCreatorId() {
        return this.creatorId;
    }

    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getCategory() {
        return this.category;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String[] getImageUrls() {
        return this.imageUrls;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public UUID getBuyerId() {
        return this.buyerId;
    }

    // @JsonSerialize(using = InstantSerializer.class)
    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCompletionInstant() {
        return this.completionInstant;
    }

    // endregion Getters

    // region Setters

    public void setCreatorId(UUID value) {
        this.creatorId = value;
    }

    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCreationInstant(Instant value) {
        this.creationInstant = value;
    }

    public void setCategory(String value) {
        this.category = value;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public void setImageUrls(String[] value) {
        this.imageUrls = value;
    }

    public void setPrice(BigDecimal value) {
        this.price = value;
    }

    public void setBuyerId(UUID value) {
        this.buyerId = value;
    }

    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCompletionInstant(Instant value) {
        this.completionInstant = value;
    }

    // endregion Setters

}
