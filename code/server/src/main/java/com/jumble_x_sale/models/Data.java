package com.jumble_x_sale.models;

import java.io.Serializable;
import java.util.UUID;

/**
 * A base class for plain data objects.
 */
public abstract class Data implements Serializable {

    // region Fields

    private static final long serialVersionUID = -5515329766200296360L;

    private UUID id;

    // endregion Fields

    // region Getters

    public UUID getId() {
        return this.id;
    }

    // endregion Getters

    // region Setters

    public void setId(UUID id) {
        this.id = id;
    }

    // endregion Setters

}
