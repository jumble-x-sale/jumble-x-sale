package com.jumble_x_sale.models;

import java.util.UUID;

public class SaleOfferMessageData extends MessageData {

    // region Fields

    private static final long serialVersionUID = 2878380067647358261L;

    private UUID saleOfferId = null;

    // endregion Fields

    // region Getters

    public UUID getSaleOfferId() {
        return this.saleOfferId;
    }

    // endregion Getters

    // region Setters

    public void setSaleOfferId(UUID saleOfferId) {
        this.saleOfferId = saleOfferId;
    }

    // endregion Setters

}
