package com.jumble_x_sale.models;

import com.jumble_x_sale.util.TimeUtils;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.time.LocalTime;

/**
 * Serializer for {@link LocalTime UTC times}.
 */
public class UtcTimeSerializer implements JsonbSerializer<LocalTime>, JsonbDeserializer<LocalTime> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(LocalTime obj, JsonGenerator generator, SerializationContext ctx) {
        generator.write(TimeUtils.UTC_TIME.format(obj));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LocalTime deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        return LocalTime.parse(parser.getString(), TimeUtils.UTC_TIME);
    }

}
