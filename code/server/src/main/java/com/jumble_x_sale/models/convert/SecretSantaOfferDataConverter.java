package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.ItemInfo;
import com.jumble_x_sale.models.SecretSantaOffer;
import com.jumble_x_sale.models.SecretSantaOfferData;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.Utils;

/**
 * Converter for {@link SecretSantaOffer secret santa offer} and its {@link SecretSantaOfferData
 * data class}.
 */
public class SecretSantaOfferDataConverter
        extends DataConverter<SecretSantaOffer, SecretSantaOfferData> {

    // region Constructors

    public SecretSantaOfferDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean validForEntityCreation(SecretSantaOfferData data) {
        return data.getCreatorId() != null && !Utils.isNullOrEmpty(data.getCategory())
                && !Utils.isNullOrEmpty(data.getTitle())
                && !Utils.isNullOrEmpty(data.getDescription());
    }

    /**
     * {@inheritDoc}
     */
    public SecretSantaOffer updateEntity(SecretSantaOfferData data) {
        SecretSantaOffer entity = this.uow.getSecretSantaOfferRepository().getById(data.getId());
        if (entity != null) {
            ItemInfo info = entity.getInfo();
            if (updateValue(info.getCreator().getId(), data.getCreatorId())) {
                User creator = this.uow.getUserRepository().getById(data.getCreatorId());
                info.setCreator(creator);
            }
            if (updateValue(info.getCreationInstant(), data.getCreationInstant())) {
                info.setCreationInstant(data.getCreationInstant());
            }
            if (updateValue(info.getCategory(), data.getCategory())) {
                info.setCategory(data.getCategory());
            }
            if (updateValue(info.getTitle(), data.getTitle())) {
                info.setTitle(data.getTitle());
            }
            if (updateValue(info.getDescription(), data.getDescription())) {
                info.setDescription(data.getDescription());
            }
            if (updateValue(info.getImageUrls(), data.getImageUrls())) {
                info.setImageUrls(data.getImageUrls());
            }
            if (updateValue(info.getMainImageData(), data.getMainImageDataBin())) {
                info.setMainImageData(data.getMainImageDataBin());
            }
        }
        return entity;
    }

    public SecretSantaOfferData createData(SecretSantaOffer entity) {
        SecretSantaOfferData data = new SecretSantaOfferData();
        ItemInfo info = entity.getInfo();
        data.setId(entity.getId());
        data.setCreatorId(info.getCreator().getId());
        data.setCreationInstant(info.getCreationInstant());
        data.setCategory(info.getCategory());
        data.setTitle(info.getTitle());
        data.setDescription(info.getDescription());
        data.setImageUrls(info.getImageUrls());
        data.setMainImageDataBin(info.getMainImageData());
        return data;
    }

    // endregion Methods

}
