package com.jumble_x_sale.models;

import java.math.BigDecimal;

public class SaleOfferData extends SecretSantaOfferData {

    // region Fields

    private static final long serialVersionUID = 6302424990740550513L;

    private BigDecimal price;

    // endregion Fields

    // region Getters

    public BigDecimal getPrice() {
        return this.price;
    }

    // endregion Getters

    // region Setters

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    // endregion Setters

}
