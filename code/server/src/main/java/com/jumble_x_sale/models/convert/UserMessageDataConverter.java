package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserData;
import com.jumble_x_sale.models.UserMessage;
import com.jumble_x_sale.models.UserMessageData;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.Utils;

/**
 * Converter for {@link User user} and its {@link UserData data class}.
 */
public class UserMessageDataConverter extends DataConverter<UserMessage, UserMessageData> {

    // region Constructors

    public UserMessageDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean validForEntityCreation(UserMessageData data) {
        return data.getSenderId() != null && data.getReceiverId() != null && !Utils.isNullOrEmpty(data.getMessage());
    }

    /**
     * {@inheritDoc}
     */
    public UserMessage updateEntity(UserMessageData data) {
        UserMessage entity = this.uow.getUserMessageRepository().getById(data.getId());
        if (entity != null) {
            if (updateValue(entity.getReceiver(), data.getReceiverId())) {
                User receiver = this.uow.getUserRepository().getById(data.getReceiverId());
                entity.setReceiver(receiver);
            }
            if (updateValue(entity.getSender().getId(), data.getSenderId())) {
                User sender = this.uow.getUserRepository().getById(data.getSenderId());
                entity.setSender(sender);
            }
            if (updateValue(entity.getCreationInstant(), data.getCreationInstant())) {
                entity.setCreationInstant(data.getCreationInstant());
            }
            if (updateValue(entity.getMessage(), data.getMessage())) {
                entity.setMessage(data.getMessage());
            }
        }
        return entity;
    }

    public UserMessageData createData(UserMessage entity) {
        UserMessageData data = new UserMessageData();
        data.setId(entity.getId());
        data.setReceiverId(entity.getReceiver().getId());
        data.setSenderId(entity.getSender().getId());
        data.setCreationInstant(entity.getCreationInstant());
        data.setMessage(entity.getMessage());
        return data;
    }

    // endregion Methods

}
