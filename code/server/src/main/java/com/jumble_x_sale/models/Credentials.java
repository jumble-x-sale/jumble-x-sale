package com.jumble_x_sale.models;

import java.io.Serializable;

public class Credentials implements Serializable {

    // region Fields

    private static final long serialVersionUID = -2581431825128746824L;

    private String email;
    private String password;

    // endregion Fields

    // region Getters

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    // endregion Getters

    // region Setters

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // endregion Setters

}
