package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.PricedItemInfo;
import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferData;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.Utils;

/**
 * Converter for {@link SaleOffer saleOffer} and its {@link SaleOfferData data class}.
 */
public class SaleOfferDataConverter extends DataConverter<SaleOffer, SaleOfferData> {

    // region Constructors

    public SaleOfferDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean validForEntityCreation(SaleOfferData data) {
        return data.getCreatorId() != null && !Utils.isNullOrEmpty(data.getCategory())
                && !Utils.isNullOrEmpty(data.getTitle())
                && !Utils.isNullOrEmpty(data.getDescription()) && data.getPrice() != null;
    }

    /**
     * {@inheritDoc}
     */
    public SaleOffer updateEntity(SaleOfferData data) {
        SaleOffer entity = this.uow.getSaleOfferRepository().getById(data.getId());
        if (entity != null) {
            PricedItemInfo info = entity.getInfo();
            if (updateValue(info.getCreator().getId(), data.getCreatorId())) {
                User creator = this.uow.getUserRepository().getById(data.getCreatorId());
                info.setCreator(creator);
            }
            if (updateValue(info.getCreationInstant(), data.getCreationInstant())) {
                info.setCreationInstant(data.getCreationInstant());
            }
            if (updateValue(info.getCategory(), data.getCategory())) {
                info.setCategory(data.getCategory());
            }
            if (updateValue(info.getTitle(), data.getTitle())) {
                info.setTitle(data.getTitle());
            }
            if (updateValue(info.getDescription(), data.getDescription())) {
                info.setDescription(data.getDescription());
            }
            if (updateValue(info.getImageUrls(), data.getImageUrls())) {
                info.setImageUrls(data.getImageUrls());
            }
            if (updateValue(info.getMainImageData(), data.getMainImageDataBin())) {
                info.setMainImageData(data.getMainImageDataBin());
            }
            if (updateValue(info.getPrice(), data.getPrice())) {
                info.setPrice(data.getPrice());
            }
        }
        return entity;
    }

    /**
     * {@inheritDoc}
     */
    public SaleOfferData createData(SaleOffer entity) {
        SaleOfferData data = new SaleOfferData();
        PricedItemInfo info = entity.getInfo();
        data.setId(entity.getId());
        data.setCreatorId(info.getCreator().getId());
        data.setCreationInstant(info.getCreationInstant());
        data.setCategory(info.getCategory());
        data.setTitle(info.getTitle());
        data.setDescription(info.getDescription());
        data.setImageUrls(info.getImageUrls());
        data.setMainImageDataBin(info.getMainImageData());
        data.setPrice(info.getPrice());
        return data;
    }

    // endregion Methods

}
