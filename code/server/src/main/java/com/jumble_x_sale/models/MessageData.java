package com.jumble_x_sale.models;

import java.time.Instant;
import java.util.UUID;

import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;

public abstract class MessageData extends Data {

    // region Fields

    private static final long serialVersionUID = -2650786411159397339L;

    private UUID senderId = null;
    @JsonbTypeSerializer(value = InstantSerializer.class)
    @JsonbTypeDeserializer(value = InstantSerializer.class)
    private Instant creationInstant = null;
    private String message = null;

    // endregion Fields

    // region Getters

    public UUID getSenderId() {
        return this.senderId;
    }

    @JsonbTypeSerializer(value = InstantSerializer.class)
    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getMessage() {
        return this.message;
    }

    // endregion Getters

    // region Setters

    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }

    @JsonbTypeDeserializer(value = InstantSerializer.class)
    public void setCreationInstant(Instant creationInstant) {
        this.creationInstant = creationInstant;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // endregion Setters

}
