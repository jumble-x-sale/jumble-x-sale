package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.ItemInfo;
import com.jumble_x_sale.models.CompletedSecretSanta;
import com.jumble_x_sale.models.CompletedSecretSantaData;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;

/**
 * Converter for {@link CompletedSecretSanta completed secret santa} and its
 * {@link CompletedSecretSantaData data class}.
 */
public class CompletedSecretSantaDataConverter extends DataConverter<CompletedSecretSanta, CompletedSecretSantaData> {

    // region Constructors

    public CompletedSecretSantaDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletedSecretSantaData createData(CompletedSecretSanta entity) {
        CompletedSecretSantaData data = new CompletedSecretSantaData();
        ItemInfo match1 = entity.getMatch1();
        ItemInfo match2 = entity.getMatch2();
        data.setId(entity.getId());
        data.setCompletionInstant(entity.getCompletionInstant());
        data.setCreatorId1(match1.getCreator().getId());
        data.setCreationInstant1(match1.getCreationInstant());
        data.setCategory1(match1.getCategory());
        data.setTitle1(match1.getTitle());
        data.setDescription1(match1.getDescription());
        data.setCreatorId2(match2.getCreator().getId());
        data.setCreationInstant2(match2.getCreationInstant());
        data.setCategory2(match2.getCategory());
        data.setTitle2(match2.getTitle());
        data.setDescription2(match2.getDescription());
        return data;
    }

    // endregion Methods

}
