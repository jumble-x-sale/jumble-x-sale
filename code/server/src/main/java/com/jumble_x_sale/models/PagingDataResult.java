package com.jumble_x_sale.models;

import com.jumble_x_sale.data.util.PagingOptions;

import javax.json.bind.annotation.JsonbPropertyOrder;

import java.util.List;

@JsonbPropertyOrder(value = {"options", "data"})
public final class PagingDataResult<D extends Data> {

    // region Fields

    private PagingOptions options;
    private List<D> data;

    // endregion Fields

    // region Getters

    public PagingOptions getOptions() {
        return options;
    }

    public List<D> getData() {
        return this.data;
    }

    // endregion Getters

    // region Setters

    public void setOptions(PagingOptions options) {
        this.options = options;
    }

    public void setData(List<D> data) {
        this.data = data;
    }

    // endregion Setters

    // region Constructors

    public PagingDataResult(PagingOptions options, List<D> data) {
        this.options = options;
        this.data = data;
    }

    // endregion Constructors

}
