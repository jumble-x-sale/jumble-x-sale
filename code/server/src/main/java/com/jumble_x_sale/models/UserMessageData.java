package com.jumble_x_sale.models;

import java.util.UUID;

public class UserMessageData extends MessageData {

    // region Fields

    private static final long serialVersionUID = 7935264550011007880L;

    private UUID receiverId = null;

    // endregion Fields

    // region Getters

    public UUID getReceiverId() {
        return this.receiverId;
    }

    // endregion Getters

    // region Setters

    public void setReceiverId(UUID receiverId) {
        this.receiverId = receiverId;
    }

    // endregion Setters

}
