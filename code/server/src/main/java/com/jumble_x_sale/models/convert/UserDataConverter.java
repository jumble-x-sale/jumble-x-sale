package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserData;
import com.jumble_x_sale.models.IdCard;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.Utils;

/**
 * Converter for {@link User user} and its {@link UserData data class}.
 */
public class UserDataConverter extends DataConverter<User, UserData> {

    // region Constructors

    public UserDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public boolean validForEntityCreation(UserData data) {
        return !Utils.isNullOrEmpty(data.getEmail()) && !Utils.isNullOrEmpty(data.getFirstName())
                && !Utils.isNullOrEmpty(data.getLastName())
                && !Utils.isNullOrEmpty(data.getIdCard())
                && !Utils.isNullOrEmpty(data.getPassword());
    }

    /**
     * {@inheritDoc}
     */
    public User updateEntity(UserData data) {
        User entity = this.uow.getUserRepository().getById(data.getId());
        if (entity != null) {
            if (updateValue(entity.getLastLoginDateTime(), data.getLastLoginDateTime())) {
                entity.setLastLoginDateTime(data.getLastLoginDateTime());
            }
            if (updateValue(entity.getFirstName(), data.getFirstName())) {
                entity.setFirstName(data.getFirstName());
            }
            if (updateValue(entity.getLastName(), data.getLastName())) {
                entity.setLastName(data.getLastName());
            }
            if (updateValue(entity.getEmail(), data.getEmail())) {
                entity.setEmail(data.getEmail());
            }
            if (updateValue(entity.getIdCard(), data.getIdCard())) {
                entity.setIdCard(new IdCard(data.getIdCard()));
            }
        }
        return entity;
    }

    public UserData createData(User entity) {
        UserData data = new UserData();
        data.setId(entity.getId());
        data.setLastLoginDateTime(entity.getLastLoginDateTime());
        data.setFirstName(entity.getFirstName());
        data.setLastName(entity.getLastName());
        data.setEmail(entity.getEmail());
        data.setIdCard(entity.getIdCard().getIdString());
        return data;
    }

    // endregion Methods

}
