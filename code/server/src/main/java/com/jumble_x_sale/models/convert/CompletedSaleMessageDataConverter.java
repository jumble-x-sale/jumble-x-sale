package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.CompletedSaleMessage;
import com.jumble_x_sale.models.CompletedSaleMessageData;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;

/**
 * Converter for {@link CompletedSaleMessage completed sale messages} and its
 * {@link CompletedSaleMessageData data class}.
 */
public class CompletedSaleMessageDataConverter extends DataConverter<CompletedSaleMessage, CompletedSaleMessageData> {

    // region Constructors

    public CompletedSaleMessageDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public CompletedSaleMessageData createData(CompletedSaleMessage entity) {
        CompletedSaleMessageData data = new CompletedSaleMessageData();
        data.setId(entity.getId());
        data.setCompletedSaleId(entity.getCompletedSale().getId());
        data.setSenderId(entity.getSender().getId());
        data.setCreationInstant(entity.getCreationInstant());
        data.setMessage(entity.getMessage());
        return data;
    }

    // endregion Methods

}
