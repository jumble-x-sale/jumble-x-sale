package com.jumble_x_sale.models;

import com.jumble_x_sale.util.TimeUtils;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.time.Instant;

/**
 * Serializer for {@link Instant instants}.
 */
public class InstantSerializer implements JsonbSerializer<Instant>, JsonbDeserializer<Instant> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(Instant obj, JsonGenerator generator, SerializationContext ctx) {
        generator.write(TimeUtils.format(obj));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Instant deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
        return TimeUtils.parseInstant(parser.getString());
    }

}
