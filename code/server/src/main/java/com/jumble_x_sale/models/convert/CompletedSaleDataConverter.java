package com.jumble_x_sale.models.convert;

import com.jumble_x_sale.models.PricedItemInfo;

import com.jumble_x_sale.models.CompletedSale;
import com.jumble_x_sale.models.CompletedSaleData;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;

/**
 * Converter for {@link CompletedSale completed sale} and its
 * {@link CompletedSaleData data class}.
 */
public class CompletedSaleDataConverter extends DataConverter<CompletedSale, CompletedSaleData> {

    // region Constructors

    public CompletedSaleDataConverter(JumbleUnitOfWork uow) {
        super(uow);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public CompletedSaleData createData(CompletedSale entity) {
        CompletedSaleData data = new CompletedSaleData();
        PricedItemInfo info = entity.getInfo();
        data.setId(entity.getId());
        data.setCreatorId(info.getCreator().getId());
        data.setCreationInstant(info.getCreationInstant());
        data.setCategory(info.getCategory());
        data.setTitle(info.getTitle());
        data.setDescription(info.getDescription());
        data.setImageUrls(info.getImageUrls());
        data.setPrice(info.getPrice());
        data.setBuyerId(entity.getBuyer().getId());
        data.setCompletionInstant(entity.getCompletionInstant());
        return data;
    }

    // endregion Methods

}
