package com.jumble_x_sale.util;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for class {@link Iterables Iterables}.
 */
public class IterablesTest {

    List<String> noElements;
    List<String> oneElement;
    List<String> twoElements;

    @Before
    public void runBefore() {
        this.noElements = new ArrayList<String>();
        this.oneElement = new ArrayList<String>();
        this.oneElement.add("A");
        this.twoElements = new ArrayList<String>();
        this.twoElements.add("A");
        this.twoElements.add("B");
    }

    /**
     * Test the method {@code Iterables.firstOrDefault}.
     */
    @Test
    public void firstOrDefaultTest() {
        assertTrue(Iterables.firstOrDefault(this.noElements, "X").equals("X"));
        assertTrue(Iterables.firstOrDefault(this.oneElement, "X").equals("A"));
        assertTrue(Iterables.firstOrDefault(this.twoElements, "X").equals("A"));
    }

    /**
     * Test the method {@code Iterables.firstOrDefault}.
     */
    @Test
    public void singleOrDefaultTest() {
        assertTrue(Iterables.singleOrDefault(this.noElements, "X").equals("X"));
        assertTrue(Iterables.singleOrDefault(this.oneElement, "X").equals("A"));
        assertTrue(Iterables.singleOrDefault(this.twoElements, "X").equals("X"));
    }

}
