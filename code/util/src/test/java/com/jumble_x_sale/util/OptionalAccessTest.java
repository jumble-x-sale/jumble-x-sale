package com.jumble_x_sale.util;

import java.util.List;
import java.util.Arrays;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class OptionalAccessTest {

    @Test
    public void test() {
        OptionalAccess<String> access = new OptionalAccess<>(Arrays.asList("A", "XBC", "DEF"),
                (value) -> !value.contains("X"));
        List<String> list = Utils.toList(access);
        assertTrue(list.size() == 2);
        assertTrue(list.get(0).equals("A"));
        assertTrue(list.get(1).equals("DEF"));
    }

}
