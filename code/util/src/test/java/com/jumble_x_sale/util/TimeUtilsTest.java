package com.jumble_x_sale.util;

import java.time.LocalDateTime;
import java.time.LocalDate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit test for {@link TimeUtils TimeUtils}.
 */
public class TimeUtilsTest {

    private static final String DateTime_String = "2019-12-13T10:20:30.123Z";
    private static final String Date_String = "2019-12-13Z";

    private static final LocalDateTime DateTime_Local =
            LocalDateTime.of(2019, 12, 13, 10, 20, 30, 123000000);
    private static final LocalDate Date_Local = LocalDate.of(2019, 12, 13);

    @Test
    public void test_toDateTime() {
        LocalDateTime localDateTime = LocalDateTime.parse(DateTime_String, TimeUtils.UTC_DATE_TIME);
        assertEquals(localDateTime, DateTime_Local);
    }

    @Test
    public void test_fromDateTime() {
        String string = TimeUtils.UTC_DATE_TIME.format(DateTime_Local);
        assertEquals(string, DateTime_String);
    }

    @Test
    public void test_toDate() {
        LocalDate localDate = LocalDate.parse(Date_String, TimeUtils.UTC_DATE);
        assertEquals(localDate, Date_Local);
    }

    @Test
    public void test_fromDate() {
        String string = TimeUtils.UTC_DATE.format(Date_Local);
        assertEquals(string, Date_String);
    }

}
