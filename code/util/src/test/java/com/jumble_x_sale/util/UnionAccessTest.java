package com.jumble_x_sale.util;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.assertTrue;

public class UnionAccessTest {

    @Test
    public void test() {
        UnionAccess<String> access = new UnionAccess<String>(Arrays.asList("A", "BC"), Arrays.asList("DEF", "GH"),
                Arrays.asList("IJ", "KLM"));
        List<String> list = Utils.toList(access);
        assertTrue(list.size() == 6);
        assertTrue(list.get(0).equals("A"));
        assertTrue(list.get(1).equals("BC"));
        assertTrue(list.get(2).equals("DEF"));
        assertTrue(list.get(3).equals("GH"));
        assertTrue(list.get(4).equals("IJ"));
        assertTrue(list.get(5).equals("KLM"));
    }

}
