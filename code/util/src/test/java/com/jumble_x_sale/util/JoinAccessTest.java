package com.jumble_x_sale.util;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JoinAccessTest {

    // region Fields

    private List<Character> first;
    private List<Character> second;

    // endregion Fields

    // region Methods

    @Before
    public void runBefore() {
        this.first = Arrays.asList('A', 'B', 'C', 'D');
        this.second = Arrays.asList('C', 'D', 'E', 'F');
    }

    @Test
    public void testFirst() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, true, false, false));

        assertEquals(2, result.size());
        assertEquals((Character) 'A', result.get(0));
        assertEquals((Character) 'B', result.get(1));
    }

    @Test
    public void testSecond() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, false, true, false));

        assertEquals(2, result.size());
        assertEquals((Character) 'E', result.get(0));
        assertEquals((Character) 'F', result.get(1));
    }

    @Test
    public void testBoth() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, false, false, true));

        assertEquals(2, result.size());
        assertEquals((Character) 'C', result.get(0));
        assertEquals((Character) 'D', result.get(1));
    }

    @Test
    public void testFirstAndSecond() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, true, true, false));

        assertEquals(4, result.size());
        assertEquals((Character) 'A', result.get(0));
        assertEquals((Character) 'F', result.get(3));
    }

    @Test
    public void testFirstAndBoth() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, true, false, true));

        assertEquals(4, result.size());
        assertEquals((Character) 'A', result.get(0));
        assertEquals((Character) 'D', result.get(3));
    }

    @Test
    public void testSecondAndBoth() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, false, true, true));

        assertEquals(4, result.size());
        assertEquals((Character) 'C', result.get(0));
        assertEquals((Character) 'F', result.get(3));
    }

    @Test
    public void testFull() {
        List<Character> result = Utils.toList(new JoinAccess<Character>(this.first, this.second, true, true, true));

        assertEquals(6, result.size());
        assertEquals((Character) 'A', result.get(0));
        assertEquals((Character) 'F', result.get(5));
    }

    // endregion Methods

}
