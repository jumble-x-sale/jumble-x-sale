package com.jumble_x_sale.util;

import java.util.*;

import org.junit.*;
import static org.junit.Assert.assertTrue;

public class InvertedAccessTest {

    @Test
    public void test() {
        InvertedAccess<String> access = new InvertedAccess<>(Arrays.asList("A", "BC", "DEF", "GH", "IJ", "KLM"));
        List<String> list = Utils.toList(access);
        assertTrue(list.size() == 6);
        assertTrue(list.get(0).equals("KLM"));
        assertTrue(list.get(1).equals("IJ"));
        assertTrue(list.get(2).equals("GH"));
        assertTrue(list.get(3).equals("DEF"));
        assertTrue(list.get(4).equals("BC"));
        assertTrue(list.get(5).equals("A"));
    }

}
