package com.jumble_x_sale.util;

import java.util.UUID;
import java.util.Iterator;
import java.util.Set;
import java.util.EnumSet;
import java.util.List;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Function;
import java.util.regex.Pattern;

/**
 * Utilities for basis data types.
 */
public final class Utils {

    // region Fields

    private static final DecimalFormat decimalFormat =
            new DecimalFormat("#,##0.0#", new DecimalFormatSymbols() {
                private static final long serialVersionUID = -2776986655372157730L;
                {
                    setGroupingSeparator(',');
                    setDecimalSeparator('.');
                }
            }) {
                private static final long serialVersionUID = -3866236653749253712L;

                {
                    setParseBigDecimal(true);
                }
            };

    // endregion Fields

    // region Constructors

    private Utils() {
    }

    // endregion Constructors

    @SuppressWarnings("unchecked")
    public static <T> T cast(Class<T> class_, Object value) {
        if (class_.isAssignableFrom(class_)) {
            return (T) value;
        }
        return null;
    }

    public static <S, T extends Comparable<T>> T max(Iterable<S> source, Function<S, T> converter,
            Comparator<T> comparator) {
        T targetItem = null;
        Iterator<S> sourceItor = source.iterator();
        if (sourceItor.hasNext()) {
            targetItem = converter.apply(sourceItor.next());
            while (sourceItor.hasNext()) {
                T tempItem = converter.apply(sourceItor.next());
                if (comparator.compare(targetItem, tempItem) > 0) {
                    tempItem = targetItem;
                }
            }
        }
        return targetItem;
    }

    public static <S, T extends Comparable<T>> T min(Iterable<S> source, Function<S, T> converter,
            Comparator<T> comparator) {
        T targetItem = null;
        Iterator<S> sourceItor = source.iterator();
        if (sourceItor.hasNext()) {
            targetItem = converter.apply(sourceItor.next());
            while (sourceItor.hasNext()) {
                T tempItem = converter.apply(sourceItor.next());
                if (comparator.compare(targetItem, tempItem) < 0) {
                    tempItem = targetItem;
                }
            }
        }
        return targetItem;
    }

    /**
     * Try to get an enum value from an id string.
     * 
     * @param <E>          The type of enum to get.
     * @param enumType     The actual enum type to get.
     * @param id           The id string to get an enum value from.
     * @param defaultValue The default enum value.
     * @return An enum value from an id string.
     */
    public static <E extends Enum<E>> E tryValueOf(Class<E> enumType, String id, E defaultValue) {
        if (id != null) {
            try {
                return Enum.valueOf(enumType, id);
            } catch (IllegalArgumentException e) {
            }
        }
        return defaultValue;
    }

    /**
     * Get an enum set from a long value.
     * 
     * @param <E>       The conrete enum type.
     * @param flagsType The flag type.
     * @param value     The long value to get the flags set from.
     * @return An enum set from a long value.
     */
    public static <E extends Enum<E> & Flags<E>> EnumSet<E> getFlags(Class<E> flagsType,
            long value) {
        EnumSet<E> flags = EnumSet.noneOf(flagsType);
        for (E enumValue : flagsType.getEnumConstants()) {
            long flagValue = enumValue.getValue();
            if ((flagValue & value) == flagValue) {
                flags.add(enumValue);
            }
        }
        return flags;
    }

    /**
     * Get a long value from an enum set.
     * 
     * @param <E>       The conrete enum type.
     * @param flagsType The flag type.
     * @param flags     The flags set to get the value from.
     * @return A long value from an enum set.
     */
    public static <E extends Enum<E> & Flags<E>> long getValue(Class<E> flagsType, Set<E> flags) {
        long value = 0;
        for (E enumValue : flagsType.getEnumConstants()) {
            value |= enumValue.getValue();
        }
        return value;
    }

    /**
     * Get the first non-null value from the arguments.
     * 
     * @param <T>    The type of values.
     * @param first  The first value in question.
     * @param others The other values in question.
     * @return The first non-null value from the arguments.
     */
    @SafeVarargs
    public static <T> T coalesce(T first, T... others) {
        for (int i = 0; i < others.length && first == null; i++) {
            first = others[i];
        }
        return first;
    }

    /**
     * Get the first non-null value from the arguments.
     * 
     * @param first  The first value in question.
     * @param others The other values in question.
     * @return The first non-null value from the arguments.
     */
    public static String coalesce(String first, String... others) {
        for (int i = 0; i < others.length && isNullOrEmpty(first); i++) {
            first = others[i];
        }
        return first;
    }

    /**
     * Get the first non-null value from the arguments.
     * 
     * @param values The values in question.
     * @return The first non-null value from the arguments.
     */
    public static String coalesce(String[] values) {
        String result = null;
        if (values.length > 0) {
            result = values[0];
            for (int i = 1; i < values.length && isNullOrEmpty(result); i++) {
                result = values[i];
            }
        }
        return result;
    }

    /**
     * Get the first non-null value from the arguments.
     * 
     * @param values The values in question.
     * @return The first non-null value from the arguments.
     */
    public static String coalesce(List<String> values) {
        String result = null;
        if (values.size() > 0) {
            result = values.get(0);
            for (int i = 1; i < values.size() && isNullOrEmpty(result); i++) {
                result = values.get(i);
            }
        }
        return result;
    }

    /**
     * Create a {@link List list} from the given {@link Iterable iterable}.
     * 
     * @param <T>    The type of elements in the {@link List list} and {@link Iterable iterable}.
     * @param values The {@link Iterable iterable} the create the list from.
     * @return A {@link List list} from the given {@link Iterable iterable}.
     */
    public static <T> List<T> toList(Iterable<T> values) {
        List<T> result = new ArrayList<>();
        for (T value : values) {
            result.add(value);
        }
        return result;
    }

    /**
     * Get the substring of a given string beginning at the given start index and ending at the
     * given end index.
     * 
     * @param value      The string to get a substring from.
     * @param startIndex The index where the substring starts. Negative indexes are measured
     *                   relative to the end of the given string.
     * @param endIndex   The index where the substring ends. Negative indexes are measured relative
     *                   to the end of the given string.
     * @return The substring of a given string beginning at the given index with the given length.
     */
    public static String substring(String value, int startIndex, int endIndex) {
        if (startIndex < 0) {
            startIndex += value.length();
        }
        if (endIndex < 0) {
            endIndex += value.length();
        }
        return value.substring(startIndex, endIndex);
    }

    /**
     * Get the substring of a given string beginning at the given index with the given length.
     * 
     * @param value      The string to get a substring from.
     * @param startIndex The index where the substring starts. Negative indexes are measured
     *                   relative to the end of the given string.
     * @param length     The length of the substring.
     * @return The substring of a given string beginning at the given index with the given length.
     */
    public static String substr(String value, int startIndex, int length) {
        if (startIndex < 0) {
            startIndex += value.length();
        }
        return value.substring(startIndex, startIndex + length);
    }

    /**
     * Get the substring of a given string beginning at the given index until the end of the given
     * string.
     * 
     * @param value      The string to get a substring from.
     * @param startIndex The index whre the substring starts. Negative indexes are measured relative
     *                   to the end of the given string.
     * @return The substring of a given string beginning at the given index until the end of the
     *         given string.
     */
    public static String substr(String value, int startIndex) {
        int endIndex = value.length() - 1;
        if (startIndex < 0) {
            startIndex += endIndex + 1;
        }
        return value.substring(startIndex, endIndex);
    }

    /**
     * Get the string with the first letter upper case.
     * 
     * @param value The string to capitalize
     * @return The string with the first letter upper case.
     */
    public static String capitalize(String value) {
        if (!isNullOrWhiteSpace(value)) {
            return Character.toUpperCase(value.charAt(0)) + value.substring(1);
        }
        return null;
    }

    /**
     * Get the string with the first letter lower case.
     * 
     * @param value The string to uncapitalize
     * @return The string with the first letter lower case.
     */
    public static String uncapitalize(String value) {
        if (!isNullOrWhiteSpace(value)) {
            return Character.toLowerCase(value.charAt(0)) + value.substring(1);
        }
        return null;
    }

    /**
     * Is the string null or empty?
     * 
     * @param value The string to test.
     * @return Is the string null or empty?
     */
    public static boolean isNullOrEmpty(String value) {
        if (value == null) {
            return true;
        }
        if (value.isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Is the string null, empty or whitespace?
     * 
     * @param value The string to test.
     * @return Is the string null or empty?
     */
    public static boolean isNullOrWhiteSpace(String value) {
        if (value == null) {
            return true;
        }
        if (value.isEmpty()) {
            return true;
        }
        if (value.trim().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Scale an int number down to a new given range.
     * 
     * @param value  The value to scale for the given range.
     * @param newMin The new minimum value for scaling.
     * @param newMax The new maximum value for scaling.
     * @param oldMin The old minimum value for scaling.
     * @param oldMax The old maximum value for scaling.
     * @return The scaled int number value.
     */
    public static int scale(int value, int newMin, int newMax, int oldMin, int oldMax) {
        final long newRange = (long) newMax - (long) newMin;
        final long oldRange = (long) oldMax - (long) oldMin;
        final long result = (newRange * ((long) value - (long) oldMin) / oldRange) + newMin;
        return (int) result;
    }

    /**
     * Scale an int number down to a new given range.
     * 
     * @param value  The value to scale for the given range.
     * @param newMin The new minimum value for scaling.
     * @param newMax The new maximum value for scaling.
     * @return The scaled int number value.
     */
    public static int scale(int value, int newMin, int newMax) {
        return scale(value, newMin, newMax, Integer.MIN_VALUE, Integer.MAX_VALUE);
    }

    /**
     * Are the two objects equal?
     * 
     * @param first  The first object to test.
     * @param second The second object to test.
     * @return Are the two objects equal?
     */
    public static boolean equals(Object first, Object second) {
        if (first == null) {
            return second == null;
        }
        return first.equals(second);
    }

    /**
     * Are the two strings equal?
     * 
     * @param first  The first string to test.
     * @param second The second string to test.
     * @return Are the two strings equal?
     */
    public static boolean equalsIgnoreCase(String first, String second) {
        if (first == null) {
            return second == null;
        }
        return first.equalsIgnoreCase(second);
    }

    /**
     * Compare two objects.
     * 
     * @param first  The first object to compare.
     * @param second The second object to compare.
     * @return An integer stating the relation of the two objects.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T>> int compareTo(Object first, T second) {
        try {
            return ((T) first).compareTo(second);
        } catch (ClassCastException e) {
        }
        return 0;
    }

    /**
     * Is the the test value between the lower and the upper bound?
     * 
     * @param first  The lower bound.
     * @param second The upper bound.
     * @return Is the the test value between the lower and the upper bound?
     */
    public static <T extends Comparable<T>> boolean between(T value, T lowerBound, T upperBound) {
        return value.compareTo(lowerBound) >= 0 && value.compareTo(upperBound) <= 0;
    }

    /**
     * Is the the test value in between the lower and the upper bound?
     * 
     * @param first  The lower bound.
     * @param second The upper bound.
     * @return Is the the test value in between the lower and the upper bound?
     */
    public static <T extends Comparable<T>> boolean inBetween(T value, T lowerBound, T upperBound) {
        return value.compareTo(lowerBound) >= 0 && value.compareTo(upperBound) < 0;
    }

    /**
     * Is the the test value between the lower and the upper bound?
     * 
     * @param first  The lower bound.
     * @param second The upper bound.
     * @return Is the the test value between the lower and the upper bound?
     */
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T>> boolean between(Object value, T lowerBound,
            T upperBound) {
        try {
            return between((T) value, lowerBound, upperBound);
        } catch (ClassCastException e) {
        }
        return false;
    }

    /**
     * Is the the test value in between the lower and the upper bound?
     * 
     * @param first  The lower bound.
     * @param second The upper bound.
     * @return Is the the test value in between the lower and the upper bound?
     */
    @SuppressWarnings("unchecked")
    public static <T extends Comparable<T>> boolean inBetween(Object value, T lowerBound,
            T upperBound) {
        try {
            return inBetween((T) value, lowerBound, upperBound);
        } catch (ClassCastException e) {
        }
        return false;
    }

    /**
     * Matches the text the given pattern?
     * 
     * @param text    The text to test.
     * @param pattern The pattern to use.
     * @return Matches the text the given pattern?
     */
    public static boolean like(String text, String pattern) {
        return Pattern.compile(pattern.replace(".", "\\.").replace("?", ".").replace("%", ".*"),
                Pattern.CASE_INSENSITIVE).matcher(text).matches();
    }

    /**
     * Creates a {@code UUID} from the string standard representation as described in the
     * {@link #toString} method.
     *
     * @param value A string that specifies a {@code UUID}
     * @return A {@code UUID} with the specified value or null when that fails.
     */
    public static UUID uuidFromString(String value) {
        UUID uuid;
        try {
            uuid = UUID.fromString(value);
        } catch (IllegalArgumentException e) {
            uuid = null;
        }
        return uuid;
    }

    /**
     * Format a {@link BigDecimal big decimal} as {@link String string}.
     *
     * @param value The number to format.
     * @return A {@link BigDecimal big decimal} as {@link String string}.
     */
    public static String format(BigDecimal value) {
        return decimalFormat.format(value);
    }

    /**
     * Parse a {@link BigDecimal big decimal}.
     *
     * @param value The text to parse.
     * @return A {@link BigDecimal big decimal}.
     */
    public static BigDecimal parseBigDecimal(String value) {
        BigDecimal bigDecimal = null;
        try {
            bigDecimal = (BigDecimal) decimalFormat.parse(value);
        } catch (ParseException pe) {
        }
        return bigDecimal;
    }

    /**
     * Divide a long value and ceil the result.
     * 
     * @param dividend The dividend.
     * @param divisor  The divisor.
     * @return The quotient.
     */
    public static long divideCeil(long dividend, long divisor) {
        return dividend / divisor + ((dividend % divisor == 0) ? 0 : 1);
    }

    /**
     * Divide a long value and ceil the result.
     * 
     * @param dividend The dividend.
     * @param divisor  The divisor.
     * @return The quotient.
     */
    public static long divideCeil(long dividend, int divisor) {
        if (divisor != 0) {
            return dividend / divisor + ((dividend % divisor == 0) ? 0 : 1);
        }
        return 0;
    }

}
