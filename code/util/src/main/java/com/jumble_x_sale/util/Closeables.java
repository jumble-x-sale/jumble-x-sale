package com.jumble_x_sale.util;

/**
 * Extends {@link AutoCloseable auto closeables} with status checks and optional
 * status ignorance.
 */
public abstract class Closeables {

    // region Constructors

    private Closeables() {
    }

    // endregion Constructors

    // region Methods

    /**
     * Try to close the resources, ignore any exceptions.
     * 
     * @param <C>        The closeable type.
     * @param closeables The resources to close.
     */
    public static <C extends CheckedAutoCloseable> void tryClose(Iterable<C> closeables) {
        for (CheckedAutoCloseable closeable : closeables) {
            closeable.tryClose();
        }
    }

    /**
     * Close the resources.
     * 
     * @param <C>        The closeable type.
     * @param closeables The resources to close.
     * @throws Exception When failing to close the resources.
     */
    public static <C extends AutoCloseable> void close(Iterable<C> closeables) throws Exception {
        for (AutoCloseable closeable : closeables) {
            closeable.close();
        }
    }

    // endregion Methods

}
