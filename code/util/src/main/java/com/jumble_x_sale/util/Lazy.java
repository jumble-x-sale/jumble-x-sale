package com.jumble_x_sale.util;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * A container that lazily initializes its content.
 */
public final class Lazy<T> {

    private Supplier<T> supplier;
    private T value;

    /**
     * Creates a container that lazily initializes its content.
     * 
     * @param supplier The function that supplies the content.
     */
    public Lazy(Supplier<T> supplier) {
        Objects.requireNonNull(supplier);
        this.supplier = supplier;
    }

    /**
     * Get the content.
     * 
     * @return The content.
     */
    public T get() {
        if (this.value != null) {
            return this.value;
        }
        synchronized (this) {
            if (this.value != null) {
                return this.value;
            }
            this.value = supplier.get();
        }
        return this.value;
    }

}
