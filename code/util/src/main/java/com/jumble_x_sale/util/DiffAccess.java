package com.jumble_x_sale.util;

import java.util.Iterator;

/**
 * 
 * @param <T> The type elements.
 */
public final class DiffAccess<T extends Comparable<T>> implements Iterable<DiffResult<T>>, Iterator<DiffResult<T>> {

    // region Fields

    private final Iterator<T> firstItor;
    private final Iterator<T> secondItor;
    private T firstValue;
    private T secondValue;
    private DiffResult<T> currentValue;

    // endregion Fields

    // region Constructors

    /**
     * Create a new comparison access.
     * 
     * @param first  The first iterator. Its elements have to be already sorted.
     * @param second THe second iterator. Its elements have to be already sorted.
     */
    private DiffAccess(Iterable<T> first, Iterable<T> second) {
        this.firstItor = first.iterator();
        this.secondItor = second.iterator();
        this.firstValue = this.firstItor.hasNext() ? this.firstItor.next() : null;
        this.secondValue = this.secondItor.hasNext() ? this.secondItor.next() : null;
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public Iterator<DiffResult<T>> iterator() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasNext() {
        this.currentValue = null;

        while (this.firstValue != null || this.secondValue != null) {
            int cmp = this.firstValue == null ? 1 : this.firstValue.compareTo(this.secondValue);
            if (cmp < 0) {
                this.currentValue = new DiffResult<T>(cmp, firstValue);
                this.firstValue = this.firstItor.hasNext() ? this.firstItor.next() : null;
                break;
            } else if (cmp > 0) {
                this.currentValue = new DiffResult<T>(cmp, secondValue);
                this.secondValue = this.secondItor.hasNext() ? this.secondItor.next() : null;
                break;
            } else {
                this.currentValue = new DiffResult<T>(cmp, secondValue);
                this.firstValue = this.firstItor.hasNext() ? this.firstItor.next() : null;
                this.secondValue = this.secondItor.hasNext() ? this.secondItor.next() : null;
                break;
            }
        }

        return this.currentValue != null;
    }

    /**
     * {@inheritDoc}
     */
    public DiffResult<T> next() {
        return this.currentValue;
    }

    // endregion Methods

}
