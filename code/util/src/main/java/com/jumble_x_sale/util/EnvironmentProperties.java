package com.jumble_x_sale.util;

import java.util.Properties;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;

/**
 * Holds properties for a specific target execution environment.
 * 
 * Allowed environment names are:
 * <ul>
 * <li>default: "-Ddefault"/"" means using "config-default.properties"</li>
 * <li>dev: "-Ddev" means using "config-dev.properties"</li>
 * <li>test: "-Dtest" means using "config-test.properties"</li>
 * <li>prod: "-Dprod" means using "config-prod.properties"</li>
 * </ul>
 */
public final class EnvironmentProperties extends Properties {

    // region Fields

    private static final long serialVersionUID = 6870439722044878464L;

    private final String environmentName;

    // endregion Fields

    // region Getters

    public String getEnvironmentName() {
        return this.environmentName;
    }

    // endregion Getters

    // region Constructors

    private EnvironmentProperties(String environmentName) {
        this.environmentName = environmentName;
    }

    // endregion Constructors

    // region Methods

    public static EnvironmentProperties get(Class<?> projectReferenceClass) {
        String environmentName;
        if (!Utils.isNullOrWhiteSpace(System.getProperty("default"))) {
            // For compilation.
            environmentName = "default";
        } else if (!Utils.isNullOrWhiteSpace(System.getProperty("dev"))) {
            // For compilation and testing in "dry dock".
            environmentName = "dev";
        } else if (!Utils.isNullOrWhiteSpace(System.getProperty("test"))) {
            // For QA testing.
            environmentName = "test";
        } else if (!Utils.isNullOrWhiteSpace(System.getProperty("prod"))) {
            // For use in production.
            environmentName = "prod";
        } else {
            environmentName = "default";
        }
        EnvironmentProperties properties = new EnvironmentProperties(environmentName);
        String propertiesFileName = MessageFormat.format("config-{0}.properties", environmentName);
        try {
            try (InputStream inputStream = projectReferenceClass.getClassLoader()
                    .getResourceAsStream(propertiesFileName)) {

                if (inputStream != null) {
                    properties.load(inputStream);
                }
            }
        } catch (IOException e) {
        }
        return properties;
    }

    // endregion Methods

}
