package com.jumble_x_sale.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that merges
 * one or more {@link Iterable iterables}.
 * 
 * @param <T> The concrete type of elements.
 */
public class UnionAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected List<Iterable<? extends T>> iterables;
    protected List<Iterator<? extends T>> iterators;
    protected int itorIdx;
    protected T next = null;

    // endregion Fields

    // region Constructors

    /**
     * A proxy {@link Iterable iterable} and {@link Iterator iterator} that merges
     * one or more iterable into one.
     * 
     * @param values The values of the original {@link Iterable iterable}.
     */
    public UnionAccess(List<Iterable<? extends T>> values) {
        this.iterables = values;
        this.iterators = new ArrayList<Iterator<? extends T>>(this.iterables.size());
        for (int i = 0; i < this.iterables.size(); i++) {
            this.iterators.add(this.iterables.get(i).iterator());
        }
    }

    /**
     * A proxy {@link Iterable iterable} that is qualified by a predicate.
     * 
     * @param values The values of the original {@link Iterable iterable}.
     */
    @SafeVarargs
    public UnionAccess(Iterable<? extends T>... values) {
        this(Arrays.asList(values));
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        for (int i = 0; i < this.iterables.size(); i++) {
            this.iterators.set(i, this.iterables.get(i).iterator());
        }
        this.itorIdx = 0;
        this.fetchNext();
    }

    /**
     * Fetches the next value.
     *
     * @return {@code true} if the iteration has more elements
     */
    private void fetchNext() {
        for (; this.itorIdx < this.iterators.size(); this.itorIdx++) {
            while (this.iterators.get(this.itorIdx).hasNext()) {
                this.next = this.iterators.get(this.itorIdx).next();
                return;
            }
        }
        this.next = null;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }

    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        if (this.next == null) {
            this.fetchNext();
            return this.next != null;
        }
        return true;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        if (this.next != null) {
            T current = this.next;
            this.fetchNext();
            return current;
        }
        throw new NoSuchElementException();
    }

    // endregion Getters

}
