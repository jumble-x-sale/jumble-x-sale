package com.jumble_x_sale.util;

import java.util.Iterator;

/**
 * Two sets joined to one {@link Iterable iterable} and {@link Iterator
 * iterator}. The two sets have to be sorted before joining!
 * 
 * @param <T> The type of items in the two sets.
 */
public final class JoinAccess<T extends Comparable<T>> implements Iterable<T>, Iterator<T> {

    // region Inner Classes

    /**
     * The operations the {@link JoinAccess join access} is able to perform.
     */
    public enum Operation {
        /**
         * A conjunctive union of two sets.
         * 
         * `first ∪ second`
         */
        UNION,
        /**
         * An intersection of two sets.
         * 
         * `first ∩ second`
         */
        INTERSECTION,
        /**
         * The difference of two sets. Also called the relative complement.
         * 
         * `first ∖ second`
         */
        FIRST_DIFFERENCE,
        /**
         * The difference of two sets. Also called the relative complement.
         * 
         * `second ∖ first`
         */
        SECOND_DIFFERENCE,
        /**
         * The symmetric difference of two sets. Also called the disjunctive union.
         * 
         * `(first ∖ second) ∪ (second ∖ first)`
         */
        SYMMETRIC_DIFFERENCE,
    }

    // endregion Inner Classes

    // region Fields

    private final Iterator<T> firstItor;
    private final Iterator<T> secondItor;
    private T firstValue;
    private T secondValue;
    private T currentValue;
    private final boolean onlyInFirst;
    private final boolean onlyInSecond;
    private final boolean inBoth;

    // endregion Fields

    // region Constructors

    /**
     * Create a new join access.
     * 
     * @param first        The first iterator. Its elements have to be already
     *                     sorted.
     * @param second       The second iterator. Its elements have to be already
     *                     sorted.
     * @param onlyInFirst  Only the items contained in the first iterable?
     * @param onlyInSecond Only the items contained in the second iterable?
     * @param inBoth       The items contained in both iterables?
     */
    public JoinAccess(Iterable<T> first, Iterable<T> second, boolean onlyInFirst, boolean onlyInSecond,
            boolean inBoth) {
        this.firstItor = first.iterator();
        this.secondItor = second.iterator();
        this.firstValue = this.firstItor.hasNext() ? this.firstItor.next() : null;
        this.secondValue = this.secondItor.hasNext() ? this.secondItor.next() : null;
        this.onlyInFirst = onlyInFirst;
        this.onlyInSecond = onlyInSecond;
        this.inBoth = inBoth;
    }

    /**
     * Create a new join access.
     * 
     * @param first     The first iterator. Its elements have to be already sorted.
     * @param second    The second iterator. Its elements have to be already sorted.
     * @param operation The set operation to execute.
     */
    public JoinAccess(Iterable<T> first, Iterable<T> second, Operation operation) {
        this(first, second,
                operation == Operation.UNION || operation == Operation.SYMMETRIC_DIFFERENCE
                        || operation == Operation.FIRST_DIFFERENCE,
                operation == Operation.UNION || operation == Operation.SYMMETRIC_DIFFERENCE
                        || operation == Operation.SECOND_DIFFERENCE,
                operation == Operation.UNION || operation == Operation.INTERSECTION);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    public Iterator<T> iterator() {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    public boolean hasNext() {
        this.currentValue = null;

        while (this.firstValue != null || this.secondValue != null) {
            int cmp = this.firstValue == null ? 1 : this.firstValue.compareTo(this.secondValue);
            if (cmp < 0) {
                T value = firstValue;
                this.firstValue = this.firstItor.hasNext() ? this.firstItor.next() : null;
                if (this.onlyInFirst) {
                    this.currentValue = value;
                    break;
                }
            } else if (cmp > 0) {
                T value = secondValue;
                this.secondValue = this.secondItor.hasNext() ? this.secondItor.next() : null;
                if (this.onlyInSecond) {
                    this.currentValue = value;
                    break;
                }
            } else {
                T value = secondValue;
                this.firstValue = this.firstItor.hasNext() ? this.firstItor.next() : null;
                this.secondValue = this.secondItor.hasNext() ? this.secondItor.next() : null;
                if (this.inBoth) {
                    this.currentValue = value;
                    break;
                }
            }
        }

        return this.currentValue != null;
    }

    /**
     * {@inheritDoc}
     */
    public T next() {
        return this.currentValue;
    }

    // endregion Methods

}
