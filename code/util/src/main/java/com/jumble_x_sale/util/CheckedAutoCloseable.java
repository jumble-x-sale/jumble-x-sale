package com.jumble_x_sale.util;

/**
 * Extends {@link AutoCloseable auto closeables} with status checks and optional
 * status ignorance.
 */
public interface CheckedAutoCloseable extends AutoCloseable {

    // region Methods

    /**
     * Is the resource still open?
     * 
     * @return Is the resource still open?
     */
    public boolean isOpen();

    /**
     * Try to close the resource, ignore any exceptions.
     */
    public default void tryClose() {
        if (this.isOpen()) {
            try {
                this.close();
            } catch (Exception e) {
            }
        }
    }

    // endregion Methods

}
