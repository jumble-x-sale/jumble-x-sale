package com.jumble_x_sale.util;

public final class DiffResult<T extends Comparable<T>> {

    // region Fields

    private final int diff;
    private final T value;

    // endregion Fields

    // region Getters

    /**
     * Get the meaning of the difference and its value.
     * 
     * <ul>
     * <li>-1 -&gt; The value was only found in the first iterable.</li>
     * <li>+1 -&gt; The value was only found in the second iterable.</li>
     * <li>0 -&gt; The value was found in both iterables.</li>
     * </ul>
     * 
     * @return The meaning of the difference and its value.
     */
    public int getDiff() {
        return this.diff;
    }

    public T getValue() {
        return this.value;
    }

    // endregion Getters

    // region Constructors

    public DiffResult(int diff, T value) {
        this.diff = diff;
        this.value = value;
    }

    // endregion Constructors

}
