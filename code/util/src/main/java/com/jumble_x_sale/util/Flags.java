package com.jumble_x_sale.util;

public interface Flags<E extends Enum<E>> {

    public long getValue();

}
