package com.jumble_x_sale.util;

import java.util.*;
// import java.util.function.*;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that provides
 * read-only access to its elements.
 * 
 * @param <T> The concrete type of elements.
 */
public class ReadOnlyAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected Iterable<? extends T> iterable;
    protected Iterator<? extends T> iterator;

    // endregion Fields

    // region Constructors

    /**
     * Create a proxy {@link Iterable iterable} and {@link Iterator iterator} that
     * provides read-only access to its elements.
     * 
     * @param values The values of the original {@link Iterable iterable}.
     */
    public ReadOnlyAccess(Iterable<? extends T> values) {
        this.iterable = values;
        this.iterator = null;
        this.reset();
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        this.iterator = this.iterable.iterator();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }

    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        return this.iterator.next();
    }

    // endregion Getters

}
