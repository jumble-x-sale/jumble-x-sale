package com.jumble_x_sale.util;

import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRules;
import java.util.Date;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.DateTimeException;
import java.time.Instant;
import java.time.LocalDate;

/**
 * Helper for time related data types.
 */
public abstract class TimeUtils {

    /**
     * The formatter for a UTC date.
     */
    public static final DateTimeFormatter UTC_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd'Z'");

    /**
     * The formatter for a UTC date time with milliseconds.
     */
    public static final DateTimeFormatter UTC_DATE_TIME =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    /**
     * The formatter for a UTC time with milliseconds.
     */
    public static final DateTimeFormatter UTC_TIME = DateTimeFormatter.ofPattern("HH:mm:ss.SSS'Z'");

    /**
     * The formatter for a local date.
     */
    public static final DateTimeFormatter LOCAL_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    /**
     * The formatter for a local date time with milliseconds.
     */
    public static final DateTimeFormatter LOCAL_DATE_TIME =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");

    /**
     * The formatter for a local time with milliseconds.
     */
    public static final DateTimeFormatter LOCAL_TIME = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");

    /**
     * The time zone id of germany.
     */
    public static final ZoneId ZoneId_Germany = ZoneId.of("Europe/Berlin");

    /**
     * The time zone rules for germany.
     */
    public static final ZoneRules ZoneRules_Germany = ZoneId_Germany.getRules();

    /**
     * Create a Date and Time helper.
     */
    private TimeUtils() {
    }

    /**
     * Today as UTC date.
     */
    public static LocalDate todayUtc() {
        return toUtcDate(Instant.now());
    }

    /**
     * Now as UTC date time.
     */
    public static LocalDateTime nowUtc() {
        return toUtcDateTime(Instant.now());
    }

    /**
     * Today as german date.
     */
    public static LocalDate todayGerman() {
        return nowGerman().toLocalDate();
    }

    /**
     * Now as german date time.
     */
    public static LocalDateTime nowGerman() {
        return LocalDateTime.ofInstant(Instant.now(), ZoneId_Germany);
    }

    /**
     * Convert an instant to a german date.
     * 
     * @param instant The instant to convert.
     */
    public static LocalDate toGermanDate(Instant instant) {
        return toGermanDateTime(instant).toLocalDate();
    }

    /**
     * Convert an instant to a german date time.
     * 
     * @param instant The instant to convert.
     */
    public static LocalDateTime toGermanDateTime(Instant instant) {
        return LocalDateTime.ofInstant(instant, ZoneRules_Germany.getOffset(instant));
    }

    /**
     * Convert an instant to a UTC date.
     * 
     * @param instant The instant to convert.
     */
    public static LocalDate toUtcDate(Instant instant) {
        return toUtcDateTime(instant).toLocalDate();
    }

    /**
     * Convert an instant to a local date time.
     * 
     * @param instant The instant to convert.
     */
    public static LocalDateTime toUtcDateTime(Instant instant) {
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
    }

    /**
     * Convert an instant to a UTC date.
     * 
     * @param instant The instant to convert.
     */
    public static Instant fromGermanDate(LocalDate date) {
        return fromGermanDateTime(date.atStartOfDay());
    }

    /**
     * Convert an instant to a local date time.
     * 
     * @param instant The instant to convert.
     */
    public static Instant fromGermanDateTime(LocalDateTime dateTime) {
        return dateTime.toInstant(ZoneRules_Germany.getOffset(dateTime));
    }

    /**
     * Convert an instant to a UTC date.
     * 
     * @param instant The instant to convert.
     */
    public static Instant fromUtcDate(LocalDate date) {
        return date.atStartOfDay().toInstant(ZoneOffset.UTC);
    }

    /**
     * Convert an instant to a local date time.
     * 
     * @param instant The instant to convert.
     */
    public static Instant fromUtcDateTime(LocalDateTime dateTime) {
        return dateTime.toInstant(ZoneOffset.UTC);
    }

    /**
     * Get an {@link OffsetDateTime} from a {@link LocalDateTime} and a {@link ZoneId}
     * 
     * @param dateTime The {@link LocalDateTime} to convert.
     * @param zoneId   The {@link ZoneId} used for conversion.
     * @return The resulting {@link OffsetDateTime}.
     */
    public static OffsetDateTime of(LocalDateTime dateTime, ZoneId zoneId) {
        return OffsetDateTime.of(dateTime, zoneId.getRules().getOffset(dateTime));
    }

    /**
     * Get an {@link OffsetDateTime} from a {@link LocalDateTime} using the system default
     * {@link ZoneId}.
     * 
     * @param dateTime The {@link LocalDateTime} to convert.
     * @return The resulting {@link OffsetDateTime}.
     */
    public static OffsetDateTime of(LocalDateTime dateTime) {
        return of(dateTime, ZoneOffset.systemDefault());
    }

    /**
     * Obtains an instance of {@code OffsetDateTime} from year, month, day, hour, minute, second and
     * nanosecond.
     * <p>
     * This returns a {@code OffsetDateTime} with the specified year, month, day-of-month, hour,
     * minute, second and nanosecond. The day must be valid for the year and month, otherwise an
     * exception will be thrown.
     *
     * @param year         the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month        the month-of-year to represent, from 1 (January) to 12 (December)
     * @param dayOfMonth   the day-of-month to represent, from 1 to 31
     * @param hour         the hour-of-day to represent, from 0 to 23
     * @param minute       the minute-of-hour to represent, from 0 to 59
     * @param second       the second-of-minute to represent, from 0 to 59
     * @param nanoOfSecond the nano-of-second to represent, from 0 to 999,999,999
     * @param zoneId       the zone id to use
     * @return the offset date-time, not null
     * @throws DateTimeException if the value of any field is out of range, or if the day-of-month
     *                           is invalid for the month-year
     */
    public static OffsetDateTime of(int year, int month, int dayOfMonth, int hour, int minute,
            int second, int nanoOfSecond, ZoneId zoneId) {
        return of(LocalDateTime.of(year, month, dayOfMonth, hour, minute, second, nanoOfSecond),
                zoneId);
    }

    /**
     * Obtains an instance of {@code OffsetDateTime} from year, month, day, hour, minute, second and
     * nanosecond.
     * <p>
     * This returns a {@code OffsetDateTime} with the specified year, month, day-of-month, hour,
     * minute, second and nanosecond. The day must be valid for the year and month, otherwise an
     * exception will be thrown.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, from 1 (January) to 12 (December)
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @param second     the second-of-minute to represent, from 0 to 59
     * @return the offset date-time, not null
     * @throws DateTimeException if the value of any field is out of range, or if the day-of-month
     *                           is invalid for the month-year
     */
    public static OffsetDateTime of(int year, int month, int dayOfMonth, int hour, int minute,
            int second) {
        return of(LocalDateTime.of(year, month, dayOfMonth, hour, minute, second));
    }

    /**
     * Obtains an instance of {@code OffsetDateTime} from year, month, day, hour, minute, second and
     * nanosecond.
     * <p>
     * This returns a {@code OffsetDateTime} with the specified year, month, day-of-month, hour,
     * minute, second and nanosecond. The day must be valid for the year and month, otherwise an
     * exception will be thrown.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, from 1 (January) to 12 (December)
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @return the offset date-time, not null
     * @throws DateTimeException if the value of any field is out of range, or if the day-of-month
     *                           is invalid for the month-year
     */
    public static OffsetDateTime of(int year, int month, int dayOfMonth) {
        return of(LocalDateTime.of(year, month, dayOfMonth, 0, 0));
    }

    /**
     * Get a {@link Date Date} from a {@link LocalDateTime LocalDateTime}.
     * 
     * @param localDateTime The {@link LocalDateTime LocalDateTime} to convert.
     * @return A {@link Date Date} from a {@link LocalDateTime LocalDateTime}.
     */
    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * Get a {@link LocalDateTime LocalDateTime} from a {@link Date Date}.
     * 
     * @param date The {@link Date Date} to convert.
     * @return A {@link LocalDateTime LocalDateTime} from a {@link Date Date}.
     */
    public static LocalDateTime toLocalDateTime(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    /**
     * Obtains an instance of {@code OffsetDateTime} from year, month, day, hour, minute, second and
     * nanosecond.
     * <p>
     * This returns a {@code OffsetDateTime} with the specified year, month, day-of-month, hour,
     * minute, second and nanosecond. The day must be valid for the year and month, otherwise an
     * exception will be thrown.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, from 1 (January) to 12 (December)
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @param second     the second-of-minute to represent, from 0 to 59
     * @return the offset date-time, not null
     * @throws DateTimeException if the value of any field is out of range, or if the day-of-month
     *                           is invalid for the month-year
     */
    public static Instant instantOfUtc(int year, int month, int dayOfMonth, int hour, int minute,
            int second) {
        return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second)
                .toInstant(ZoneOffset.UTC);
    }

    /**
     * Obtains an instance of {@code OffsetDateTime} from year, month, day, hour, minute, second and
     * nanosecond.
     * <p>
     * This returns a {@code OffsetDateTime} with the specified year, month, day-of-month, hour,
     * minute, second and nanosecond. The day must be valid for the year and month, otherwise an
     * exception will be thrown.
     *
     * @param year       the year to represent, from MIN_YEAR to MAX_YEAR
     * @param month      the month-of-year to represent, from 1 (January) to 12 (December)
     * @param dayOfMonth the day-of-month to represent, from 1 to 31
     * @param hour       the hour-of-day to represent, from 0 to 23
     * @param minute     the minute-of-hour to represent, from 0 to 59
     * @param second     the second-of-minute to represent, from 0 to 59
     * @return the offset date-time, not null
     * @throws DateTimeException if the value of any field is out of range, or if the day-of-month
     *                           is invalid for the month-year
     */
    public static Instant instantOfGerman(int year, int month, int dayOfMonth, int hour, int minute,
            int second) {
        LocalDateTime localDateTime =
                LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
        return localDateTime.toInstant(ZoneRules_Germany.getOffset(localDateTime));
    }

    /**
     * Format an {@link Instant instant} as text.
     * 
     * @param instant   The instant to format.
     * @param formatter The formatter to use.
     * @return An {@link Instant instant} as text.
     */
    public static String format(Instant instant, DateTimeFormatter formatter) {
        return formatter.format(LocalDateTime.ofEpochSecond(instant.getEpochSecond(),
                instant.getNano(), ZoneOffset.UTC));
    }

    /**
     * Format an {@link Instant Instant} as text.
     * 
     * @param instant The {@link Instant Instant} to format.
     * @return An {@link Instant Instant} as text.
     */
    public static String format(Instant instant) {
        return UTC_DATE_TIME.format(LocalDateTime.ofEpochSecond(instant.getEpochSecond(),
                instant.getNano(), ZoneOffset.UTC));
    }

    /**
     * Format an {@link LocalDate LocalDate} as text.
     * 
     * @param localDate The {@link LocalDate LocalDate} to format.
     * @return An {@link LocalDate LocalDate} as text.
     */
    public static String format(LocalDate localDate) {
        return LOCAL_DATE.format(localDate);
    }

    /**
     * Format an {@link LocalDateTime LocalDateTime} as text.
     * 
     * @param localDateTime The {@link LocalDateTime LocalDateTime} to format.
     * @return An {@link LocalDateTime LocalDateTime} as text.
     */
    public static String format(LocalDateTime localDateTime) {
        return LOCAL_DATE_TIME.format(localDateTime);
    }

    /**
     * Format an {@link LocalTime LocalTime} as text.
     * 
     * @param localTime The {@link LocalTime LocalTime} to format.
     * @return An {@link LocalTime LocalTime} as text.
     */
    public static String format(LocalTime localTime) {
        return LOCAL_TIME.format(localTime);
    }

    /**
     * Parse an {@link Instant Instant} from text.
     * 
     * @param text      The text to parse.
     * @param formatter The formatter to use.
     * @return an {@link Instant Instant} from text.
     */
    public static Instant parseInstant(String text, DateTimeFormatter formatter) {
        return LocalDateTime.parse(text, formatter).toInstant(ZoneOffset.UTC);
    }

    /**
     * Parse an {@link Instant Instant} from text.
     * 
     * @param text The text to parse.
     * @return an {@link Instant Instant} from text.
     */
    public static Instant parseInstant(String text) {
        return LocalDateTime.parse(text, UTC_DATE_TIME).toInstant(ZoneOffset.UTC);
    }

    /**
     * Parse a {@link LocalDate LocalDate} from text.
     * 
     * @param text The text to parse.
     * @return an {@link LocalDate LocalDate} from text.
     */
    public static LocalDate parseLocalDate(String text) {
        return LocalDate.parse(text, LOCAL_DATE);
    }

    /**
     * Parse a {@link LocalDateTime LocalDateTime} from text.
     * 
     * @param text The text to parse.
     * @return an {@link LocalDateTime LocalDateTime} from text.
     */
    public static LocalDateTime parseLocalDateTime(String text) {
        return LocalDateTime.parse(text, LOCAL_DATE_TIME);
    }

    /**
     * Parse a {@link LocalTime LocalTime} from text.
     * 
     * @param text The text to parse.
     * @return an {@link LocalTime LocalTime} from text.
     */
    public static LocalTime parseLocalTime(String text) {
        return LocalTime.parse(text, LOCAL_TIME);
    }

}
