package com.jumble_x_sale.util;

/**
 * Base implementation for {@link AutoCloseable auto closeable} classes with
 * finalizer.
 */
public abstract class CheckedAutoCloseableFinalBase extends CheckedAutoCloseableBase {

    // region Methods

    /**
     * Finalize this persistence context.
     */
    @Override
    protected void finalize() throws Throwable {
        if (this.isOpen()) {
            this.close();
        }
        // No reason to call the super method, it's empty anyway.
    }

    // endregion Methods

}
