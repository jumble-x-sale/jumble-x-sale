package com.jumble_x_sale.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that allows
 * access to the elements of a stream.
 * 
 * @param <T> The concrete type of elements.
 */
public class StreamAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected Stream<? extends T> stream;
    protected Iterator<? extends T> iterator;

    // endregion Fields

    // region Getters

    public Stream<? extends T> getStream() {
        return this.stream;
    }

    // endregion Getters

    // region Constructors

    /**
     * Create a proxy {@link Iterable iterable} and {@link Iterator iterator} that
     * allows access to the elements of a stream.
     * 
     * @param stream The {@code Stream stream} that contains the elements to access.
     */
    public StreamAccess(Stream<? extends T> stream) {
        this.stream = stream;
        this.iterator = null;
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        this.iterator = this.stream.iterator();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }

    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        return this.iterator.next();
    }

    // endregion Getters

}
