package com.jumble_x_sale.util;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

/**
 * A proxy {@link Iterable iterable} and {@link Iterator iterator} that filters
 * its elements by a {@link Predicate predicate}.
 * 
 * @param <T> The concrete type of elements.
 */
public class OptionalAccess<T> implements Iterable<T>, Iterator<T> {

    // region Fields

    protected Iterable<? extends T> iterable;
    protected Iterator<? extends T> iterator;
    protected Predicate<T> predicate;
    protected T next = null;

    // endregion Fields

    // region Constructors

    /**
     * Create a proxy {@link Iterable iterable} and {@link Iterator iterator} that
     * filters its elements by a {@link Predicate predicate}.
     * 
     * @param values    The values of the original {@link Iterable iterable}.
     * @param predicate The {@link Predicate predicate} by which to filter the
     *                  original {@link Iterable iterable}.
     */
    public OptionalAccess(Iterable<? extends T> values, Predicate<T> predicate) {
        this.iterable = values;
        this.predicate = predicate;
        this.iterator = null;
        this.reset();
    }

    // endregion Constructors

    // region Methods

    /**
     * Resets the iterator.
     *
     */
    private void reset() {
        this.iterator = this.iterable.iterator();
        this.fetchNext();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    public Iterator<T> iterator() {
        this.reset();
        return this;
    }

    /**
     * Fetches the next value.
     *
     * @return {@code true} if the iteration has more elements
     */
    private void fetchNext() {
        while (this.iterator.hasNext()) {
            T value = this.iterator.next();
            if (this.predicate.test(value)) {
                this.next = value;
                return;
            }
        }
        this.next = null;
    }

    // endregion Methods

    // region Getters

    /**
     * Returns {@code true} if the iteration has more elements. (In other words,
     * returns {@code true} if {@link #next} would return an element rather than
     * throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    public boolean hasNext() {
        if (this.next == null) {
            this.fetchNext();
            return this.next != null;
        }
        return true;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    public T next() {
        if (this.next != null) {
            T current = this.next;
            this.fetchNext();
            return current;
        }
        throw new NoSuchElementException();
    }

    // endregion Getters

}
