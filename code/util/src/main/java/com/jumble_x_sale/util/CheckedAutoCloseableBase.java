package com.jumble_x_sale.util;

/**
 * Base implementation for {@link CheckedAutoCloseable checked auto closeable}
 * classes.
 */
public abstract class CheckedAutoCloseableBase implements CheckedAutoCloseable {

    // region Fields

    protected boolean open = true;

    // endregion Fields

    // region Methods

    /**
     * Is the resource still open?
     * 
     * @return Is the resource still open?
     */
    public boolean isOpen() {
        return this.open;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws Exception {
        this.open = false;
    }

    // endregion Methods

}
