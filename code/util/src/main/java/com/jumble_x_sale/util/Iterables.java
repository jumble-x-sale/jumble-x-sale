package com.jumble_x_sale.util;

import java.util.Iterator;
import java.util.stream.Stream;

/**
 * {@link Iterable Iterable} and {@link Iterator iterator} helper.
 */
public abstract class Iterables {

    /**
     * Create a {@link Iterable Iterable} and {@link Iterator iterator} helper.
     */
    private Iterables() {
    }

    /**
     * Get the first value in an {@link Iterable iterable} or a default value.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param iterable     The {@link Iterable iterable} to check.
     * @param defaultValue The default value.
     * @return The first value in an {@link Iterable iterable} or a default value.
     */
    public static <T> T firstOrDefault(Iterable<T> iterable, T defaultValue) {
        for (T value : iterable) {
            return value;
        }
        return defaultValue;
    }

    /**
     * Get the first value in an {@link Iterable iterable} or null.
     *
     * @param <T>      The type of elements in the {@link Iterable iterable}.
     * @param iterable The {@link Iterable iterable} to check.
     * @return The first value in an {@link Iterable iterable} or null.
     */
    public static <T> T firstOrDefault(Iterable<T> iterable) {
        return firstOrDefault(iterable, null);
    }

    /**
     * Get the first value in an {@link Iterable iterable} or a default value.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param stream       The {@link Stream stream} to check.
     * @param defaultValue The default value.
     * @return The first value in an {@link Stream stream} or a default value.
     */
    public static <T> T firstOrDefault(Stream<T> stream, T defaultValue) {
        Iterator<T> iterator = stream.iterator();
        while (iterator.hasNext()) {
            return iterator.next();
        }
        return defaultValue;
    }

    /**
     * Get a single value in an {@link Iterable iterable} or a default value.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param iterable     The {@link Iterable iterable} to check.
     * @param defaultValue The default value.
     * @return A single value in an {@link Iterable iterable} or a default value.
     */
    public static <T> T singleOrDefault(Iterable<T> iterable, T defaultValue) {
        boolean valueTaken = false;
        T result = defaultValue;
        for (T value : iterable) {
            if (valueTaken) {
                return defaultValue;
            }
            result = value;
            valueTaken = true;
        }
        return result;
    }

    /**
     * Get a single value in a {@link Stream stream} or a default value.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param stream       The {@link Stream stream} to check.
     * @param defaultValue The default value.
     * @return A single value in an {@link Stream stream} or a default value.
     */
    public static <T> T singleOrDefault(Stream<T> stream, T defaultValue) {
        boolean valueTaken = false;
        T result = defaultValue;
        Iterator<T> iterator = stream.iterator();
        while (iterator.hasNext()) {
            if (valueTaken) {
                return defaultValue;
            }
            result = iterator.next();
            valueTaken = true;
        }
        return result;
    }

    /**
     * Has the {@link Iterable iterable} any value?
     *
     * @param <T>      The type of elements in the {@link Iterable iterable}.
     * @param iterable The {@link Iterable iterable} to check.
     * @return Has the {@link Iterable iterable} any value?
     */
    public static <T> boolean any(Iterable<T> iterable) {
        Iterator<T> iterator = iterable.iterator();
        return iterator.hasNext();
    }

    /**
     * Has the {@link Stream stream} any value?
     *
     * @param <T>    The type of elements in the {@link Iterable iterable}.
     * @param stream The {@link Stream stream} to check.
     * @return Has the {@link Stream stream} any value?
     */
    public static <T> boolean any(Stream<T> stream) {
        Iterator<T> iterator = stream.iterator();
        return iterator.hasNext();
    }

    /**
     * Join the two {@link Iterable iterables} and get the wanted set parts.
     *
     * @param <T>          The type of elements in the {@link Iterable iterable}.
     * @param first        The first iterator. Its elements have to be already
     *                     sorted!
     * @param second       The second iterator. Its elements have to be already
     *                     sorted!
     * @param onlyInFirst  Only the items contained in the first iterable?
     * @param onlyInSecond Only the items contained in the second iterable?
     * @param inBoth       The items contained in both iterables?
     * @return The wanted set parts of the two {@link Iterable iterables}.
     */
    public static <T extends Comparable<T>> Iterable<T> join(Iterable<T> first, Iterable<T> second, boolean onlyInFirst,
            boolean onlyInSecond, boolean inBoth) {
        return new JoinAccess<T>(first, second, onlyInFirst, onlyInSecond, inBoth);
    }

    /**
     * Join the two {@link Iterable iterables} and get the wanted set parts.
     *
     * @param <T>       The type of elements in the {@link Iterable iterable}.
     * @param first     The first iterator. Its elements have to be already sorted!
     * @param second    The second iterator. Its elements have to be already sorted!
     * @param operation The set operation to execute.
     * @return The wanted set parts of the two {@link Iterable iterables}.
     */
    public static <T extends Comparable<T>> Iterable<T> join(Iterable<T> first, Iterable<T> second,
            JoinAccess.Operation operation) {
        return new JoinAccess<T>(first, second, operation);
    }

}
