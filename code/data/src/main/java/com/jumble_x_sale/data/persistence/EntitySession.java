package com.jumble_x_sale.data.persistence;

import com.jumble_x_sale.util.CheckedAutoCloseableBase;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * Represents a generic session for working with a set of related entities.
 */
public final class EntitySession extends CheckedAutoCloseableBase {

    // region Fields

    protected final EntityManager manager;
    protected final EntityTransaction transaction;

    // endregion Fields

    // region Properties

    public EntityManager getManager() {
        return this.manager;
    }

    // endregion Properties

    // region Constructors

    /**
     * Create an abstract generic context for access to different kinds of entities.
     * 
     * @param context The context to which this session belongs to.
     */
    protected EntitySession(EntityContext context) {
        this.manager = context.factory.createEntityManager();
        this.transaction = this.manager.getTransaction();
    }

    // endregion Constructors

    // region Methods

    /**
     * Close this persistence context.
     */
    @Override
    public void close() throws Exception {
        this.rollback();
        this.manager.close();
        super.close();
    }

    /**
     * Begin the work.
     */
    public void begin() {
        if (!this.transaction.isActive()) {
            this.transaction.begin();
        }
    }

    /**
     * Flush the current changes to the data source.
     * 
     * Requires an open transaction, that may be committed or rolled back at a later
     * time.
     */
    public void flush() {
        if (!this.transaction.isActive()) {
            this.transaction.begin();
        }
        this.manager.flush();
    }

    /**
     * Commit the work.
     */
    public void commit() {
        if (this.transaction.isActive()) {
            this.transaction.commit();
        }
    }

    /**
     * Rollback the work.
     */
    public void rollback() {
        if (this.transaction.isActive()) {
            this.transaction.rollback();
        }
    }

    /**
     * Begin the work.
     * 
     * @param sessions The sessions to begin work for.
     */
    public static void begin(Iterable<EntitySession> sessions) {
        for (EntitySession session : sessions) {
            session.begin();
        }
    }

    /**
     * Flush the current changes to the data source. These changes are still
     * uncommitted.
     * 
     * @param sessions The sessions to flush.
     */
    public static void flush(Iterable<EntitySession> sessions) {
        for (EntitySession session : sessions) {
            session.flush();
        }
    }

    /**
     * Commit the work.
     * 
     * @param sessions The sessions to commit.
     */
    public static void commit(Iterable<EntitySession> sessions) {
        for (EntitySession session : sessions) {
            session.commit();
        }
    }

    /**
     * Rollback the work.
     * 
     * @param sessions The sessions to rollback.
     */
    public static void rollback(Iterable<EntitySession> sessions) {
        for (EntitySession session : sessions) {
            session.rollback();
        }
    }

    // endregion Methods

}
