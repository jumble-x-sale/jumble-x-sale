package com.jumble_x_sale.data;

import com.jumble_x_sale.util.CheckedAutoCloseable;

/**
 * An abstract unit of work as base for a superstructure that holds
 * interdependent {@link Repository repositories} and {@link Entity entities}.
 */
public interface UnitOfWork extends CheckedAutoCloseable {

    // region Getters

    /**
     * Should changes be automatically flushed to the data source?
     * 
     * @return Should changes be automatically flushed to the data source?
     */
    public boolean getAutoFlush();

    // endregion Getters

    // region Setters

    /**
     * Should changes be automatically flushed to the data source?
     * 
     * @param value Should changes be automatically flushed to the data source?
     */
    public void setAutoFlush(boolean value);

    // endregion Setters

    // region Methods

    /**
     * Begin working.
     */
    public void begin();

    /**
     * Flush the current changes to the data source. These changes are still
     * uncommitted.
     */
    public void flush();

    /**
     * Commit all open changes.
     */
    public void commit();

    /**
     * Rollback all open changes.
     */
    public void rollback();

    // endregion Methods

}
