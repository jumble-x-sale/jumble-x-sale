package com.jumble_x_sale.data.util;

import com.jumble_x_sale.util.TimeUtils;
import com.jumble_x_sale.util.Utils;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public final class MultiValueFilter extends Filter {

    // region Inner Classes

    /**
     * An operator that takes zero or more operands.
     */
    public static enum Operator {
        /**
         * Equals a value in the list.
         */
        IN(IN_CODE),
        /**
         * Not equals a value in the list.
         */
        NOT_IN(NOT_IN_CODE);

        private static final HashMap<String, Operator> valuesByCode = new HashMap<>();
        static {
            Operator[] types = Operator.values();
            for (int i = 0; i < types.length; i++) {
                valuesByCode.put(types[i].getCode(), types[i]);
            }
        }

        private final String code;

        private Operator(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public static Operator tryValueOf(String idOrCode, Operator defaultValue) {
            idOrCode = idOrCode.toUpperCase();
            Operator value = valuesByCode.get(idOrCode);
            if (value == null) {
                value = Utils.tryValueOf(Operator.class, idOrCode, defaultValue);
            }
            return value;
        }

        public static Operator tryValueOf(String idOrCode) {
            return tryValueOf(idOrCode, null);
        }
    }

    protected static final String IN_CODE = "IN";
    protected static final String NOT_IN_CODE = "NIN";

    protected static final String PN_VALUES = "values";

    public static class OperatorSerializer
            implements JsonbSerializer<Operator>, JsonbDeserializer<Operator> {
        @Override
        public void serialize(Operator obj, JsonGenerator generator, SerializationContext ctx) {
            generator.write(obj.getCode());
        }

        @Override
        public Operator deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
            return Operator.tryValueOf(parser.getString());
        }
    }

    // endregion Inner Classes

    // region Fields

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    private Operator operator;

    private String[] values;

    // endregion Fields

    // region Getters

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    public Operator getOperator() {
        return this.operator;
    }

    public String[] getValues() {
        return this.values;
    }

    // endregion Getters

    // region Setters

    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    // endregion Setters

    // region Constructors

    protected MultiValueFilter(String propertyName, DataType propertyType, Operator operator,
            String[] values) {
        super(propertyName, propertyType);
        this.operator = operator;
        this.values = values;
    }

    public MultiValueFilter() {
        this(null, null, null, null);
    }

    // endregion Constructors

    // region Methods

    public List<UUID> valuesAsUUID() {
        return Arrays.stream(this.values).map(Utils::uuidFromString).collect(Collectors.toList());
    }

    public List<BigDecimal> valuesAsDecimal() {
        return Arrays.stream(this.values).map(Utils::parseBigDecimal).collect(Collectors.toList());
    }

    public List<Instant> valuesAsInstant() {
        return Arrays.stream(this.values).map(TimeUtils::parseInstant).collect(Collectors.toList());
    }

    public List<LocalDate> valuesAsDate() {
        return Arrays.stream(this.values).map(TimeUtils::parseLocalDate)
                .collect(Collectors.toList());
    }

    public List<LocalDateTime> valuesAsDateTime() {
        return Arrays.stream(this.values).map(TimeUtils::parseLocalDateTime)
                .collect(Collectors.toList());
    }

    public List<LocalTime> valuesAsTime() {
        return Arrays.stream(this.values).map(TimeUtils::parseLocalTime)
                .collect(Collectors.toList());
    }

    public List<String> values() {
        return Arrays.asList(this.values);
    }

    // endregion Methods

}
