package com.jumble_x_sale.data.persistence;

import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.util.CheckedAutoCloseableBase;
import com.jumble_x_sale.util.Closeables;

import java.util.HashSet;
import java.util.Set;

/**
 * An abstract persistence unit of work that holds entity repositories.
 * 
 * A persistent unit of work may have more than one {@link EntitySession entity
 * session} each for a related {@link EntityContext entity context} given by the
 * {@link PersistentWorkContext work context}.
 */
public abstract class PersistentUnitOfWork<U extends UnitOfWork> extends CheckedAutoCloseableBase
        implements UnitOfWork {

    // region Fields

    protected final PersistentWorkContext<U> workContext;
    protected final Set<EntitySession> sessions = new HashSet<>();
    protected boolean autoFlush = true;

    // endregion Fields

    // region Getters

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getAutoFlush() {
        return this.autoFlush;
    }

    /**
     * Get the work context for this unit of work.
     * 
     * @return The work context for this unit of work.
     */
    public PersistentWorkContext<U> getWorkContext() {
        return this.workContext;
    }

    // endregion Getters

    // region Setters

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAutoFlush(boolean value) {
        this.autoFlush = value;
    }

    // endregion Setters

    // region Constructors

    protected PersistentUnitOfWork(PersistentWorkContext<U> workContext) {
        this.workContext = workContext;
    }

    // endregion Constructors

    // region Methods

    /**
     * Create a new entity session for the given context.
     * 
     * @param context The entity context to create a session for.
     * @return A new entity session for the given context.
     */
    protected EntitySession createSession(EntityContext context) {
        EntitySession session = new EntitySession(context);
        this.sessions.add(session);
        return session;
    }

    /**
     * Close this persistence context.
     */
    @Override
    public void close() throws Exception {
        Closeables.tryClose(this.sessions);
        super.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void begin() {
        EntitySession.begin(this.sessions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() {
        EntitySession.flush(this.sessions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commit() {
        EntitySession.commit(this.sessions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void rollback() {
        EntitySession.rollback(this.sessions);
    }

    // endregion Methods

}
