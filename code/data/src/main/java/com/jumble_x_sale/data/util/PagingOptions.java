package com.jumble_x_sale.data.util;

import com.jumble_x_sale.util.Utils;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.json.bind.annotation.JsonbTransient;

@JsonbPropertyOrder(value = {"pageNumber", "pageSize", "pageCount", "rowCount"})
public class PagingOptions extends ResultOptions {

    // region Fields

    public static final int DEFAULT_PAGE_SIZE = 20;

    private int pageNumber;
    private int pageSize;
    private int pageCount;
    private long rowCount;

    // endregion Fields

    // region Getters

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getPageCount() {
        return pageCount;
    }

    @JsonbProperty
    public long getRowCount() {
        return rowCount;
    }

    @JsonbTransient
    public int getStartPosition() {
        return (pageNumber - 1) * pageSize;
    }

    @JsonbTransient
    public int getResultCount() {
        return pageSize;
    }

    // endregion Getters

    // region Setters

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @JsonbTransient
    public void setRowCount(long rowCount) {
        this.rowCount = rowCount;
        this.pageCount = (int) Utils.divideCeil(rowCount, pageSize);
    }

    // endregion Setters

    // region Constructors

    public PagingOptions(int pageNumber, int pageSize, long rowCount) {
        if (pageNumber <= 0 || pageSize <= 0) {
            this.pageNumber = 1;
            this.pageSize = DEFAULT_PAGE_SIZE;
        } else {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
        }
        this.setRowCount(rowCount);
    }

    public PagingOptions() {
        this(0, 0, 0);
    }

    public static PagingOptions start() {
        return new PagingOptions(0, 0, 0);
    }

    // endregion Constructors

}
