package com.jumble_x_sale.data.mocks;

import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.data.util.DualValueFilter;
import com.jumble_x_sale.data.util.Filter;
import com.jumble_x_sale.data.util.MultiValueFilter;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.WindowingOptions;
import com.jumble_x_sale.data.util.SingleValueFilter.Operator;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.SingleValueFilter;
import com.jumble_x_sale.util.Types;
import com.jumble_x_sale.util.Utils;
import java.util.UUID;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A mock {@link Repository repository} for {@link Entity entities}.
 *
 * @param <U> The concrete {@link UnitOfWork unit of work}.
 * @param <E> The concrete {@link Entity entity}.
 */
public abstract class MockRepository<U extends UnitOfWork, E extends Entity>
        implements Repository<U, E> {

    // region Fields

    protected final U unitOfWork;
    protected final Class<E> entityClass;
    protected final Constructor<E> entityCopyCtor;
    protected final HashMap<UUID, E> entities = new HashMap<UUID, E>();
    protected final HashMap<UUID, E> savedEntities = new HashMap<UUID, E>();

    // endregion Fields

    // region Constructors

    protected MockRepository(U unitOfWork, Class<E> entityClass) {
        this.unitOfWork = unitOfWork;
        this.entityClass = entityClass;
        this.entityCopyCtor = Types.getDeclaredConstructor(entityClass);
    }

    // endregion Constructors

    // region Methods

    private void getClones(Map<UUID, E> target, Map<UUID, E> source) {
        target.clear();
        try {
            for (Map.Entry<UUID, E> entry : source.entrySet()) {
                E value = entry.getValue();
                target.put(entry.getKey(), entityCopyCtor.newInstance(value));
            }
        } catch (InstantiationException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
    }

    /**
     * Get the {@link UnitOfWork unit of work}.
     */
    public U getUnitOfWork() {
        return this.unitOfWork;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(E entity) {
        entity.onPrePersist();
        entities.put(entity.getId(), entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E merge(E entity) {
        // Actually nothing to change or merge.
        return entity;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(E entity) {
        return entities.remove(entity.getId()) != null;
    }

    /**
     * Commit all open changes.
     */
    protected void commit() {
        getClones(this.savedEntities, this.entities);
    }

    /**
     * Rollback all open changes.
     */
    protected void rollback() {
        getClones(this.entities, this.savedEntities);
    }

    /**
     * Get the {@link Entity entity} with the given ID from the repository.
     *
     * @param id The {@link UUID} of the wanted {@link Entity entity}.
     * @return The {@link Entity entity} with the given ID.
     */
    public E getById(UUID id) {
        return entities.get(id);
    }

    /**
     * {@inheritDoc}
     */
    public Stream<E> get(List<Filter> filters, WindowingOptions options) {
        Stream<E> stream = this.filter(filters);
        if (options.getStartPosition() > 0) {
            stream = stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream = stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    public PagingResult<E> get(List<Filter> filters, PagingOptions options) {
        Stream<E> stream = this.filter(filters);
        if (options.getStartPosition() > 0) {
            stream = stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream = stream.limit(options.getResultCount());
        }

        options.setRowCount(entities.values().size());
        return new PagingResult<>(options, stream);
    }

    /**
     * {@inheritDoc}
     */
    public PagingResult<E> get(List<Filter> filters, PagingOptions options,
            Predicate<E> predicate) {
        Stream<E> stream = this.filter(filters).filter(predicate);
        if (options.getStartPosition() > 0) {
            stream = stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream = stream.limit(options.getResultCount());
        }

        options.setRowCount(entities.values().size());
        return new PagingResult<>(options, stream);
    }

    /**
     * Get all {@link Entity entities} from the repository that match the given {@link Predicate
     * predicate}.
     *
     * @param predicate The {@link Predicate predicate} by which to filter the {@link Entity
     *                  entities}.
     * @return All {@link Entity entities} from the repository that match the given {@link Predicate
     *         predicate}.
     */
    protected Stream<E> getByPredicate(Predicate<E> predicate) {
        return this.entities.values().stream().filter(predicate);
    }

    /**
     * Invoke all property getters for the given filter.
     *
     * @param entity               The entity on which to use the getters.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link Filter filter} on which to invoke the getters.
     * @return The result of the property getters.
     */
    private Object invokeGetters(E entity, Map<String, String[]> propertyNameMappings,
            Filter filter) {
        try {
            String[] propertyNames = propertyNameMappings.get(filter.getPropertyName());
            Object obj = null;
            Class<?> cls = entityClass;
            for (int i = 0; i < propertyNames.length; i++, cls = obj.getClass()) {
                Method method = cls.getMethod("get" + propertyNames[i]);
                obj = method.invoke(entity);
            }
            return obj;
        } catch (NoSuchMethodException e) {
        } catch (IllegalAccessException e) {
        } catch (InvocationTargetException e) {
        }
        return null;
    }

    /**
     * Create a predicate from a {@link SingleValueFilter SingleValueFilter}.
     *
     * @param stream               The stream to apply the filter to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link SingleValueFilter SingleValueFilter} to create a
     *                             predicate from.
     * @return A predicate from a {@link SingleValueFilter SingleValueFilter}.
     */
    private Predicate<E> predicate(Stream<E> stream, Map<String, String[]> propertyNameMappings,
            SingleValueFilter filter) {
        Predicate<E> predicate = null;
        switch (filter.getOperator()) {
            case EQUAL_TO:
            case NOT_EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.valueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = (e) -> invokeGetters(e, propertyNameMappings, filter)
                                .equals(filter.getValue());
                        break;
                }
                if (filter.getOperator() == Operator.NOT_EQUAL_TO) {
                    predicate = predicate.negate();
                }
                break;
            case LESS_THAN_OR_EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsUUID()) <= 0;
                        break;
                    case NUMBER:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDecimal()) <= 0;
                        break;
                    case INSTANT:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsInstant()) <= 0;
                        break;
                    case DATE:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) <= 0;
                        break;
                    case DATE_TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDateTime()) <= 0;
                        break;
                    case TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) <= 0;
                        break;
                    case STRING:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.getValue()) <= 0;
                        break;
                }
                break;
            case LESS_THAN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsUUID()) < 0;
                        break;
                    case NUMBER:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDecimal()) < 0;
                        break;
                    case INSTANT:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsInstant()) < 0;
                        break;
                    case DATE:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) < 0;
                        break;
                    case DATE_TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDateTime()) < 0;
                        break;
                    case TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) < 0;
                        break;
                    case STRING:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.getValue()) < 0;
                        break;
                }
                break;
            case GREATER_THAN_OR_EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsUUID()) >= 0;
                        break;
                    case NUMBER:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDecimal()) >= 0;
                        break;
                    case INSTANT:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsInstant()) >= 0;
                        break;
                    case DATE:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) >= 0;
                        break;
                    case DATE_TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDateTime()) >= 0;
                        break;
                    case TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) >= 0;
                        break;
                    case STRING:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.getValue()) >= 0;
                        break;
                }
                break;
            case GREATER_THAN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsUUID()) > 0;
                        break;
                    case NUMBER:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDecimal()) > 0;
                        break;
                    case INSTANT:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsInstant()) > 0;
                        break;
                    case DATE:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) > 0;
                        break;
                    case DATE_TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsDateTime()) > 0;
                        break;
                    case TIME:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.valueAsTime()) > 0;
                        break;
                    case STRING:
                        predicate = (e) -> Utils.compareTo(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.getValue()) > 0;
                        break;
                }
                break;
            case LIKE:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case NUMBER:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case INSTANT:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case DATE:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case DATE_TIME:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case TIME:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case STRING:
                        predicate = (e) -> Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                }
                break;
            case NOT_LIKE:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case NUMBER:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case INSTANT:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case DATE:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case DATE_TIME:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case TIME:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                    case STRING:
                        predicate = (e) -> !Utils.like(
                                invokeGetters(e, propertyNameMappings, filter).toString(),
                                filter.getValue());
                        break;
                }
                break;
        }
        return predicate;
    }

    /**
     * Create a predicate from a {@link DualValueFilter DualValueFilter}.
     *
     * @param stream               The stream to apply the filter to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link DualValueFilter DualValueFilter} to create a predicate
     *                             from.
     * @return A predicate from a {@link DualValueFilter DualValueFilter}.
     */
    private Predicate<E> predicate(Stream<E> stream, Map<String, String[]> propertyNameMappings,
            DualValueFilter filter) {
        Predicate<E> predicate = null;
        switch (filter.getOperator()) {
            case BETWEEN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.minValueAsUUID(), filter.maxValueAsUUID());
                        break;
                    case NUMBER:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.minValueAsDecimal(), filter.maxValueAsDecimal());
                        break;
                    case INSTANT:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.minValueAsInstant(), filter.maxValueAsInstant());
                        break;
                    case DATE:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.minValueAsDate(), filter.maxValueAsDate());
                        break;
                    case DATE_TIME:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.minValueAsDateTime(), filter.maxValueAsDateTime());
                        break;
                    case TIME:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.minValueAsTime(), filter.maxValueAsTime());
                        break;
                    case STRING:
                        predicate =
                                (e) -> Utils.between(invokeGetters(e, propertyNameMappings, filter),
                                        filter.getMinValue(), filter.getMaxValue());
                        break;
                }
                break;
            case IN_BETWEEN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.minValueAsUUID(), filter.maxValueAsUUID());
                        break;
                    case NUMBER:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.minValueAsDecimal(), filter.maxValueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.minValueAsInstant(), filter.maxValueAsInstant());
                        break;
                    case DATE:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.minValueAsDate(), filter.maxValueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.minValueAsDateTime(), filter.maxValueAsDateTime());
                        break;
                    case TIME:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.minValueAsTime(), filter.maxValueAsTime());
                        break;
                    case STRING:
                        predicate = (e) -> Utils.inBetween(
                                invokeGetters(e, propertyNameMappings, filter),
                                filter.getMinValue(), filter.getMaxValue());
                        break;
                }
                break;
        }
        return predicate;
    }

    /**
     * Create a predicate from a {@link MultiValueFilter MultiValueFilter}.
     *
     * @param stream               The stream to apply the filter to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link MultiValueFilter MultiValueFilter} to create a
     *                             predicate from.
     * @return A predicate from a {@link MultiValueFilter MultiValueFilter}.
     */
    private Predicate<E> predicate(Stream<E> stream, Map<String, String[]> propertyNameMappings,
            MultiValueFilter filter) {
        Predicate<E> predicate = null;
        switch (filter.getPropertyType()) {
            case UUID:
                predicate = (e) -> filter.valuesAsUUID()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
            case NUMBER:
                predicate = (e) -> filter.valuesAsDecimal()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
            case INSTANT:
                predicate = (e) -> filter.valuesAsInstant()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
            case DATE:
                predicate = (e) -> filter.valuesAsDate()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
            case DATE_TIME:
                predicate = (e) -> filter.valuesAsDateTime()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
            case TIME:
                predicate = (e) -> filter.valuesAsTime()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
            case STRING:
                predicate = (e) -> filter.values()
                        .contains(invokeGetters(e, propertyNameMappings, filter));
                break;
        }
        if (filter.getOperator() == MultiValueFilter.Operator.NOT_IN) {
            predicate = predicate.negate();
        }
        return predicate;
    }

    /**
     * Create a predicate from filters.
     *
     * @param stream               The stream to apply the filters to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filters              The filters to create a predicate from.
     * @return A predicate from filters.
     */
    public Predicate<E> predicate(Stream<E> stream, Map<String, String[]> propertyNameMappings,
            Filter[] filters) {
        Predicate<E> predicate = (e) -> true;
        for (int i = 0; i < filters.length; i++) {
            if (filters[i] instanceof SingleValueFilter) {
                predicate.and(
                        predicate(stream, propertyNameMappings, (SingleValueFilter) filters[i]));
            } else if (filters[i] instanceof DualValueFilter) {
                predicate.and(
                        this.predicate(stream, propertyNameMappings, (DualValueFilter) filters[i]));
            } else if (filters[i] instanceof MultiValueFilter) {
                predicate.and(this.predicate(stream, propertyNameMappings,
                        (MultiValueFilter) filters[i]));
            }
        }
        return predicate;
    }

    /**
     * Get a filtered stream withg matching entities.
     *
     * @param filters The filters to create a predicate from.
     * @return Get a filtered stream withg matching entities.
     */
    public Stream<E> filter(List<Filter> filters) {
        Filter[] filterArray = filters.stream().toArray(Filter[]::new);
        Stream<E> stream = entities.values().stream();
        return stream.filter(this.predicate(stream, this.getPropertyNameMappings(), filterArray));
    }

    // endregion Methods

}
