package com.jumble_x_sale.data;

import com.jumble_x_sale.util.CheckedAutoCloseable;

/**
 * A context for working on a subject. Used for managing {@link UnitOfWork units
 * of work}.
 */
public interface WorkContext<U extends UnitOfWork> extends CheckedAutoCloseable {

    // region Methods

    /**
     * Create a new unit of work to handle a self-contained process.
     * 
     * @return A new unit of work to handle a self-contained process.
     */
    public U createUnitOfWork();

    // region Methods

}
