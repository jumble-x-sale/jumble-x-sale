package com.jumble_x_sale.data.util;

import com.jumble_x_sale.util.TimeUtils;
import com.jumble_x_sale.util.Utils;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.UUID;

public final class DualValueFilter extends Filter {

    // region Inner Classes

    /**
     * An operator that takes two operands.
     */
    public static enum Operator {
        /**
         * Inclusive between. Greater than or equal to the minimum, less than or equal to the
         * maximum.
         */
        BETWEEN(BETWEEN_CODE),
        /**
         * Exclusive between. Greater than or equal to the minimum, less than the maximum.
         */
        IN_BETWEEN(IN_BETWEEN_CODE);

        private static final HashMap<String, Operator> valuesByCode = new HashMap<>();
        static {
            Operator[] types = Operator.values();
            for (int i = 0; i < types.length; i++) {
                valuesByCode.put(types[i].getCode(), types[i]);
            }
        }

        private final String code;

        private Operator(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public static Operator tryValueOf(String idOrCode, Operator defaultValue) {
            idOrCode = idOrCode.toUpperCase();
            Operator value = valuesByCode.get(idOrCode);
            if (value == null) {
                value = Utils.tryValueOf(Operator.class, idOrCode, defaultValue);
            }
            return value;
        }

        public static Operator tryValueOf(String idOrCode) {
            return tryValueOf(idOrCode, null);
        }
    }

    protected static final String BETWEEN_CODE = "BTW";
    protected static final String IN_BETWEEN_CODE = "IBTW";

    protected static final String PN_MIN_VALUE = "minValue";
    protected static final String PN_MAX_VALUE = "maxValue";

    public static class OperatorSerializer
            implements JsonbSerializer<Operator>, JsonbDeserializer<Operator> {
        @Override
        public void serialize(Operator obj, JsonGenerator generator, SerializationContext ctx) {
            generator.write(obj.getCode());
        }

        @Override
        public Operator deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
            return Operator.tryValueOf(parser.getString());
        }
    }

    // endregion Inner Classes

    // region Fields

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    private Operator operator;

    private String minValue;
    private String maxValue;

    // endregion Fields

    // region Getters

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    public Operator getOperator() {
        return this.operator;
    }

    public String getMinValue() {
        return this.minValue;
    }

    public String getMaxValue() {
        return this.maxValue;
    }

    // endregion Getters

    // region Setters

    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public void setMinValue(String minValue) {
        this.minValue = minValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    // endregion Setters

    // region Constructors

    protected DualValueFilter(String propertyName, DataType propertyType, Operator operator,
            String minValue, String maxValue) {
        super(propertyName, propertyType);
        this.operator = operator;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public DualValueFilter() {
        this(null, null, null, null, null);
    }

    // endregion Constructors

    // region Methods

    public UUID minValueAsUUID() {
        return Utils.uuidFromString(this.minValue);
    }

    public UUID maxValueAsUUID() {
        return Utils.uuidFromString(this.maxValue);
    }

    public BigDecimal minValueAsDecimal() {
        return Utils.parseBigDecimal(this.minValue);
    }

    public BigDecimal maxValueAsDecimal() {
        return Utils.parseBigDecimal(this.maxValue);
    }

    public Instant minValueAsInstant() {
        return TimeUtils.parseInstant(this.minValue);
    }

    public Instant maxValueAsInstant() {
        return TimeUtils.parseInstant(this.maxValue);
    }

    public LocalDate minValueAsDate() {
        return TimeUtils.parseLocalDate(this.minValue);
    }

    public LocalDate maxValueAsDate() {
        return TimeUtils.parseLocalDate(this.maxValue);
    }

    public LocalDateTime minValueAsDateTime() {
        return TimeUtils.parseLocalDateTime(this.minValue);
    }

    public LocalDateTime maxValueAsDateTime() {
        return TimeUtils.parseLocalDateTime(this.maxValue);
    }

    public LocalTime minValueAsTime() {
        return TimeUtils.parseLocalTime(this.minValue);
    }

    public LocalTime maxValueAsTime() {
        return TimeUtils.parseLocalTime(this.maxValue);
    }

    // endregion Methods

}
