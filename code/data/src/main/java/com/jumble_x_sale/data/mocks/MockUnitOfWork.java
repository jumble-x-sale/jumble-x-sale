package com.jumble_x_sale.data.mocks;

import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.util.CheckedAutoCloseableBase;

import java.util.List;
import java.util.ArrayList;

/**
 * An abstract mock unit of work that holds entity repositories.
 * 
 * @param <U> The concrete type of {@link UnitOfWork unit of work}.
 */
public abstract class MockUnitOfWork<U extends UnitOfWork> extends CheckedAutoCloseableBase implements UnitOfWork {

    // region Fields

    protected MockWorkContext<U> workContext;

    /**
     * A list with all {@link Repository repositories} in this {@link UnitOfWork
     * unit of work}.
     */
    protected List<MockRepository<?, ?>> repositories = new ArrayList<MockRepository<?, ?>>();

    protected boolean autoFlush = true;

    // endregion Fields

    // region Getters

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getAutoFlush() {
        return this.autoFlush;
    }

    // endregion Getters

    // region Setters

    /**
     * {@inheritDoc}
     */
    @Override
    public void setAutoFlush(boolean value) {
        this.autoFlush = value;
    }

    // endregion Setters

    // region Constructors

    protected MockUnitOfWork(MockWorkContext<U> workContext) {
        this.workContext = workContext;
    }

    // endregion Constructors

    // region Methods

    /**
     * Add a new {@link Repository repository} to the {@link UnitOfWork unit of
     * work}. Used on construction.
     * 
     * @param <R>        The concrete type of {@link Repository repository} to add.
     * @param <E>        The concrete type of {@link Entity entities} in the
     *                   {@link Repository repository} to add.
     * @param repository The new {@link Repository repository} to add to
     *                   {@link UnitOfWork unit of work}.
     * @return The {@link Repository repository} that was added to the
     *         {@link UnitOfWork unit of work}.
     */
    protected <R extends MockRepository<U, E>, E extends Entity> R addRepository(R repository) {
        this.repositories.add(repository);
        return repository;
    }

    /**
     * Finalize this persistence context.
     */
    @Override
    protected void finalize() throws Throwable {
        try {
            this.close();
        } finally {
            super.finalize();
        }
    }

    /**
     * Close this persistence context.
     */
    @Override
    public void close() throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    public void begin() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() {
    }

    /**
     * {@inheritDoc}
     */
    public void commit() {
        for (MockRepository<?, ?> repository : this.repositories) {
            repository.commit();
        }
    }

    /**
     * {@inheritDoc}
     */
    public void rollback() {
        for (MockRepository<?, ?> repository : this.repositories) {
            repository.rollback();
        }
    }

    // endregion Methods

}
