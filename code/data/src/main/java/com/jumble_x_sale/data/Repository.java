package com.jumble_x_sale.data;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import com.jumble_x_sale.data.util.Filter;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.util.WindowingOptions;

/**
 * A repository for {@link Entity entities}.
 * 
 * @param <U> The type of the related {@link UnitOfWork unit of work}.
 * @param <E> The type of {@link Entity entity} in this repository.
 */
public interface Repository<U extends UnitOfWork, E extends Entity> {

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    public Map<String, String[]> getPropertyNameMappings();

    /**
     * Get the {@link UnitOfWork unit of work}.
     * 
     * @return The {@link UnitOfWork unit of work}.
     */
    public U getUnitOfWork();

    /**
     * Add the given {@link Entity entity} to the {@link Repository repository}.
     * 
     * @param entity The given {@link Entity entity}.
     */
    public void add(E entity);

    /**
     * Merge the given {@link Entity entity} to the {@link Repository repository}.
     * 
     * @param entity The {@link Entity entity} to merge.
     * @return The merged entity.
     */
    public E merge(E entity);

    /**
     * Remove the given {@link Entity entity} from the {@link Repository repository}.
     * 
     * @param entity The given {@link Entity entity}.
     * @return Could the entity be removed?
     */
    public boolean remove(E entity);

    /**
     * Get the {@link Entity entity} with the given ID from the repository.
     * 
     * @param id The {@link UUID} of the wanted {@link Entity entity}.
     * @return The {@link Entity entity} with the given ID.
     */
    public E getById(UUID id);

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @param filters The {@link Filter filters} for the result.
     * @param options The {@link WindowingOptions windowing options} for the result.
     * @return All {@link Entity entities} from the repository.
     */
    public Stream<E> get(List<Filter> filters, WindowingOptions options);

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @param filters The {@link Filter filters} for the result.
     * @param options The {@link PagingOptions paging options} for the result.
     * @return All {@link Entity entities} from the repository.
     */
    public PagingResult<E> get(List<Filter> filters, PagingOptions options);

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @param filterPagingOptions The {@link FilterPagingOptions paging filter options} for the
     *                            result.
     * @return All {@link Entity entities} from the repository.
     */
    default PagingResult<E> get(FilterPagingOptions filterPagingOptions) {
        // return this.get(filterPagingOptions.getFilters(), filterPagingOptions.getOptions());
        return this.get(filterPagingOptions.getFilters(), filterPagingOptions);
    }

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @param filters The {@link Filter filters} for the result.
     * @return All {@link Entity entities} from the repository.
     */
    default Stream<E> get(List<Filter> filters) {
        return this.get(filters, ResultOptions.allRows());
    }

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @param options The {@link WindowingOptions windowing options} for the result.
     * @return All {@link Entity entities} from the repository.
     */
    default Stream<E> get(WindowingOptions options) {
        return this.get(Filter.EMPTY_LIST, options);
    }

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @param options The {@link PagingOptions paging options} for the result.
     * @return All {@link Entity entities} from the repository.
     */
    default PagingResult<E> get(PagingOptions options) {
        return this.get(Filter.EMPTY_LIST, options);
    }

    /**
     * Get all {@link Entity entities} from the repository.
     * 
     * @return All {@link Entity entities} from the repository.
     */
    default Stream<E> get() {
        return this.get(ResultOptions.allRows());
    }

}
