package com.jumble_x_sale.data.util;

import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.json.bind.annotation.JsonbTransient;

import java.util.List;

@JsonbPropertyOrder(value = {"pageNumber", "pageSize", "pageCount", "rowCount"})
public final class FilterPagingOptions extends PagingOptions {

    // region Fields

    private List<Filter> filters;

    // endregion Fields

    // region Getters

    @JsonbTransient
    public List<Filter> getFilters() {
        return this.filters;
    }

    // endregion Getters

    // region Setters

    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    // endregion Setters

    // region Constructors

    public FilterPagingOptions(int pageNumber, int pageSize, long rowCount, List<Filter> filters) {
        super(pageNumber, pageSize, rowCount);
        this.filters = filters;
    }

    public FilterPagingOptions() {
        this(0, 0, 0, Filter.EMPTY_LIST);
    }

    // endregion Constructors

}
