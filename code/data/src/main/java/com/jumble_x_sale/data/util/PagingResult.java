package com.jumble_x_sale.data.util;

import java.io.Serializable;
import java.util.stream.Stream;

/**
 * The result of paging query for entities.
 */
public final class PagingResult<E extends Serializable> {

    // region Fields

    private PagingOptions options;
    private Stream<E> data;

    // endregion Fields

    // region Getters

    public PagingOptions getOptions() {
        return options;
    }

    public Stream<E> getData() {
        return this.data;
    }

    // endregion Getters

    // region Setters

    public void setOptions(PagingOptions options) {
        this.options = options;
    }

    public void setData(Stream<E> data) {
        this.data = data;
    }

    // endregion Setters

    // region Constructors

    public PagingResult(PagingOptions options, Stream<E> data) {
        this.options = options;
        this.data = data;
    }

    // endregion Constructors

}
