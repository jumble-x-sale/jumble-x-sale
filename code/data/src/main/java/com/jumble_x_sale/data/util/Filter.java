package com.jumble_x_sale.data.util;

import com.jumble_x_sale.util.Utils;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.adapter.JsonbAdapter;
import javax.json.bind.annotation.JsonbPropertyOrder;
import javax.json.bind.annotation.JsonbTypeAdapter;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@JsonbTypeAdapter(value = Filter.Adapter.class)
@JsonbPropertyOrder(value = {Filter.PN_PROPERTY_NAME, Filter.PN_PROPERTY_TYPE, Filter.PN_OPERATOR,
        SingleValueFilter.PN_VALUE, DualValueFilter.PN_MIN_VALUE, DualValueFilter.PN_MAX_VALUE,
        MultiValueFilter.PN_VALUES})
public abstract class Filter {

    // region Inner Classes

    /**
     * The type of filter value.
     */
    public static enum DataType {
        /**
         * A {@link UUID UUID}.
         */
        UUID("ID"),
        /**
         * A {@link BigDecimal BigDecimal}.
         */
        NUMBER("N"),
        /**
         * An {@link Instant Instant}.
         */
        INSTANT("I"),
        /**
         * A {@link LocalDate LocalDate}.
         */
        DATE("D"),
        /**
         * A {@link LocalDateTime LocalDateTime}.
         */
        DATE_TIME("DT"),
        /**
         * A {@link LocalTime LocalTime}.
         */
        TIME("T"),
        /**
         * A {@link String String}.
         */
        STRING("S");

        private static final HashMap<String, DataType> valuesByCode = new HashMap<>();
        static {
            DataType[] types = DataType.values();
            for (int i = 0; i < types.length; i++) {
                valuesByCode.put(types[i].getCode(), types[i]);
            }
        }

        private final String code;

        private DataType(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public static DataType tryValueOf(String idOrCode, DataType defaultValue) {
            idOrCode = idOrCode.toUpperCase();
            DataType value = valuesByCode.get(idOrCode);
            if (value == null) {
                value = Utils.tryValueOf(DataType.class, idOrCode, defaultValue);
            }
            return value;
        }

        public static DataType tryValueOf(String idOrCode) {
            return tryValueOf(idOrCode, null);
        }
    }

    public static class OperatorSerializer
            implements JsonbSerializer<DataType>, JsonbDeserializer<DataType> {
        @Override
        public void serialize(DataType obj, JsonGenerator generator, SerializationContext ctx) {
            generator.write(obj.getCode());
        }

        @Override
        public DataType deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
            return DataType.tryValueOf(parser.getString());
        }
    }

    public static class Adapter implements JsonbAdapter<Filter, JsonObject> {
        @Override
        public JsonObject adaptToJson(Filter obj) throws Exception {
            JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
            if (obj != null) {
                objectBuilder.add(PN_PROPERTY_NAME, obj.getPropertyName());
                objectBuilder.add(PN_PROPERTY_TYPE,
                        Json.createValue(obj.getPropertyType().getCode()));

                if (obj instanceof SingleValueFilter) {
                    SingleValueFilter singleValueFilter = (SingleValueFilter) obj;
                    objectBuilder.add(PN_OPERATOR,
                            Json.createValue(singleValueFilter.getOperator().getCode()));
                    objectBuilder.add(SingleValueFilter.PN_VALUE, singleValueFilter.getValue());
                } else if (obj instanceof DualValueFilter) {
                    DualValueFilter dualValueFilter = (DualValueFilter) obj;
                    objectBuilder.add(PN_OPERATOR,
                            Json.createValue(dualValueFilter.getOperator().getCode()));
                    objectBuilder.add(DualValueFilter.PN_MIN_VALUE, dualValueFilter.getMinValue());
                    objectBuilder.add(DualValueFilter.PN_MAX_VALUE, dualValueFilter.getMinValue());
                } else if (obj instanceof MultiValueFilter) {
                    MultiValueFilter multiValueFilter = (MultiValueFilter) obj;
                    objectBuilder.add(PN_OPERATOR,
                            Json.createValue(multiValueFilter.getOperator().getCode()));
                    JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
                    for (String value : multiValueFilter.getValues()) {
                        arrayBuilder.add(value);
                    }
                    objectBuilder.add(MultiValueFilter.PN_VALUES, arrayBuilder);
                }
            }
            return objectBuilder.build();
        }

        @Override
        public Filter adaptFromJson(JsonObject obj) throws Exception {
            String operator = obj.getString(PN_OPERATOR);
            switch (operator) {
                case SingleValueFilter.EQUAL_TO_CODE:
                case SingleValueFilter.NOT_EQUAL_TO_CODE:
                case SingleValueFilter.LESS_THAN_OR_EQUAL_TO_CODE:
                case SingleValueFilter.GREATER_THAN_OR_EQUAL_TO_CODE:
                case SingleValueFilter.LESS_THAN_CODE:
                case SingleValueFilter.GREATER_THAN_CODE:
                case SingleValueFilter.LIKE_CODE:
                case SingleValueFilter.NOT_LIKE_CODE:
                    return new SingleValueFilter(obj.getString(PN_PROPERTY_NAME),
                            DataType.tryValueOf(obj.getString(PN_PROPERTY_TYPE)),
                            SingleValueFilter.Operator.tryValueOf(obj.getString(PN_OPERATOR)),
                            obj.getString(SingleValueFilter.PN_VALUE));
                case DualValueFilter.BETWEEN_CODE:
                case DualValueFilter.IN_BETWEEN_CODE:
                    return new DualValueFilter(obj.getString(PN_PROPERTY_NAME),
                            DataType.tryValueOf(obj.getString(PN_PROPERTY_TYPE)),
                            DualValueFilter.Operator.tryValueOf(obj.getString(PN_OPERATOR)),
                            obj.getString(DualValueFilter.PN_MIN_VALUE),
                            obj.getString(DualValueFilter.PN_MAX_VALUE));
                case MultiValueFilter.IN_CODE:
                case MultiValueFilter.NOT_IN_CODE:
                    return new MultiValueFilter(obj.getString(PN_PROPERTY_NAME),
                            DataType.tryValueOf(obj.getString(PN_PROPERTY_TYPE)),
                            MultiValueFilter.Operator.tryValueOf(obj.getString(PN_OPERATOR)),
                            fromJsonStringArray(obj.getJsonArray(MultiValueFilter.PN_VALUES)));
            }

            return null;
        }

        private static String fromJsonString(JsonString jsonString) {
            return jsonString.getString();
        }

        private static String[] fromJsonStringArray(JsonArray jsonArray) {
            return jsonArray.getValuesAs(JsonString.class).stream().map(Adapter::fromJsonString)
                    .toArray(String[]::new);
        }
    }

    // endregion Inner Classes

    // region Fields

    public static final Filter[] EMPTY_ARRAY = new Filter[0];
    public static final List<Filter> EMPTY_LIST = Collections.emptyList();

    protected static final String PN_PROPERTY_NAME = "propertyName";
    protected static final String PN_PROPERTY_TYPE = "propertyType";
    protected static final String PN_OPERATOR = "operator";

    private String propertyName;

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    private DataType propertyType;

    // endregion Fields

    // region Getters

    public String getPropertyName() {
        return this.propertyName;
    }

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    public DataType getPropertyType() {
        return this.propertyType;
    }

    // endregion Getters

    // region Setters

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    public void setPropertyType(DataType propertyType) {
        this.propertyType = propertyType;
    }

    // endregion Setters

    // region Constructors

    protected Filter(String propertyName, DataType propertyType) {
        this.propertyName = propertyName;
        this.propertyType = propertyType;
    }

    // endregion Constructors

    // region Methods

    public static SingleValueFilter equal(String propertyName, DataType propertyType,
            String value) {
        return new SingleValueFilter(propertyName, propertyType,
                SingleValueFilter.Operator.EQUAL_TO, value);
    }

    public static SingleValueFilter notEqual(String propertyName, DataType propertyType,
            String value) {
        return new SingleValueFilter(propertyName, propertyType,
                SingleValueFilter.Operator.NOT_EQUAL_TO, value);
    }

    public static SingleValueFilter lesserEqual(String propertyName, DataType propertyType,
            String value) {
        return new SingleValueFilter(propertyName, propertyType,
                SingleValueFilter.Operator.LESS_THAN_OR_EQUAL_TO, value);
    }

    public static SingleValueFilter greaterEqual(String propertyName, DataType propertyType,
            String value) {
        return new SingleValueFilter(propertyName, propertyType,
                SingleValueFilter.Operator.GREATER_THAN_OR_EQUAL_TO, value);
    }

    public static SingleValueFilter lesser(String propertyName, DataType propertyType,
            String value) {
        return new SingleValueFilter(propertyName, propertyType,
                SingleValueFilter.Operator.LESS_THAN, value);
    }

    public static SingleValueFilter greater(String propertyName, DataType propertyType,
            String value) {
        return new SingleValueFilter(propertyName, propertyType,
                SingleValueFilter.Operator.GREATER_THAN, value);
    }

    public static DualValueFilter between(String propertyName, DataType propertyType,
            String minValue, String maxValue) {
        return new DualValueFilter(propertyName, propertyType, DualValueFilter.Operator.BETWEEN,
                minValue, maxValue);
    }

    public static DualValueFilter inBetween(String propertyName, DataType propertyType,
            String minValue, String maxValue) {
        return new DualValueFilter(propertyName, propertyType, DualValueFilter.Operator.IN_BETWEEN,
                minValue, maxValue);
    }

    @SafeVarargs
    public static MultiValueFilter in(String propertyName, DataType propertyType,
            String... values) {
        return new MultiValueFilter(propertyName, propertyType, MultiValueFilter.Operator.IN,
                values);
    }

    @SafeVarargs
    public static MultiValueFilter notIn(String propertyName, DataType propertyType,
            String... values) {
        return new MultiValueFilter(propertyName, propertyType, MultiValueFilter.Operator.NOT_IN,
                values);
    }

    // endregion Methods

}
