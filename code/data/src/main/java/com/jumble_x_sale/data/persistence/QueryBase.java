package com.jumble_x_sale.data.persistence;

import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.util.Filter;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * The base of query.
 * 
 * @param <E>  The concrete type of entity.
 * @param <PK> The of primary key used in the entity.
 */
public abstract class QueryBase<E extends Serializable, PK extends Serializable> {

    // region Fields

    private final EntityAccess<E, PK> access;
    private final Class<E> entityClass;
    protected final CriteriaBuilder b;
    private final CriteriaQuery<E> cq;
    private final Root<E> r;
    private BiFunction<CriteriaBuilder, Root<?>, Predicate> mainPredicateGetter;
    private Function<Root<?>, Predicate> secondaryPredicateGetter;
    private TypedQuery<E> tq;
    private Consumer<TypedQuery<?>> parameterSetter;

    // endregion Fields

    // region Setters

    /**
     * Set the {@link BiFunction main predicate getter} for matching.
     * 
     * @param mainPredicateGetter The {@link BiFunction main predicate getter} for
     *                            matching.
     */
    public void setMainPredicateGetter(BiFunction<CriteriaBuilder, Root<?>, Predicate> mainPredicateGetter) {
        this.mainPredicateGetter = mainPredicateGetter;
    }

    // endregion Setters

    // region Constructors

    /**
     * Create a generic access to an entity.
     * 
     * @param access The {[@link EntityAccess entity access} for which to create the
     *               query.
     */
    public QueryBase(final EntityAccess<E, PK> access) {
        this.access = access;
        this.entityClass = access.entityClass;
        this.b = access.getCriteriaBuilder();
        this.cq = b.createQuery(this.entityClass);
        this.r = cq.from(this.entityClass);
        this.mainPredicateGetter = null;
        this.secondaryPredicateGetter = null;
        this.tq = null;
        this.parameterSetter = null;
    }

    // endregion Constructors

    // region Methods

    /**
     * Create a predicate from filters.
     *
     * @param propertyNameMappings The path mappings for properties.
     * @param filters              The filters to create a predicate from.
     * @return A predicate from filters.
     */
    protected Predicate predicate(Map<String, String[]> propertyNameMappings, List<Filter> filters) {
        Filter[] filterArray = filters.stream().toArray(Filter[]::new);
        return this.access.predicate(r, propertyNameMappings, filterArray);
    }

    /**
     * Prepare the query.
     *
     * @param parameterSetter          The {@link Consumer parameter setter} for
     *                                 matching.
     * @param secondaryPredicateGetter The {@link Function secondary predicate
     *                                 getter} for matching.
     */
    protected void prepare(Consumer<TypedQuery<?>> parameterSetter,
            Function<Root<?>, Predicate> secondaryPredicateGetter) {
        this.parameterSetter = parameterSetter;
        this.secondaryPredicateGetter = secondaryPredicateGetter;
        Predicate mainPredicate = mainPredicateGetter == null ? null : mainPredicateGetter.apply(b, r);
        Predicate secondaryPredicate = secondaryPredicateGetter == null ? null : secondaryPredicateGetter.apply(r);
        CriteriaQuery<E> cq = this.cq.select(r);
        if (mainPredicate != null) {
            if (secondaryPredicate != null) {
                cq = cq.where(mainPredicate, secondaryPredicate);
            } else {
                cq = cq.where(mainPredicate);
            }
        } else if (secondaryPredicate != null) {
            cq = cq.where(secondaryPredicate);
        }
        tq = access.createQuery(cq);
        parameterSetter.accept(tq);
    }

    /**
     * Prepare the query.
     */
    protected void prepare(Consumer<TypedQuery<?>> parameterSetter) {
        this.parameterSetter = parameterSetter;
        Predicate mainPredicate = mainPredicateGetter == null ? null : mainPredicateGetter.apply(b, r);
        CriteriaQuery<E> cq = this.cq.select(r);
        if (mainPredicate != null) {
            cq = cq.where(mainPredicate);
        }
        tq = access.createQuery(cq);
        parameterSetter.accept(tq);
    }

    /**
     * Find the one matching {@link Entity entity}.
     *
     * @return The one matching {@link Entity entity}.
     */
    protected E findOne() {
        E entity;
        try {
            entity = this.tq.getSingleResult();
        } catch (NoResultException e) {
            entity = null;
        }
        return entity;
    }

    /**
     * Find all matching {@link Entity entities} as stream.
     *
     * @return All matching {@link Entity entities} as stream.
     */
    protected Stream<E> find() {
        return this.tq.getResultStream();
    }

    /**
     * Find some matching {@link Entity entities} as stream, bounded by result
     * options.
     *
     * @param options The result options.
     * @return Some matching {@link Entity entities} as stream, bounded by position
     *         and number.
     */
    protected Stream<E> find(ResultOptions options) {
        TypedQuery<E> tq = this.tq;
        if (options.getStartPosition() > 0) {
            tq = tq.setFirstResult(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            tq = tq.setMaxResults(options.getResultCount());
        }
        return tq.getResultStream();
    }

    /**
     * Find some matching {@link Entity entities} as stream, bounded by result
     * options.
     *
     * @param options The result options.
     * @return Some matching {@link Entity entities} as stream, bounded by position
     *         and number.
     */
    protected PagingResult<E> find(PagingOptions options) {
        Predicate mainPredicate = mainPredicateGetter == null ? null : mainPredicateGetter.apply(b, r);
        Predicate secondaryPredicate = secondaryPredicateGetter == null ? null : secondaryPredicateGetter.apply(r);

        final CriteriaQuery<Long> cqCnt = b.createQuery(Long.class);
        cqCnt.select(b.count(cqCnt.from(this.entityClass)));
        if (mainPredicate != null) {
            if (secondaryPredicate != null) {
                cqCnt.where(mainPredicate, secondaryPredicate);
            } else {
                cqCnt.where(mainPredicate);
            }
        } else if (secondaryPredicate != null) {
            cqCnt.where(secondaryPredicate);
        }

        if (options.getStartPosition() > 0) {
            tq = tq.setFirstResult(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            tq = tq.setMaxResults(options.getResultCount());
        }

        TypedQuery<Long> tqCnt = access.createQuery(cqCnt);
        this.parameterSetter.accept(tqCnt);

        options.setRowCount(tqCnt.getSingleResult());
        return new PagingResult<>(options, tq.getResultStream());
    }

    // endregion Methods

}
