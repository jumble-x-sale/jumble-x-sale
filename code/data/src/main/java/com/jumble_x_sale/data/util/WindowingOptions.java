package com.jumble_x_sale.data.util;

public final class WindowingOptions extends ResultOptions {

    // region Fields

    private int startPosition;
    private int resultCount;

    // endregion Fields

    // region Getters

    public int getStartPosition() {
        return startPosition;
    }

    public int getResultCount() {
        return resultCount;
    }

    // endregion Getters

    // region Setters

    public void setStartPosition(int startPosition) {
        this.startPosition = startPosition;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    // endregion Setters

    // region Constructors

    protected WindowingOptions(int startPosition, int resultCount) {
        this.startPosition = startPosition < 0 ? 0 : startPosition;
        this.resultCount = resultCount < 0 ? 0 : resultCount;
    }

    public static WindowingOptions allRows() {
        return new WindowingOptions(0, 0);
    }

    // endregion Constructors

}
