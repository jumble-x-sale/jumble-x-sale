package com.jumble_x_sale.data.util;

public abstract class ResultOptions {

    // region Getters

    public abstract int getStartPosition();

    public abstract int getResultCount();

    // endregion Getters

    // region Methods

    public static WindowingOptions withWindowing(int startPosition, int resultCount) {
        return new WindowingOptions(startPosition, resultCount);
    }

    public static WindowingOptions allRows() {
        return new WindowingOptions(0, 0);
    }

    public static PagingOptions withPaging(int pageNumber, int pageSize) {
        return new PagingOptions(pageNumber, pageSize, 0);
    }

    public static PagingOptions withPaging() {
        return new PagingOptions(0, 0, 0);
    }

    // endregion Methods

}
