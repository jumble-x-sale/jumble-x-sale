package com.jumble_x_sale.data.util;

import com.jumble_x_sale.util.TimeUtils;
import com.jumble_x_sale.util.Utils;

import javax.json.bind.serializer.JsonbDeserializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.annotation.JsonbTypeDeserializer;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.DeserializationContext;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.UUID;

public final class SingleValueFilter extends Filter {

    // region Inner Classes

    /**
     * An operator that takes one operand.
     */
    public static enum Operator {
        /**
         * The given value must be equaled.
         */
        EQUAL_TO(EQUAL_TO_CODE),
        /**
         * The given value must not be equaled.
         */
        NOT_EQUAL_TO(NOT_EQUAL_TO_CODE),
        /**
         * The given value must not be matched.
         */
        LESS_THAN_OR_EQUAL_TO(LESS_THAN_OR_EQUAL_TO_CODE),
        /**
         * The given value must not be matched.
         */
        GREATER_THAN_OR_EQUAL_TO(GREATER_THAN_OR_EQUAL_TO_CODE),
        /**
         * The given value must not be matched.
         */
        LESS_THAN(LESS_THAN_CODE),
        /**
         * The given value must not be matched.
         */
        GREATER_THAN(GREATER_THAN_CODE),
        /**
         * The given value is a pattern to be matched.
         */
        LIKE(LIKE_CODE),
        /**
         * The given value is a pattern to be matched.
         */
        NOT_LIKE(NOT_LIKE_CODE);

        private static final HashMap<String, Operator> valuesByCode = new HashMap<>();
        static {
            Operator[] types = Operator.values();
            for (int i = 0; i < types.length; i++) {
                valuesByCode.put(types[i].getCode(), types[i]);
            }
        }

        private final String code;

        private Operator(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }

        public static Operator tryValueOf(String idOrCode, Operator defaultValue) {
            idOrCode = idOrCode.toUpperCase();
            Operator value = valuesByCode.get(idOrCode);
            if (value == null) {
                value = Utils.tryValueOf(Operator.class, idOrCode, defaultValue);
            }
            return value;
        }

        public static Operator tryValueOf(String idOrCode) {
            return tryValueOf(idOrCode, null);
        }
    }

    protected static final String EQUAL_TO_CODE = "EQ";
    protected static final String NOT_EQUAL_TO_CODE = "NEQ";
    protected static final String LESS_THAN_OR_EQUAL_TO_CODE = "LTEQ";
    protected static final String GREATER_THAN_OR_EQUAL_TO_CODE = "GTEQ";
    protected static final String LESS_THAN_CODE = "LT";
    protected static final String GREATER_THAN_CODE = "GT";
    protected static final String LIKE_CODE = "LK";
    protected static final String NOT_LIKE_CODE = "NLK";

    protected static final String PN_VALUE = "value";

    public static class OperatorSerializer
            implements JsonbSerializer<Operator>, JsonbDeserializer<Operator> {
        @Override
        public void serialize(Operator obj, JsonGenerator generator, SerializationContext ctx) {
            generator.write(obj.getCode());
        }

        @Override
        public Operator deserialize(JsonParser parser, DeserializationContext ctx, Type rtType) {
            return Operator.tryValueOf(parser.getString());
        }
    }

    // endregion Inner Classes

    // region Fields

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    private Operator operator;

    private String value;

    // endregion Fields

    // region Getters

    @JsonbTypeSerializer(value = OperatorSerializer.class)
    public Operator getOperator() {
        return this.operator;
    }

    public String getValue() {
        return this.value;
    }

    // endregion Getters

    // region Setters

    @JsonbTypeDeserializer(value = OperatorSerializer.class)
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public void setValue(String value) {
        this.value = value;
    }

    // endregion Setters

    // region Constructors

    protected SingleValueFilter(String propertyName, DataType propertyType, Operator operator,
            String value) {
        super(propertyName, propertyType);
        this.operator = operator;
        this.value = value;
    }

    public SingleValueFilter() {
        this(null, null, null, null);
    }

    // endregion Constructors

    // region Methods

    public UUID valueAsUUID() {
        return Utils.uuidFromString(this.value);
    }

    public BigDecimal valueAsDecimal() {
        return Utils.parseBigDecimal(this.value);
    }

    public Instant valueAsInstant() {
        return TimeUtils.parseInstant(this.value);
    }

    public LocalDate valueAsDate() {
        return TimeUtils.parseLocalDate(this.value);
    }

    public LocalDateTime valueAsDateTime() {
        return TimeUtils.parseLocalDateTime(this.value);
    }

    public LocalTime valueAsTime() {
        return TimeUtils.parseLocalTime(this.value);
    }

    // endregion Methods

}
