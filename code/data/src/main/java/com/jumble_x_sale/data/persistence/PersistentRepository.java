package com.jumble_x_sale.data.persistence;

import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.util.Filter;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.WindowingOptions;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * A mock {@link Repository repository} for {@link Entity entities}.
 *
 * @param <PU> The concrete {@link PersistentUnitOfWork persistent unit of
 *             work}.
 * @param <U>  The concrete {@link UnitOfWork unit of work}.
 * @param <E>  The concrete {@link Entity entity}.
 */
public abstract class PersistentRepository<PU extends PersistentUnitOfWork<U>, U extends UnitOfWork, E extends Entity>
        implements Repository<U, E> {

    // region Fields

    protected final PU unitOfWork;
    protected final EntityAccess<E, UUID> entityAccess;
    protected final Class<E> entityClass;
    protected final CriteriaBuilder cb;
    protected final Stream<E> emptyResult;

    // endregion Fields

    // region Getters

    /**
     * Get the {@link UnitOfWork unit of work}.
     */
    @SuppressWarnings("unchecked")
    public U getUnitOfWork() {
        return (U) this.unitOfWork;
    }

    // endregion Getters

    // region Constructors

    protected PersistentRepository(PU unitOfWork, EntitySession entitySession, Class<E> entityClass) {
        this.unitOfWork = unitOfWork;
        this.entityAccess = new EntityAccess<>(entitySession, entityClass);
        this.entityClass = entityClass;
        this.cb = this.entityAccess.getCriteriaBuilder();
        this.emptyResult = Stream.of();
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(E entity) {
        this.entityAccess.add(entity);
        if (this.unitOfWork.autoFlush) {
            this.unitOfWork.flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E merge(E entity) {
        E result = this.entityAccess.merge(entity);
        if (this.unitOfWork.autoFlush) {
            this.unitOfWork.flush();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(E entity) {
        boolean result = this.entityAccess.remove(entity);
        if (this.unitOfWork.autoFlush) {
            this.unitOfWork.flush();
        }
        return result;
    }

    /**
     * Get the {@link Entity entity} with the given ID from the repository.
     *
     * @param id The {@link UUID} of the wanted {@link Entity entity}.
     * @return The {@link Entity entity} with the given ID.
     */
    public E getById(final UUID id) {
        return this.entityAccess.findByPk(id);
    }

    /**
     * {@inheritDoc}
     */
    public Stream<E> get(List<Filter> filters, WindowingOptions options) {
        return this.entityAccess.find(this.getPropertyNameMappings(), filters, options);
    }

    /**
     * {@inheritDoc}
     */
    public PagingResult<E> get(List<Filter> filters, PagingOptions options) {
        return this.entityAccess.find(this.getPropertyNameMappings(), filters, options);
    }

    // endregion Methods

}
