package com.jumble_x_sale.data.persistence;

import com.jumble_x_sale.util.CheckedAutoCloseableFinalBase;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Represents a generic context for a set of related entities.
 */
public final class EntityContext extends CheckedAutoCloseableFinalBase {

    // region Fields

    protected final EntityManagerFactory factory;

    // endregion Fields

    // region Constructors

    /**
     * Create an generic context for access to different kinds of entities.
     * 
     * @param persistenceUnitName The name of persistence unit to bind the context
     *                            to.
     */
    protected EntityContext(String persistenceUnitName) {
        this.factory = Persistence.createEntityManagerFactory(persistenceUnitName);
    }

    // endregion Constructors

    // region Methods

    /**
     * Close this persistence context.
     */
    @Override
    public void close() throws Exception {
        if (this.factory.isOpen()) {
            this.factory.close();
        }
        super.close();
    }

    // endregion Methods

}
