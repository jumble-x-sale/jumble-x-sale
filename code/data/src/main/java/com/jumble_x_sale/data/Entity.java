package com.jumble_x_sale.data;

import java.util.UUID;
import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Version;
import javax.persistence.PrePersist;

/**
 * Some kind of entity.
 */
@MappedSuperclass
public abstract class Entity implements Serializable {

    // region Fields

    private static final long serialVersionUID = 4457690491178608875L;

    @Id
    @Column(columnDefinition = "binary(16)", nullable = false, updatable = false)
    private UUID id;

    @Version
    @Column(name = "Version", nullable = false)
    private long version;

    // endregion Fields

    // region Methods

    protected Entity() {
        this.id = null;
    }

    protected Entity(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return this.id;
    }

    protected void setId(UUID value) {
        this.id = value;
    }

    @PrePersist
    public void onPrePersist() {
        if (this.id == null) {
            this.id = UUID.randomUUID();
        }
    }

    // endregion Methods

}
