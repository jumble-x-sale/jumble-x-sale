package com.jumble_x_sale.data.persistence;

import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.Filter;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.util.MultiValueFilter;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.SingleValueFilter;
import com.jumble_x_sale.data.util.DualValueFilter;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Represents a generic access to an entity.
 * 
 * @param <E>  The concrete type of entity.
 * @param <PK> The of primary key used in the entity.
 */
public final class EntityAccess<E extends Serializable, PK extends Serializable> {

    // region Fields

    protected final EntitySession session;
    protected final EntityManager manager;
    protected final Class<E> entityClass;
    protected final CriteriaBuilder cb;

    // endregion Fields

    // region Getters

    public EntitySession getContext() {
        return this.session;
    }

    public Class<E> getEntityType() {
        return this.entityClass;
    }

    /**
     * Get a criteria builder.
     * 
     * @return A criteria builder.
     */
    public CriteriaBuilder getCriteriaBuilder() {
        return this.cb;
    }

    // endregion Getters

    // region Constructors

    /**
     * Create a generic access to an entity.
     * 
     * @param session     The session to which this entity access belongs.
     * @param entityClass The concrete entity class.
     */
    protected EntityAccess(final EntitySession session, Class<E> entityClass) {
        this.session = session;
        this.manager = session.manager;
        this.entityClass = entityClass;
        this.cb = this.manager.getCriteriaBuilder();
    }

    // endregion Constructors

    // region Methods

    /**
     * Get a typed query.
     * 
     * @param T             The query result type.
     * @param criteriaQuery The criteria query for which to create the typed query.
     * @return A typed query.
     */
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
        return this.manager.createQuery(criteriaQuery);
    }

    /**
     * Add an {@link Entity entity}.
     *
     * @param entity The given {@link Entity entity}.
     * @return The given {@link Entity entity}.
     */
    public E add(E entity) {
        this.manager.persist(entity);
        return entity;
    }

    /**
     * Add an {@link Entity entity}.
     * 
     * @param entity The given {@link Entity entity}.
     * @return The merged {@link Entity entity}.
     */
    public E merge(E entity) {
        return this.manager.merge(entity);
    }

    /**
     * Remove an {@link Entity entity}.
     *
     * @param entity The given {@link Entity entity}.
     * @return An {@link Entity entity}.
     */
    public boolean remove(Object entity) {
        try {
            this.manager.remove(entity);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    /**
     * Remove the given {@link Entity entity} to the repository.
     * 
     * @param primaryKey The primary key of the {@link Entity entity} to remove.
     * @return Could the entity be removed?
     */
    public boolean removeByPk(PK primaryKey) {
        return this.remove(this.findByPk(primaryKey));
    }

    /**
     * Remove the given {@link Entity entity} to the repository.
     * 
     * @param entities The given {@link Entity entities}.
     */
    public void remove(Iterable<E> entities) {
        for (E entity : entities) {
            this.remove(entity);
        }
    }

    /**
     * Find an {@link Entity entity} by its primary key.
     *
     * @param primaryKey The primary key of the {@link Entity entity} to find.
     * @return An {@link Entity entity} by its primary key.
     */
    public E findByPk(Object primaryKey) {
        return this.manager.find(this.entityClass, primaryKey);
    }

    /**
     * Find all {@link Entity entities} in the {@link Repository repository}.
     *
     * @return All {@link Entity entities} in the {@link Repository repository}.
     */
    public Stream<E> find() {
        return this.find(null, Filter.EMPTY_LIST, ResultOptions.allRows());
    }

    /**
     * Find some {@link Entity entities} in the {@link Repository repository}.
     *
     * @param propertyNameMappings The path mappings for properties.
     * @param filters              The {@link Filter filters} for which to create the path.
     * @param options              The {@link ResultOptions result options} for which to create the
     *                             path.
     * @return A page of {@link Entity entities} in the {@link Repository repository}.
     */
    public Stream<E> find(Map<String, String[]> propertyNameMappings, List<Filter> filters,
            ResultOptions options) {
        Filter[] filterArray = filters.stream().toArray(Filter[]::new);

        final CriteriaQuery<E> q = cb.createQuery(this.entityClass);
        final Root<E> r = q.from(this.entityClass);

        q.select(r);

        if (filters != null && filters.size() > 0) {
            q.where(this.predicate(r, propertyNameMappings, filterArray));
        }

        TypedQuery<E> tq = this.manager.createQuery(q);
        if (options.getStartPosition() > 0) {
            tq = tq.setFirstResult(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            tq = tq.setMaxResults(options.getResultCount());
        }
        return tq.getResultStream();
    }

    /**
     * Find some {@link Entity entities} in the {@link Repository repository}.
     *
     * @param propertyNameMappings The path mappings for properties.
     * @param filters              The {@link Filter filters} for which to create the path.
     * @param options              The {@link PagingOptions result options} for which to create the
     *                             path.
     * @return A page of {@link Entity entities} in the {@link Repository repository}.
     */
    public PagingResult<E> find(Map<String, String[]> propertyNameMappings, List<Filter> filters,
            PagingOptions options) {
        Filter[] filterArray = filters.stream().toArray(Filter[]::new);

        final CriteriaQuery<Long> cqCnt = cb.createQuery(Long.class);
        final CriteriaQuery<E> cq = cb.createQuery(this.entityClass);
        final Root<E> r = cq.from(this.entityClass);
        final Root<E> rCnt = cqCnt.from(this.entityClass);

        cq.select(r);
        cqCnt.select(cb.count(rCnt));

        if (filters != null && filters.size() > 0) {
            cq.where(this.predicate(r, propertyNameMappings, filterArray));
            cqCnt.where(this.predicate(rCnt, propertyNameMappings, filterArray));
        }

        TypedQuery<E> tq = this.manager.createQuery(cq);
        if (options.getStartPosition() > 0) {
            tq = tq.setFirstResult(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            tq = tq.setMaxResults(options.getResultCount());
        }

        options.setRowCount(this.manager.createQuery(cqCnt).getSingleResult());
        return new PagingResult<>(options, tq.getResultStream());
    }

    /**
     * Create a {@link Path path} corresponding to the referenced attribute.
     *
     * @param root                 The root for the path.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link Filter filter} for which to create the path.
     * @return A {@link Path path} corresponding to the referenced attribute.
     */
    private <Y> Path<Y> get(Root<E> root, Map<String, String[]> propertyNameMappings,
            Filter filter) {
        if (propertyNameMappings != null) {
            String[] propertyNameMapping = propertyNameMappings.get(filter.getPropertyName());
            if (propertyNameMapping.length > 0) {
                Path<Y> path;
                path = root.get(propertyNameMapping[0]);
                for (int i = 1; i < propertyNameMapping.length; i++) {
                    path = path.get(propertyNameMapping[i]);
                }
                return path;
            }
        }
        return root.get(filter.getPropertyName());
    }

    /**
     * Create a predicate from a {@link SingleValueFilter SingleValueFilter}.
     *
     * @param root                 The root to apply the filter to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link SingleValueFilter SingleValueFilter} to create a
     *                             predicate from.
     * @return A predicate from a {@link SingleValueFilter SingleValueFilter}.
     */
    private Predicate predicate(Root<E> root, Map<String, String[]> propertyNameMappings,
            SingleValueFilter filter) {
        Predicate predicate = null;
        switch (filter.getOperator()) {
            case EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = cb.equal(this.get(root, propertyNameMappings, filter),
                                filter.getValue());
                        break;
                }
                break;
            case NOT_EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = cb.notEqual(this.get(root, propertyNameMappings, filter),
                                filter.getValue());
                        break;
                }
                break;
            case LESS_THAN_OR_EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.lessThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate =
                                cb.lessThanOrEqualTo(this.get(root, propertyNameMappings, filter),
                                        filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate =
                                cb.lessThanOrEqualTo(this.get(root, propertyNameMappings, filter),
                                        filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.lessThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.valueAsTime());
                        break;
                    case DATE_TIME:
                        predicate =
                                cb.lessThanOrEqualTo(this.get(root, propertyNameMappings, filter),
                                        filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.lessThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = cb.lessThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.getValue());
                        break;
                }
                break;
            case LESS_THAN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = cb.lessThan(this.get(root, propertyNameMappings, filter),
                                filter.getValue());
                        break;
                }
                break;
            case GREATER_THAN_OR_EQUAL_TO:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter),
                                filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter),
                                filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.valueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter),
                                filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.getValue());
                        break;
                }
                break;
            case GREATER_THAN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsUUID());
                        break;
                    case NUMBER:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.valueAsTime());
                        break;
                    case STRING:
                        predicate = cb.greaterThan(this.get(root, propertyNameMappings, filter),
                                filter.getValue());
                        break;
                }
                break;
            case LIKE:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case NUMBER:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case INSTANT:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case DATE:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case DATE_TIME:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case TIME:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case STRING:
                        predicate = cb.like(this.get(root, propertyNameMappings, filter),
                                filter.getValue());
                        break;
                }
                break;
            case NOT_LIKE:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case NUMBER:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case INSTANT:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case DATE:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case DATE_TIME:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case TIME:
                        predicate = cb.like(
                                this.get(root, propertyNameMappings, filter).as(String.class),
                                filter.getValue());
                        break;
                    case STRING:
                        predicate = cb.like(this.get(root, propertyNameMappings, filter),
                                filter.getValue());
                        break;
                }
                break;
        }
        return predicate;
    }

    /**
     * Create a predicate from a {@link DualValueFilter DualValueFilter}.
     *
     * @param root                 The root to apply the filter to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link DualValueFilter DualValueFilter} to create a predicate
     *                             from.
     * @return A predicate from a {@link DualValueFilter DualValueFilter}.
     */
    private Predicate predicate(Root<E> root, Map<String, String[]> propertyNameMappings,
            DualValueFilter filter) {
        Predicate predicate = null;
        switch (filter.getOperator()) {
            case BETWEEN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.minValueAsUUID(), filter.maxValueAsUUID());
                        break;
                    case NUMBER:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.minValueAsDecimal(), filter.maxValueAsDecimal());
                        break;
                    case INSTANT:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.minValueAsInstant(), filter.maxValueAsInstant());
                        break;
                    case DATE:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.minValueAsDate(), filter.maxValueAsDate());
                        break;
                    case DATE_TIME:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.minValueAsDateTime(), filter.maxValueAsDateTime());
                        break;
                    case TIME:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.minValueAsTime(), filter.maxValueAsTime());
                        break;
                    case STRING:
                        predicate = cb.between(this.get(root, propertyNameMappings, filter),
                                filter.getMinValue(), filter.getMaxValue());
                        break;
                }
                break;
            case IN_BETWEEN:
                switch (filter.getPropertyType()) {
                    case UUID:
                        predicate = cb.and(
                                cb.greaterThanOrEqualTo(
                                        this.get(root, propertyNameMappings, filter),
                                        filter.minValueAsUUID()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.maxValueAsUUID()));
                        break;
                    case NUMBER:
                        predicate = cb.and(
                                cb.greaterThanOrEqualTo(
                                        this.get(root, propertyNameMappings, filter),
                                        filter.minValueAsDecimal()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.maxValueAsDecimal()));
                        break;
                    case INSTANT:
                        predicate = cb.and(
                                cb.greaterThanOrEqualTo(
                                        this.get(root, propertyNameMappings, filter),
                                        filter.minValueAsInstant()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.maxValueAsInstant()));
                        break;
                    case DATE:
                        predicate = cb.and(
                                cb.greaterThanOrEqualTo(
                                        this.get(root, propertyNameMappings, filter),
                                        filter.minValueAsDate()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.maxValueAsDate()));
                        break;
                    case DATE_TIME:
                        predicate = cb.and(
                                cb.greaterThanOrEqualTo(
                                        this.get(root, propertyNameMappings, filter),
                                        filter.minValueAsDateTime()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.maxValueAsDateTime()));
                        break;
                    case TIME:
                        predicate = cb.and(
                                cb.greaterThanOrEqualTo(
                                        this.get(root, propertyNameMappings, filter),
                                        filter.minValueAsTime()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.maxValueAsTime()));
                        break;
                    case STRING:
                        predicate = cb.and(cb.greaterThanOrEqualTo(
                                this.get(root, propertyNameMappings, filter), filter.getMinValue()),
                                cb.lessThan(this.get(root, propertyNameMappings, filter),
                                        filter.getMaxValue()));
                        break;
                }
                break;
        }
        return predicate;
    }

    /**
     * Create a predicate from a {@link MultiValueFilter MultiValueFilter}.
     *
     * @param root                 The root to apply the filter to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filter               The {@link MultiValueFilter MultiValueFilter} to create a
     *                             predicate from.
     * @return A predicate from a {@link MultiValueFilter MultiValueFilter}.
     */
    private Predicate predicate(Root<E> root, Map<String, String[]> propertyNameMappings,
            MultiValueFilter filter) {
        Predicate predicate = null;
        switch (filter.getPropertyType()) {
            case UUID:
                predicate = this.get(root, propertyNameMappings, filter).in(filter.valuesAsUUID());
                break;
            case NUMBER:
                predicate =
                        this.get(root, propertyNameMappings, filter).in(filter.valuesAsDecimal());
                break;
            case INSTANT:
                predicate =
                        this.get(root, propertyNameMappings, filter).in(filter.valuesAsInstant());
                break;
            case DATE:
                predicate = this.get(root, propertyNameMappings, filter).in(filter.valuesAsDate());
                break;
            case DATE_TIME:
                predicate =
                        this.get(root, propertyNameMappings, filter).in(filter.valuesAsDateTime());
                break;
            case TIME:
                predicate = this.get(root, propertyNameMappings, filter).in(filter.valuesAsTime());
                break;
            case STRING:
                predicate = this.get(root, propertyNameMappings, filter).in(filter.values());
                break;
        }
        if (filter.getOperator() == MultiValueFilter.Operator.NOT_IN) {
            predicate = cb.not(predicate);
        }
        return predicate;
    }

    /**
     * Create a predicate from filters.
     *
     * @param root                 The root to apply the filters to.
     * @param propertyNameMappings The path mappings for properties.
     * @param filters              The filters to create a predicate from.
     * @return A predicate from filters.
     */
    public Predicate predicate(Root<E> root, Map<String, String[]> propertyNameMappings,
            Filter[] filters) {
        Predicate[] predicates = new Predicate[filters.length];
        for (int i = 0; i < filters.length; i++) {
            if (filters[i] instanceof SingleValueFilter) {
                predicates[i] =
                        this.predicate(root, propertyNameMappings, (SingleValueFilter) filters[i]);
            } else if (filters[i] instanceof DualValueFilter) {
                predicates[i] =
                        this.predicate(root, propertyNameMappings, (DualValueFilter) filters[i]);
            } else if (filters[i] instanceof MultiValueFilter) {
                predicates[i] =
                        this.predicate(root, propertyNameMappings, (MultiValueFilter) filters[i]);
            }
        }
        return cb.and(predicates);
    }

    // endregion Methods

}
