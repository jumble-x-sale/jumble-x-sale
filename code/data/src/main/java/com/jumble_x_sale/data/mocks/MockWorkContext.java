package com.jumble_x_sale.data.mocks;

import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.data.WorkContext;
import com.jumble_x_sale.util.CheckedAutoCloseableBase;

/**
 * An abstract persistence unit of work that holds entity repositories.
 */
public abstract class MockWorkContext<U extends UnitOfWork> extends CheckedAutoCloseableBase implements WorkContext<U> {
}
