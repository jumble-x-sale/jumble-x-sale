package com.jumble_x_sale.data.persistence;

import java.util.Set;
import java.util.HashSet;

import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.data.WorkContext;
import com.jumble_x_sale.util.CheckedAutoCloseableBase;

/**
 * An abstract persistent unit of work that holds entity repositories.
 * 
 * A persistent work context may have more than one data sources and thus more
 * than one {@link EntityContext entity context}.
 */
public abstract class PersistentWorkContext<U extends UnitOfWork> extends CheckedAutoCloseableBase
        implements WorkContext<U> {

    // region Fields

    protected final Set<EntityContext> contexts;

    // endregion Fields

    // region Constructors

    protected PersistentWorkContext() {
        this.contexts = new HashSet<>();
    }

    // endregion Constructors

    // region Methods

    /**
     * Create a new entity context with the given name.
     * 
     * @param persistenceUnitName The identifying name of entity context.
     * @return A new entity context with the given name.
     */
    protected EntityContext createContext(String persistenceUnitName) {
        EntityContext context = new EntityContext(persistenceUnitName);
        this.contexts.add(context);
        return context;
    }

    /**
     * Close this persistence context.
     */
    @Override
    public void close() throws Exception {
        for (EntityContext context : this.contexts) {
            // We ignore possible individual exceptions in order to keep on closing all
            // resources.
            context.tryClose();
        }
        super.close();
    }

    // endregion Methods

}
