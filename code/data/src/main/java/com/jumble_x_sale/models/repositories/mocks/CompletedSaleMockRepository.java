package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.CompletedSale;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSaleRepository;
import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for completed sales.
 */
public final class CompletedSaleMockRepository
        extends MockRepository<JumbleUnitOfWork, CompletedSale> implements CompletedSaleRepository {

    public CompletedSaleMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, CompletedSale.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(CompletedSale entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSale> getByBuyer(User buyer, ResultOptions options) {
        Stream<CompletedSale> stream =
                StreamSupport.stream(this.getByBuyer(buyer).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSale> getByBuyer(User buyer, PagingOptions options) {
        Stream<CompletedSale> stream =
                StreamSupport.stream(this.getByBuyer(buyer).spliterator(), false);
        options.setRowCount(stream.count());
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return new PagingResult<>(options, stream);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSale> getByBuyer(User buyer) {
        return this.getByPredicate(t -> t.getBuyer().getId().equals(buyer.getId()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSale> getByCreator(UUID creatorId, ResultOptions options) {
        Stream<CompletedSale> stream =
                StreamSupport.stream(this.getByCreator(creatorId).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSale> getByCreator(UUID creatorId, FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> m.getInfo().getCreator().getId().equals(creatorId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSale> getExceptCreator(UUID creatorId,
            FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> !m.getInfo().getCreator().getId().equals(creatorId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSale> getByCreator(UUID creatorId) {
        return this.getByPredicate(t -> t.getInfo().getCreator().getId().equals(creatorId));
    }

}
