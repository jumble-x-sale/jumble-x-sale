package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

import java.time.Instant;
// import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * A text message sent directly from {@link User sender} to {@link User receiver}.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "UserMessageId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "UserMessages")
public class UserMessage extends Entity {

    // region Fields

    private static final long serialVersionUID = -8657926752974325034L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SenderId", nullable = false)
    private User sender;

    @ManyToOne(optional = false)
    @JoinColumn(name = "ReceiverId", nullable = false)
    private User receiver;

    @Column(name = "CreationInstant", nullable = false)
    private Instant creationInstant;

    @Column(name = "Message", nullable = false)
    private String message;

    // endregion Fields

    // region Getters

    public User getReceiver() {
        return this.receiver;
    }

    public User getSender() {
        return this.sender;
    }

    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getMessage() {
        return this.message;
    }

    // endregion Getters

    // region Setters

    public void setReceiver(User value) {
        this.receiver = value;
    }

    public void setSender(User value) {
        this.sender = value;
    }

    public void setCreationInstant(Instant value) {
        this.creationInstant = value;
    }

    public void setMessage(String value) {
        this.message = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public UserMessage() {
        this.receiver = null;
        this.sender = null;
        this.creationInstant = null;
        this.message = null;
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public UserMessage(UserMessage other) {
        this.receiver = other.receiver;
        this.sender = other.sender;
        this.creationInstant = other.creationInstant;
        this.message = other.message;
    }

    /**
     * Constructor.
     * 
     * @param receiver The {@link User user} who receives the message.
     * @param sender   The {@link User user} who sends the message.
     * @param message  The text of the message.
     */
    public UserMessage(User receiver, User sender, String message) {
        this.receiver = receiver;
        this.sender = sender;
        this.message = message;
        this.creationInstant = Instant.now();
    }

    // endregion Constructors

}
