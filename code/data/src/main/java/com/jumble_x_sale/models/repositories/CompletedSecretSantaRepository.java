package com.jumble_x_sale.models.repositories;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.SecretSantaOffer;
import com.jumble_x_sale.models.CompletedSecretSanta;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * A repository for completed secret santas.
 */
public interface CompletedSecretSantaRepository
        extends Repository<JumbleUnitOfWork, CompletedSecretSanta> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a {@link CompletedSecretSanta completedSecretSanta} by matching 2
     * {@link SecretSantaOffer secret santa offer}.
     * 
     * @param match1 The first match.
     * @param match2 The second match.
     * @return A completed Secret Santa.
     */
    default CompletedSecretSanta create(SecretSantaOffer match1, SecretSantaOffer match2) {
        CompletedSecretSanta completedSecretSanta =
                new CompletedSecretSanta(Instant.now(), match1.getInfo(), match2.getInfo());
        this.getUnitOfWork().getCompletedSecretSantaRepository().add(completedSecretSanta);
        return completedSecretSanta;
    }

    /**
     * Provides a list of all {@link CompletedSecretSanta completedSecretSanta} assigned to a
     * {@link User member}
     * 
     * @param member  The member where the completed secret santas are assigned to.
     * @param options The result options.
     * @return A stream of {@link CompletedSecretSanta completedSecretSanta}
     */
    public Stream<CompletedSecretSanta> getByMember(User member, ResultOptions options);

    /**
     * Provides a list of all {@link CompletedSecretSanta completedSecretSanta} assigned to a
     * {@link User member}
     * 
     * @param member  The member where the completed secret santas are assigned to.
     * @param options The result options.
     * @return A stream of {@link CompletedSecretSanta completedSecretSanta}
     */
    public PagingResult<CompletedSecretSanta> getByMember(User member, FilterPagingOptions options);

    /**
     * Provides a list of all {@link CompletedSecretSanta completedSecretSanta} assigned to a
     * {@link User member}
     * 
     * @param member The member where the completed secret santas are assigned to.
     * @return A stream of {@link CompletedSecretSanta completedSecretSanta}
     */
    default Stream<CompletedSecretSanta> getByMember(User member) {
        return this.getByMember(member, ResultOptions.allRows());
    }

    // endregion Methods

}
