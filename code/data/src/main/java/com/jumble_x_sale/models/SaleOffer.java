package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
// import java.io.Serializable;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * SaleOffer contains all the information about a sale offer.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "SaleOfferId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "SaleOffers")
public class SaleOffer extends Entity {

    // region Fields

    private static final long serialVersionUID = 2406544252183042001L;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "creator", joinColumns = @JoinColumn(name = "CreatorId"))})
    private PricedItemInfo info;

    @OneToMany(mappedBy = "saleOffer", orphanRemoval = true, cascade = {CascadeType.ALL})
    private List<SaleOfferMessage> messages = new ArrayList<>();

    // endregion Fields

    // region Getters

    public PricedItemInfo getInfo() {
        return this.info;
    }

    public List<SaleOfferMessage> getMessages() {
        return this.messages;
    }

    // endregion Getters

    // region Setters

    public void setInfo(PricedItemInfo value) {
        this.info = value;
    }

    public void setMessages(List<SaleOfferMessage> value) {
        this.messages = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor
     */
    public SaleOffer() {
        this.info = null;
    }

    /**
     * Constructor
     * 
     * @param creator         The {@link User creator} of the sale offer.
     * @param creationInstant The the point in time when the {@link SaleOffer sale offer} was
     *                        created (should be "now").
     * @param category        The sale offers category.
     * @param title           The sale offers title.
     * @param description     A description about the sale offer.
     * @param imageUrls       An String array which contains the paths to the sale offer images.
     * @param mainImageData   A byte array which contains the main image data.
     * @param price           The price of the sale offer.
     */
    public SaleOffer(User creator, Instant creationInstant, String category, String title,
            String description, String[] imageUrls, byte[] mainImageData, BigDecimal price) {
        this.info = new PricedItemInfo(creator, creationInstant, category, title, description,
                imageUrls, mainImageData, price);
    }

    /**
     * Constructor
     * 
     * @param info The info for this sale offer.
     */
    public SaleOffer(PricedItemInfo info) {
        this.info = info;
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public SaleOffer(SaleOffer other) {
        this.info = new PricedItemInfo(other.info);
    }

    // endregion Constructors

}
