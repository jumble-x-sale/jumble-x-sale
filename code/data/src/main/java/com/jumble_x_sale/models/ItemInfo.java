package com.jumble_x_sale.models;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Column;
import javax.persistence.Access;
import javax.persistence.AccessType;

/**
 * Item info contains information about the {@link User creator}, the category, the title, the
 * description and the image urls of an item.
 */
@MappedSuperclass
@Embeddable
@Access(AccessType.FIELD)
public class ItemInfo implements Serializable {

    // region Fields

    private static final long serialVersionUID = 2887934712097513009L;

    @ManyToOne(optional = false)
    @JoinColumn(nullable = false, insertable = false, updatable = false)
    private User creator;

    @Column(name = "CreationInstant", nullable = false)
    private Instant creationInstant;

    @Column(name = "Category", nullable = false)
    private String category;

    @Column(name = "Title", nullable = false)
    private String title;

    @Column(name = "Description", nullable = false)
    private String description;

    @Column(name = "ImageUrls")
    private String[] imageUrls;

    @Lob
    @Column(name = "MainImageData")
    private byte[] mainImageData;

    // endregion Fields

    // region Getters

    public User getCreator() {
        return this.creator;
    }

    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getCategory() {
        return this.category;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return this.description;
    }

    public String[] getImageUrls() {
        return this.imageUrls;
    }

    public byte[] getMainImageData() {
        return this.mainImageData;
    }

    // endregion Getters

    // region Setters

    public void setCreator(User value) {
        this.creator = value;
    }

    public void setCreationInstant(Instant value) {
        this.creationInstant = value;
    }

    public void setCategory(String value) {
        this.category = value;
    }

    public void setTitle(String value) {
        this.title = value;
    }

    public void setDescription(String value) {
        this.description = value;
    }

    public void setImageUrls(String[] value) {
        this.imageUrls = value;
    }

    public void setMainImageData(byte[] value) {
        this.mainImageData = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public ItemInfo() {
        this.creator = null;
        this.creationInstant = Instant.now();
        this.category = null;
        this.title = null;
        this.description = null;
        this.imageUrls = null;
        this.mainImageData = null;
    }

    /**
     * Constructor.
     * 
     * @param creator         A {@link User creator} who creates the {@link SecretSantaOffer secret
     *                        santa offer}.
     * @param creationInstant The the point in time when the {@link ItemInfo item} was created
     *                        (should be "now").
     * @param category        The category of the {@link SecretSantaOffer secret santa offer}.
     * @param title           The title of the {@link SecretSantaOffer secret santa offer}.
     * @param description     The description of the {@link SecretSantaOffer secret santa offer}.
     * @param imageUrls       A String array which contains the paths to the sale offer images.
     * @param mainImageData   A byte array which contains the main image data.
     */
    public ItemInfo(User creator, Instant creationInstant, String category, String title,
            String description, String[] imageUrls, byte[] mainImageData) {
        this.creator = creator;
        this.creationInstant = creationInstant;
        this.category = category;
        this.title = title;
        this.description = description;
        this.imageUrls = imageUrls;
        this.mainImageData = mainImageData;
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public ItemInfo(ItemInfo other) {
        this.creator = other.creator;
        this.creationInstant = other.creationInstant;
        this.category = other.category;
        this.title = other.title;
        this.description = other.description;
        this.imageUrls = other.imageUrls;
        this.mainImageData = other.mainImageData;
    }

    // endregion Constructors

}
