package com.jumble_x_sale.models;

import java.util.HashMap;
// import java.io.Serializable;

import com.jumble_x_sale.util.Utils;

/**
 * The type of jumble user.
 */
public enum UserType {// implements Serializable {

    /**
     * A jumble admin.
     */
    ADMIN("A"),
    /**
     * A jumble clerk.
     */
    CLERK("C"),
    /**
     * A jumble member.
     */
    MEMBER("M");

    private static final HashMap<String, UserType> valuesByCode = new HashMap<>();

    private final String code;

    static {
        UserType[] userTypes = UserType.values();
        for (int i = 0; i < userTypes.length; i++) {
            valuesByCode.put(userTypes[i].getCode(), userTypes[i]);
        }
    }

    private UserType(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public static UserType tryValueOf(String idOrCode, UserType defaultValue) {
        UserType value = valuesByCode.get(idOrCode);
        if (value == null) {
            value = Utils.tryValueOf(UserType.class, idOrCode, defaultValue);
        }
        return value;
    }

    public static UserType tryValueOf(String idOrCode) {
        return tryValueOf(idOrCode, null);
    }

}
