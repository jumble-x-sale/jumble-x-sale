package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.persistence.EntityContext;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSaleMessageRepository;
import com.jumble_x_sale.models.repositories.CompletedSaleRepository;
import com.jumble_x_sale.models.repositories.CompletedSecretSantaRepository;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SaleOfferRepository;
import com.jumble_x_sale.models.repositories.SaleOfferMessageRepository;
import com.jumble_x_sale.models.repositories.SecretSantaOfferRepository;
import com.jumble_x_sale.models.repositories.UserMessageRepository;
import com.jumble_x_sale.models.repositories.UserRepository;
import com.jumble_x_sale.models.User;

import java.util.logging.Logger;

/**
 * A persistent jumble unit of work that holds entity {@link Repository
 * repositories}.
 */
public final class JumblePersistentUnitOfWork extends PersistentUnitOfWork<JumbleUnitOfWork>
        implements JumbleUnitOfWork {

    // region Fields

    private static final Logger LOGGER = Logger.getLogger(JumblePersistentUnitOfWork.class.getSimpleName());

    private final EntitySession mainEntitySession;

    private User activeUser = null;
    private UserRepository userRepository = null;
    private UserMessageRepository userMessageRepository = null;
    private SaleOfferRepository saleOfferRepository = null;
    private SaleOfferMessageRepository saleOfferMessageRepository = null;
    private CompletedSaleRepository completedSaleRepository = null;
    private CompletedSaleMessageRepository completedSaleMessageRepository = null;
    private SecretSantaOfferRepository secretSantaOfferRepository = null;
    private CompletedSecretSantaRepository completedSecretSantaRepository = null;

    // endregion Fields

    // region Constructors

    protected JumblePersistentUnitOfWork(JumblePersistentWorkContext workContext, EntityContext mainEntityContext) {
        super(workContext);
        this.mainEntitySession = this.createSession(mainEntityContext);
        LOGGER.info("\"JumblePersistentUnitOfWork\" created.");
    }

    // endregion Constructors

    // region Getters

    public User getActiveUser() {
        return this.activeUser;
    }

    public UserRepository getUserRepository() {
        if (this.userRepository != null) {
            return this.userRepository;
        }
        return (this.userRepository = new UserPersistentRepository(this, mainEntitySession));
    }

    public UserMessageRepository getUserMessageRepository() {
        if (this.userMessageRepository != null) {
            return this.userMessageRepository;
        }
        return (this.userMessageRepository = new UserMessagePersistentRepository(this, mainEntitySession));
    }

    public SaleOfferRepository getSaleOfferRepository() {
        if (this.saleOfferRepository != null) {
            return this.saleOfferRepository;
        }
        return (this.saleOfferRepository = new SaleOfferPersistentRepository(this, mainEntitySession));
    }

    public SaleOfferMessageRepository getSaleOfferMessageRepository() {
        if (this.saleOfferMessageRepository != null) {
            return this.saleOfferMessageRepository;
        }
        return (this.saleOfferMessageRepository = new SaleOfferMessagePersistentRepository(this, mainEntitySession));
    }

    public CompletedSaleRepository getCompletedSaleRepository() {
        if (this.completedSaleRepository != null) {
            return this.completedSaleRepository;
        }
        return (this.completedSaleRepository = new CompletedSalePersistentRepository(this, mainEntitySession));
    }

    public CompletedSaleMessageRepository getCompletedSaleMessageRepository() {
        if (this.completedSaleMessageRepository != null) {
            return this.completedSaleMessageRepository;
        }
        return (this.completedSaleMessageRepository = new CompletedSaleMessagePersistentRepository(this,
                mainEntitySession));
    }

    public SecretSantaOfferRepository getSecretSantaOfferRepository() {
        if (this.secretSantaOfferRepository != null) {
            return this.secretSantaOfferRepository;
        }
        return (this.secretSantaOfferRepository = new SecretSantaOfferPersistentRepository(this, mainEntitySession));
    }

    public CompletedSecretSantaRepository getCompletedSecretSantaRepository() {
        if (this.completedSecretSantaRepository != null) {
            return this.completedSecretSantaRepository;
        }
        return (this.completedSecretSantaRepository = new CompletedSecretSantaPersistentRepository(this,
                mainEntitySession));
    }

    // endregion Getters

    // region Setters

    public void setActiveUser(User value) {
        this.activeUser = value;
    }

    // endregion Setters
}
