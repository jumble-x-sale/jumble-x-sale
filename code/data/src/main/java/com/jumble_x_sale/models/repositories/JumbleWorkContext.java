package com.jumble_x_sale.models.repositories;

import com.jumble_x_sale.data.WorkContext;
import com.jumble_x_sale.models.repositories.mocks.JumbleMockWorkContext;
import com.jumble_x_sale.models.repositories.persistence.JumblePersistentWorkContext;
import com.jumble_x_sale.util.EnvironmentProperties;
import com.jumble_x_sale.util.Utils;

/**
 * A context for working on jumble. Used for managing {@link JumbleUnitOfWork units of work}.
 */
public interface JumbleWorkContext extends WorkContext<JumbleUnitOfWork> {

    static final EnvironmentProperties environmentProperties =
            EnvironmentProperties.get(JumbleWorkContext.class);

    /**
     * Get the name of the environment that is used.
     * 
     * @return The name of the environment that is used.
     */
    public static String getEnvironmentName() {
        return environmentProperties.getEnvironmentName();
    };

    /**
     * Get the name of the persitence unit to use.
     * 
     * @return The name of the persitence unit to use.
     */
    public static String getPersistenceUnitName() {
        return environmentProperties.getProperty("persistenceUnitName");
    };

    /**
     * Create a jumble work context.
     * 
     * @return A jumble work context.
     */
    public static JumbleWorkContext create() {
        String workContextClassName = environmentProperties.getProperty("workContextClassName");

        if (JumblePersistentWorkContext.class.getSimpleName()
                .equalsIgnoreCase(workContextClassName)) {
            return new JumblePersistentWorkContext();
        } else if (JumbleMockWorkContext.class.getSimpleName().equalsIgnoreCase(
                workContextClassName) || Utils.isNullOrWhiteSpace(workContextClassName)) {
            return new JumbleMockWorkContext();
        }
        // Returns "null" when an explicitly given config value is invalid.
        return null;
    }

}
