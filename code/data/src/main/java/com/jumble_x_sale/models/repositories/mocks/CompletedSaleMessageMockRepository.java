package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.CompletedSaleMessage;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSaleMessageRepository;

import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for completed sale messages.
 */
public final class CompletedSaleMessageMockRepository
        extends MockRepository<JumbleUnitOfWork, CompletedSaleMessage>
        implements CompletedSaleMessageRepository {

    public CompletedSaleMessageMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, CompletedSaleMessage.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(CompletedSaleMessage entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSaleMessage> getBySaleCreator(UUID saleCreatorId,
            FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> m.getCompletedSale().getInfo().getCreator().getId().equals(saleCreatorId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSaleMessage> getBySale(UUID completedSaleId, ResultOptions options) {
        Stream<CompletedSaleMessage> stream =
                StreamSupport.stream(this.getBySale(completedSaleId).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSaleMessage> getBySale(UUID completedSaleId,
            PagingOptions options) {
        Stream<CompletedSaleMessage> stream =
                StreamSupport.stream(this.getBySale(completedSaleId).spliterator(), false);
        options.setRowCount(stream.count());
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return new PagingResult<>(options, stream);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText,
            ResultOptions options) {
        Stream<CompletedSaleMessage> stream = StreamSupport
                .stream(this.getByText(completedSaleId, containedText).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText,
            PagingOptions options) {
        Stream<CompletedSaleMessage> stream = StreamSupport
                .stream(this.getByText(completedSaleId, containedText).spliterator(), false);
        options.setRowCount(stream.count());
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return new PagingResult<>(options, stream);
    }

}
