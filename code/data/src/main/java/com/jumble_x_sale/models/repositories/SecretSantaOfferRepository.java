package com.jumble_x_sale.models.repositories;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.ItemInfo;
import com.jumble_x_sale.models.SecretSantaOffer;

/**
 * A repository for secret santas.
 */
public interface SecretSantaOfferRepository extends Repository<JumbleUnitOfWork, SecretSantaOffer> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a new {@link SecretSantaOffer secret santa offer}.
     * 
     * @param creator       A {@link User creator} who creates the {@link SecretSantaOffer secret
     *                      santa offer}.
     * @param category      The category of the {@link SecretSantaOffer secret santa offer}.
     * @param title         The title of the {@link SecretSantaOffer secret santa offer}.
     * @param description   The description of the {@link SecretSantaOffer secret santa offer}.
     * @param imageUrls     An String array which contains the paths to the sale offer images.
     * @param mainImageData A byte array which contains the main image data.
     * @return A new {@link SecretSantaOffer secret santa offer}.
     */
    default SecretSantaOffer create(User creator, String category, String title, String description,
            String[] imageUrls, byte[] mainImageData) {
        SecretSantaOffer secretSanta = new SecretSantaOffer(creator, Instant.now(), category, title,
                description, imageUrls, mainImageData);
        this.getUnitOfWork().getSecretSantaOfferRepository().add(secretSanta);
        return secretSanta;
    }

    /**
     * Get a matching {@link SecretSantaOffer secret santa offer}.
     * 
     * @param secretSantaOffer The {@link SecretSantaOffer secret santa offer} to match with.
     * @return A matching {@link SecretSantaOffer secret santa offer}.
     */
    default SecretSantaOffer match(SecretSantaOffer secretSantaOffer) {
        SecretSantaOffer otherSecretSantaOffer = null;
        ItemInfo secretSantaOfferInfo = secretSantaOffer.getInfo();
        if (secretSantaOfferInfo != null) {
            for (SecretSantaOffer s : this.getUnitOfWork().getSecretSantaOfferRepository()
                    .getByCategory(secretSantaOfferInfo.getCategory())
                    .collect(Collectors.toList())) {
                if (s.getInfo().getCategory().equals(secretSantaOffer.getInfo().getCategory())
                        && !s.getId().equals(secretSantaOffer.getId())) {
                    otherSecretSantaOffer = s;
                    break;
                }
            }
        }
        return otherSecretSantaOffer;
    }

    /**
     * Lists all {@link SecretSantaOffer secret santa offers} belonging to a category.
     * 
     * @param category The category where the {@link SecretSantaOffer secret santa offers} should
     *                 belong to.
     * @param options  The result options.
     * @return A stream of {@link SecretSantaOffer secret santa offers}.
     */
    public Stream<SecretSantaOffer> getByCategory(String category, ResultOptions options);

    /**
     * Lists all {@link SecretSantaOffer secret santa offers} belonging to a category.
     * 
     * @param category The category where the {@link SecretSantaOffer secret santa offers} should
     *                 belong to.
     * @param options  The result options.
     * @return A stream of {@link SecretSantaOffer secret santa offers}.
     */
    public PagingResult<SecretSantaOffer> getByCategory(String category, PagingOptions options);

    /**
     * Lists all {@link SecretSantaOffer secret santa offers} belonging to a category.
     * 
     * @param category The category where the {@link SecretSantaOffer secret santa offers} should
     *                 belong to.
     * @return A stream of {@link SecretSantaOffer secret santa offers}.
     */
    default Stream<SecretSantaOffer> getByCategory(String category) {
        return this.getByCategory(category, ResultOptions.allRows());
    }

    /**
     * Lists all {@link SecretSantaOffer secret santa offers} of a {@link User user}.
     * 
     * @param memberId The id of the creator of the {@link SecretSantaOffer secret santa offers}.
     * @param options  The result options.
     * @return A stream of {@link SecretSantaOffer secret santa offers}.
     */
    public Stream<SecretSantaOffer> getByCreator(UUID memberId, ResultOptions options);

    /**
     * Lists all {@link SecretSantaOffer secret santa offers} of a {@link User user}.
     * 
     * @param memberId The id of the creator of the {@link SecretSantaOffer secret santa offers}.
     * @param options  The result options.
     * @return A stream of {@link SecretSantaOffer secret santa offers}.
     */
    public PagingResult<SecretSantaOffer> getByCreator(UUID memberId, PagingOptions options);

    /**
     * Lists all {@link SecretSantaOffer secret santa offers} of a {@link User user}.
     * 
     * @param memberId The id of the creator of the {@link SecretSantaOffer secret santa offers}.
     * @return A stream of {@link SecretSantaOffer secret santa offers}.
     */
    default Stream<SecretSantaOffer> getByCreator(UUID memberId) {
        return this.getByCreator(memberId, ResultOptions.allRows());
    }

    // endregion Methods

}
