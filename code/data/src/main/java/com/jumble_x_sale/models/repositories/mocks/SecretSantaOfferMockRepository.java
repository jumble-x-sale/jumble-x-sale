package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.SecretSantaOffer;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SecretSantaOfferRepository;

import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for secret santas.
 */
public final class SecretSantaOfferMockRepository extends MockRepository<JumbleUnitOfWork, SecretSantaOffer>
        implements SecretSantaOfferRepository {

    /**
     * {@inheritDoc}
     */
    public SecretSantaOfferMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, SecretSantaOffer.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SecretSantaOffer> getByCategory(String category, ResultOptions options) {
        Stream<SecretSantaOffer> stream = StreamSupport.stream(
                this.getByPredicate(s -> s.getInfo().getCategory().contentEquals(category)).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SecretSantaOffer> getByCategory(String category, PagingOptions options) {
        Stream<SecretSantaOffer> stream = StreamSupport.stream(
                this.getByPredicate(s -> s.getInfo().getCategory().contentEquals(category)).spliterator(), false);
        options.setRowCount(stream.count());
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return new PagingResult<>(options, stream);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SecretSantaOffer> getByCreator(UUID creatorId, ResultOptions options) {
        Stream<SecretSantaOffer> stream = StreamSupport.stream(
                this.getByPredicate(s -> s.getInfo().getCreator().getId().equals(creatorId)).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SecretSantaOffer> getByCreator(UUID creatorId, PagingOptions options) {
        Stream<SecretSantaOffer> stream = StreamSupport.stream(
                this.getByPredicate(s -> s.getInfo().getCreator().getId().equals(creatorId)).spliterator(), false);
        options.setRowCount(stream.count());
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return new PagingResult<>(options, stream);
    }

}
