package com.jumble_x_sale.models.repositories;

import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserMessage;
import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.CompletedSale;
import com.jumble_x_sale.models.CompletedSaleMessage;
import com.jumble_x_sale.models.SecretSantaOffer;
import com.jumble_x_sale.models.CompletedSecretSanta;

/**
 * A jumble unit of work that holds {@link Repository repositories} for each
 * concrete {@link Entity entity}.
 */
public interface JumbleUnitOfWork extends UnitOfWork {

    /**
     * Get the active {@link User user} for this {@link UnitOfWork unit of work}.
     * 
     * @return The active {@link User user} for this {@link UnitOfWork unit of
     *         work}.
     */
    public User getActiveUser();

    /**
     * Get the {@link Repository repository} for {@link User users}.
     * 
     * @return The {@link Repository repository} for {@link User users}.
     */
    public UserRepository getUserRepository();

    /**
     * Get the {@link Repository repository} for {@link UserMessage user messages}.
     * 
     * @return The {@link Repository repository} for {@link UserMessage user
     *         messages}.
     */
    public UserMessageRepository getUserMessageRepository();

    /**
     * Get the {@link Repository repository} for {@link SaleOffer saleOffers}.
     * 
     * @return The {@link Repository repository} for {@link SaleOffer saleOffers}.
     */
    public SaleOfferRepository getSaleOfferRepository();

    /**
     * Get the {@link Repository repository} for {@link SaleOfferMessage sale offer
     * messages}.
     * 
     * @return The {@link Repository repository} for {@link SaleOfferMessage sale
     *         offer messages}.
     */
    public SaleOfferMessageRepository getSaleOfferMessageRepository();

    /**
     * Get the {@link Repository repository} for {@link CompletedSale completed sale
     * offers}.
     * 
     * @return The {@link Repository repository} for {@link CompletedSale completed
     *         sale offers}.
     */
    public CompletedSaleRepository getCompletedSaleRepository();

    /**
     * Get the {@link Repository repository} for {@link CompletedSaleMessage
     * completed sale messages}.
     * 
     * @return The {@link Repository repository} for {@link CompletedSaleMessage
     *         completed sale messages}.
     */
    public CompletedSaleMessageRepository getCompletedSaleMessageRepository();

    /**
     * Get the {@link Repository repository} for {@link SecretSantaOffer secret
     * santa offers}.
     * 
     * @return The {@link Repository repository} for {@link SecretSantaOffer secret
     *         santa offers}.
     */
    public SecretSantaOfferRepository getSecretSantaOfferRepository();

    /**
     * Get the {@link Repository repository} for {@link CompletedSecretSanta
     * completed secret santas}.
     * 
     * @return The {@link Repository repository} for {@link CompletedSecretSanta
     *         completed secret santas}.
     */
    public CompletedSecretSantaRepository getCompletedSecretSantaRepository();

    /**
     * Set the active {@link User user} for this {@link UnitOfWork unit of work}.
     * 
     * @param value The active {@link User user} for this {@link UnitOfWork unit of
     *              work}.
     */
    public void setActiveUser(User value);

}
