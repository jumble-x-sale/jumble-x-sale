package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.SecretSantaOffer;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SecretSantaOfferRepository;
import com.jumble_x_sale.util.Utils;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for secret santas.
 */
public final class SecretSantaOfferPersistentRepository
        extends PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, SecretSantaOffer>
        implements SecretSantaOfferRepository {

    // region Inner Classes

    private static final class SecretSantaOfferByCategoryQuery
            extends QueryBase<SecretSantaOffer, UUID> {
        private ParameterExpression<String> category = this.b.parameter(String.class);

        public Stream<SecretSantaOffer> find(String category, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.category, category));
            return find(options);
        }

        public PagingResult<SecretSantaOffer> find(String category, PagingOptions options) {
            prepare(tq -> tq.setParameter(this.category, category));
            return find(options);
        }

        public SecretSantaOfferByCategoryQuery(EntityAccess<SecretSantaOffer, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CATEGORY), category));
        }
    }

    private static final class SecretSantaOfferByCreatorQuery
            extends QueryBase<SecretSantaOffer, UUID> {
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public Stream<SecretSantaOffer> find(UUID creatorId, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId));
            return find(options);
        }

        public PagingResult<SecretSantaOffer> find(UUID creatorId, PagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId));
            return find(options);
        }

        public SecretSantaOfferByCreatorQuery(EntityAccess<SecretSantaOffer, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String INFO = "info";
    protected static final String CREATOR = "creator";
    protected static final String CATEGORY = "category";

    protected SecretSantaOfferByCategoryQuery secretSantaOfferByCategoryQuery = null;
    protected SecretSantaOfferByCreatorQuery secretSantaOfferByCreatorQuery = null;

    // endregion Fields

    // region Constructors

    public SecretSantaOfferPersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, SecretSantaOffer.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SecretSantaOffer> getByCategory(String category, ResultOptions options) {
        if (Utils.isNullOrWhiteSpace(category)) {
            return this.emptyResult;
        }
        if (this.secretSantaOfferByCategoryQuery != null) {
            return this.secretSantaOfferByCategoryQuery.find(category, options);
        }
        return (this.secretSantaOfferByCategoryQuery =
                new SecretSantaOfferByCategoryQuery(this.entityAccess)).find(category, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SecretSantaOffer> getByCategory(String category, PagingOptions options) {
        if (Utils.isNullOrWhiteSpace(category)) {
            return new PagingResult<>(options, this.emptyResult);
        }
        if (this.secretSantaOfferByCategoryQuery != null) {
            return this.secretSantaOfferByCategoryQuery.find(category, options);
        }
        return (this.secretSantaOfferByCategoryQuery =
                new SecretSantaOfferByCategoryQuery(this.entityAccess)).find(category, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SecretSantaOffer> getByCreator(UUID creatorId, ResultOptions options) {
        if (creatorId == null) {
            return this.emptyResult;
        }
        if (this.secretSantaOfferByCreatorQuery != null) {
            return this.secretSantaOfferByCreatorQuery.find(creatorId, options);
        }
        return (this.secretSantaOfferByCreatorQuery =
                new SecretSantaOfferByCreatorQuery(this.entityAccess)).find(creatorId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SecretSantaOffer> getByCreator(UUID creatorId, PagingOptions options) {
        if (creatorId == null) {
            return new PagingResult<>(options, this.emptyResult);
        }
        if (this.secretSantaOfferByCreatorQuery != null) {
            return this.secretSantaOfferByCreatorQuery.find(creatorId, options);
        }
        return (this.secretSantaOfferByCreatorQuery =
                new SecretSantaOfferByCreatorQuery(this.entityAccess)).find(creatorId, options);
    }

    // endregion Methods

}
