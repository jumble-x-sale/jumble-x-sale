package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;
import com.jumble_x_sale.util.UnionAccess;
import com.jumble_x_sale.util.Utils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
// import java.io.Serializable;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.mindrot.jbcrypt.BCrypt;

/**
 * User contain user information that is shared by all {@link UserType user types}.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "UserId", columnDefinition = "binary(16)",
        nullable = false, updatable = false))
@Table(name = "Users")
public class User extends Entity {

    // region Fields

    private static final long serialVersionUID = 7673872970677924802L;

    @Column(name = "UserType", columnDefinition = "char(1)", nullable = false)
    private UserType userType;

    @Column(name = "CreationInstant", nullable = false)
    private Instant creationInstant;

    @Column(name = "LastLoginDateTime")
    private Instant lastLoginDateTime;

    @Column(name = "FirstName", nullable = false)
    private String firstName;

    @Column(name = "LastName", nullable = false)
    private String lastName;

    @Column(name = "Email", nullable = false)
    private String email;

    @Column(name = "PasswordHash", nullable = false)
    private String passwordHash;

    @Embedded
    private IdCard idCard;

    @Column(name = "Active", nullable = false)
    private boolean active;

    @OneToMany(mappedBy = "receiver", orphanRemoval = true, cascade = {CascadeType.ALL})
    private List<UserMessage> receivedMessages = new ArrayList<>();

    @OneToMany(mappedBy = "sender", orphanRemoval = true, cascade = {CascadeType.ALL})
    private List<UserMessage> sentMessages = new ArrayList<>();

    @OneToMany(mappedBy = "info.creator")
    private List<SaleOffer> saleOffers;

    @OneToMany(mappedBy = "info.creator")
    private List<CompletedSale> completedSaleOffers;

    @OneToMany(mappedBy = "info.creator")
    private List<SecretSantaOffer> secretSantaOffers;

    @OneToMany(mappedBy = "match1.creator")
    private List<CompletedSecretSanta> completedSecretSantaMatches1;

    @OneToMany(mappedBy = "match2.creator")
    private List<CompletedSecretSanta> completedSecretSantaMatches2;

    // endregion Fields

    // region Getters

    public UserType getUserType() {
        return this.userType;
    }

    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public Instant getLastLoginDateTime() {
        return this.lastLoginDateTime;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPasswordHash() {
        return this.passwordHash;
    }

    public IdCard getIdCard() {
        return this.idCard;
    }

    public boolean isActive() {
        return this.active;
    }

    public List<UserMessage> getReceivedMessages() {
        return this.receivedMessages;
    }

    public List<UserMessage> getSentMessages() {
        return this.sentMessages;
    }

    public List<SaleOffer> getSaleOffers() {
        return this.saleOffers;
    }

    public List<CompletedSale> getCompletedSales() {
        return this.completedSaleOffers;
    }

    public List<SecretSantaOffer> getSecretSanta() {
        return this.secretSantaOffers;
    }

    public List<CompletedSecretSanta> getCompletedSecretSantaMatches1() {
        return this.completedSecretSantaMatches1;
    }

    public List<CompletedSecretSanta> getCompletedSecretSantaMatches2() {
        return this.completedSecretSantaMatches2;
    }

    public Iterable<CompletedSecretSanta> getCompletedSecretSantas() {
        return new UnionAccess<CompletedSecretSanta>(this.completedSecretSantaMatches1,
                this.completedSecretSantaMatches2);
    }

    // endregion Getters

    // region Setters

    public void setUserType(UserType value) {
        this.userType = value;
    }

    public void setCreationInstant(Instant value) {
        this.creationInstant = value;
    }

    public void setLastLoginDateTime(Instant value) {
        this.lastLoginDateTime = value;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public void setPasswordHash(String value) {
        this.passwordHash = value;
    }

    public void setIdCard(IdCard value) {
        this.idCard = value;
    }

    public void setActive(boolean value) {
        this.active = value;
    }

    public void setReceivedMessages(List<UserMessage> value) {
        this.receivedMessages = value;
    }

    public void setSentMessages(List<UserMessage> value) {
        this.sentMessages = value;
    }

    public void setSaleOffers(List<SaleOffer> value) {
        this.saleOffers = value;
    }

    public void setCompletedSales(List<CompletedSale> value) {
        this.completedSaleOffers = value;
    }

    public void setSecretSanta(List<SecretSantaOffer> value) {
        this.secretSantaOffers = value;
    }

    public void getCompletedSecretSantaMatches1(List<CompletedSecretSanta> value) {
        this.completedSecretSantaMatches1 = value;
    }

    public void getCompletedSecretSantaMatches2(List<CompletedSecretSanta> value) {
        this.completedSecretSantaMatches2 = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public User() {
        this.userType = null;
        this.creationInstant = null;
        this.lastLoginDateTime = null;
        this.email = null;
        this.passwordHash = null;
        this.idCard = null;
        this.active = false;
        this.saleOffers = saleOffers != null ? saleOffers : new ArrayList<SaleOffer>();
        this.completedSaleOffers =
                completedSaleOffers != null ? completedSaleOffers : new ArrayList<CompletedSale>();
        this.secretSantaOffers =
                secretSantaOffers != null ? secretSantaOffers : new ArrayList<SecretSantaOffer>();
        this.completedSecretSantaMatches1 =
                completedSecretSantaMatches1 != null ? completedSecretSantaMatches1
                        : new ArrayList<CompletedSecretSanta>();
        this.completedSecretSantaMatches2 =
                completedSecretSantaMatches2 != null ? completedSecretSantaMatches2
                        : new ArrayList<CompletedSecretSanta>();
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public User(User other) {
        super(other.getId());
        this.userType = other.userType;
        this.creationInstant = other.creationInstant;
        this.lastLoginDateTime = other.lastLoginDateTime;
        this.email = other.email;
        this.passwordHash = other.passwordHash;
        this.idCard = other.idCard;
        this.active = other.active;
        this.receivedMessages = other.receivedMessages;
        this.sentMessages = other.sentMessages;
        this.saleOffers = other.saleOffers;
        this.completedSaleOffers = other.completedSaleOffers;
        this.secretSantaOffers = other.secretSantaOffers;
        this.completedSecretSantaMatches1 = other.completedSecretSantaMatches1;
        this.completedSecretSantaMatches2 = other.completedSecretSantaMatches2;
    }

    /**
     * Constructor.
     * 
     * @param userType          Defines the {@link UserType userType} of the {@link User user}.
     * @param creationInstant   The date and time the {@link User user} got created.
     * @param lastLoginDateTime The date and time the {@link User user} logged in for the last time.
     * @param firstName         The firstname of the {@link User user}.
     * @param lastName          The lastname of the {@link User user}.
     * @param email             The email of the {@link User user}.
     * @param password          The password of the {@link User user}.
     * @param idCard            An {@link IdCard idCard} of the {@link User user}, which contains
     *                          his identity number.
     * @param active            true if the {@link User user} is active, else false.
     */
    public User(UserType userType, Instant creationInstant, Instant lastLoginDateTime,
            String firstName, String lastName, String email, String password, IdCard idCard,
            boolean active) {
        this.userType = userType;
        this.creationInstant = creationInstant;
        this.lastLoginDateTime = lastLoginDateTime;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.passwordHash = getPasswordHash(password);
        this.idCard = idCard;
        this.active = active;
    }

    // endregion Constructors

    // region Methods

    public boolean passwordIsValid(String password) {
        return BCrypt.checkpw(password, this.passwordHash);
    }

    /**
     * Get the hash for a given password.
     * 
     * @param password The password for which to get the hash value.
     * @return The hash for a given password.
     */
    private static String getPasswordHash(String password) {
        final int salt = Utils.scale(password.hashCode(), 7, 15);
        final String hash = BCrypt.hashpw(password, BCrypt.gensalt(salt));
        return hash;
    }

    // endregion Methods

}
