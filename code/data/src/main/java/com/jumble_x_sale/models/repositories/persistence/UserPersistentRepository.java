package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.UserRepository;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent {@link Repository repository} for {@link User users}.
 */
public final class UserPersistentRepository
        extends PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, User> implements UserRepository {

    // region Inner Classes

    private static final class UserByEmailQuery extends QueryBase<User, UUID> {
        private ParameterExpression<String> email = this.b.parameter(String.class);

        public User findOne(String email) {
            prepare(tq -> tq.setParameter(this.email, email));
            // prepare();
            // tq.setParameter(this.email, email);
            return findOne();
        }

        public UserByEmailQuery(EntityAccess<User, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.equal(r.get(EMAIL), email));
        }
    }

    private static final class UserByNameQuery extends QueryBase<User, UUID> {
        private ParameterExpression<String> firstName = this.b.parameter(String.class);
        private ParameterExpression<String> lastName = this.b.parameter(String.class);

        public User findOne(String firstName, String lastName) {
            prepare(tq -> {
                tq.setParameter(this.firstName, firstName);
                tq.setParameter(this.lastName, lastName);
            });
            // prepare();
            // tq.setParameter(this.firstName, firstName);
            // tq.setParameter(this.lastName, lastName);
            return findOne();
        }

        public UserByNameQuery(EntityAccess<User, UUID> access) {
            super(access);
            setMainPredicateGetter(
                    (b, r) -> b.and(b.equal(r.get(FIRST_NAME), firstName), b.equal(r.get(LAST_NAME), lastName)));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String EMAIL = "email";
    protected static final String FIRST_NAME = "firstName";
    protected static final String LAST_NAME = "lastName";

    protected UserByEmailQuery userByEmailQuery = null;
    protected UserByNameQuery userByNameQuery = null;

    // endregion Fields

    // region Constructors

    /**
     * Create a mock {@link Repository repository} for {@link User users}.
     * 
     * @param unitOfWork    The {@link JumbleUnitOfWork jumble unit of work} to
     *                      which this {@link Repository repository} belongs.
     * @param entitySession The {@link EntitySession entity session} this repository
     *                      will use.
     */
    public UserPersistentRepository(JumblePersistentUnitOfWork unitOfWork, EntitySession entitySession) {
        super(unitOfWork, entitySession, User.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public User getByEmail(String email) {
        if (this.userByEmailQuery != null) {
            return this.userByEmailQuery.findOne(email);
        }
        return (this.userByEmailQuery = new UserByEmailQuery(this.entityAccess)).findOne(email);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User getByName(String firstName, String lastName) {
        if (this.userByNameQuery != null) {
            return this.userByNameQuery.findOne(firstName, lastName);
        }
        return (this.userByNameQuery = new UserByNameQuery(this.entityAccess)).findOne(firstName, lastName);
    }

    // endregion Methods

}
