package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.CompletedSaleMessage;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSaleMessageRepository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for completed sale messages.
 */
public final class CompletedSaleMessagePersistentRepository extends
        PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, CompletedSaleMessage>
        implements CompletedSaleMessageRepository {

    // region Inner Classes

    private static final class CompletedSaleMessageBySaleQuery
            extends QueryBase<CompletedSaleMessage, UUID> {
        private ParameterExpression<UUID> completedSaleId = this.b.parameter(UUID.class);

        public Stream<CompletedSaleMessage> find(UUID completedSaleId, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.completedSaleId, completedSaleId));
            return find(options);
        }

        public PagingResult<CompletedSaleMessage> find(UUID completedSaleId,
                PagingOptions options) {
            prepare(tq -> tq.setParameter(this.completedSaleId, completedSaleId));
            return find(options);
        }

        public CompletedSaleMessageBySaleQuery(EntityAccess<CompletedSaleMessage, UUID> access) {
            super(access);
            setMainPredicateGetter(
                    (b, r) -> b.equal(r.get(COMPLETED_SALE).get(ID), completedSaleId));
        }
    }

    private static final class CompletedSaleMessageByTextQuery
            extends QueryBase<CompletedSaleMessage, UUID> {
        private ParameterExpression<UUID> completedSaleId = this.b.parameter(UUID.class);
        private ParameterExpression<String> text = this.b.parameter(String.class);

        public Stream<CompletedSaleMessage> find(UUID completedSaleId, String containedText,
                ResultOptions options) {
            prepare(tq -> {
                tq.setParameter(this.completedSaleId, completedSaleId);
                tq.setParameter(text, "%" + containedText + "%");
            });
            return find(options);
        }

        public PagingResult<CompletedSaleMessage> find(UUID completedSaleId, String containedText,
                PagingOptions options) {
            prepare(tq -> {
                tq.setParameter(this.completedSaleId, completedSaleId);
                tq.setParameter(text, "%" + containedText + "%");
            });
            return find(options);
        }

        public CompletedSaleMessageByTextQuery(EntityAccess<CompletedSaleMessage, UUID> access) {
            super(access);
            setMainPredicateGetter(
                    (b, r) -> b.and(b.equal(r.get(COMPLETED_SALE).get(ID), completedSaleId),
                            b.like(r.get(MESSAGE), text)));
        }
    }

    private static final class CompletedSaleMessageBySaleCreatorQuery
            extends QueryBase<CompletedSaleMessage, UUID> {
        private CompletedSaleMessagePersistentRepository repo;
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public PagingResult<CompletedSaleMessage> find(UUID creatorId,
                FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public CompletedSaleMessageBySaleCreatorQuery(CompletedSaleMessagePersistentRepository repo,
                EntityAccess<CompletedSaleMessage, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b
                    .equal(r.get(COMPLETED_SALE).get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String COMPLETED_SALE = "completedSale";
    protected static final String INFO = "info";
    protected static final String CREATOR = "creator";
    protected static final String MESSAGE = "message";

    protected CompletedSaleMessageBySaleCreatorQuery completedSaleMessageBySaleCreatorQuery = null;
    protected CompletedSaleMessageBySaleQuery completedSaleMessageBySaleQuery = null;
    protected CompletedSaleMessageByTextQuery completedSaleMessageByTextQuery = null;

    // endregion Fields

    // region Constructors

    public CompletedSaleMessagePersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, CompletedSaleMessage.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(CompletedSaleMessage entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSaleMessage> getBySaleCreator(UUID saleCreatorId,
            FilterPagingOptions filterPagingOptions) {
        if (this.completedSaleMessageBySaleCreatorQuery != null) {
            return this.completedSaleMessageBySaleCreatorQuery.find(saleCreatorId,
                    filterPagingOptions);
        }
        return (this.completedSaleMessageBySaleCreatorQuery =
                new CompletedSaleMessageBySaleCreatorQuery(this, this.entityAccess))
                        .find(saleCreatorId, filterPagingOptions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSaleMessage> getBySale(UUID completedSaleId, ResultOptions options) {
        if (this.completedSaleMessageBySaleQuery != null) {
            return this.completedSaleMessageBySaleQuery.find(completedSaleId, options);
        }
        return (this.completedSaleMessageBySaleQuery =
                new CompletedSaleMessageBySaleQuery(this.entityAccess)).find(completedSaleId,
                        options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSaleMessage> getBySale(UUID completedSaleId,
            PagingOptions options) {
        if (this.completedSaleMessageBySaleQuery != null) {
            return this.completedSaleMessageBySaleQuery.find(completedSaleId, options);
        }
        return (this.completedSaleMessageBySaleQuery =
                new CompletedSaleMessageBySaleQuery(this.entityAccess)).find(completedSaleId,
                        options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText,
            ResultOptions options) {
        if (this.completedSaleMessageByTextQuery != null) {
            return this.completedSaleMessageByTextQuery.find(completedSaleId, containedText,
                    options);
        }
        return (this.completedSaleMessageByTextQuery =
                new CompletedSaleMessageByTextQuery(this.entityAccess)).find(completedSaleId,
                        containedText, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText,
            PagingOptions options) {
        if (this.completedSaleMessageByTextQuery != null) {
            return this.completedSaleMessageByTextQuery.find(completedSaleId, containedText,
                    options);
        }
        return (this.completedSaleMessageByTextQuery =
                new CompletedSaleMessageByTextQuery(this.entityAccess)).find(completedSaleId,
                        containedText, options);
    }

    // endregion Methods

}
