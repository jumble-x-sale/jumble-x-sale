package com.jumble_x_sale.models.repositories;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.models.IdCard;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserType;

import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * A repository for users.
 */
public interface UserRepository extends Repository<JumbleUnitOfWork, User> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] { "id" });
            map.put("firstName", new String[] { "firstName" });
            map.put("lastName", new String[] { "lastName" });
            map.put("password", new String[] { "password" });
            map.put("email", new String[] { "email" });
            map.put("idCard", new String[] { "idCard", "idString" });
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a new {@link User user}.
     * 
     * @param userType  Defines the {@link UserType userType} of the {@link User
     *                  user}.
     * @param firstName The firstname of the {@link User user}.
     * @param lastName  The lastname of the {@link User user}.
     * @param email     The email of the {@link User user}.
     * @param password  The password of the {@link User user}.
     * @param idCard    An {@link IdCard idCard} of the {@link User user}, which
     *                  contains his identity number.
     * @param active    true if the {@link User user} is active, else false.
     * @return The created {@link User user}.
     */
    default User create(UserType userType, String firstName, String lastName, String email, String password,
            IdCard idCard, boolean active) {
        Instant creationInstant = Instant.now();
        User user = new User(userType, creationInstant, creationInstant, firstName, lastName, email, password, idCard,
                active);
        this.getUnitOfWork().getUserRepository().add(user);
        return user;
    }

    /**
     * Try to login the user with the given email and password.
     * 
     * @param email    The email of the user to login.
     * @param password The password of the user to login.
     * @return Could the user be logged in?
     */
    default boolean login(String email, String password) {
        User user = this.getByEmail(email);
        if (user != null && user.passwordIsValid(password)) {
            this.getUnitOfWork().setActiveUser(user);
            return true;
        }
        return false;
    }

    /**
     * Get a user by his e-mail.
     * 
     * @param email The e-mail that the wanted user has.
     * @return The user that has the given e-mail.
     */
    public User getByEmail(String email);

    /**
     * Get a {@link User user} by its name.
     * 
     * @param firstName The first name of the wanted {@link User user}.
     * @param lastName  The last name of the wanted {@link User user}.
     * @return A {@link User user} by its name.
     */
    public User getByName(String firstName, String lastName);

    // endregion Methods

}
