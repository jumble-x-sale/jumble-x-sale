package com.jumble_x_sale.models;

import com.jumble_x_sale.util.TimeUtils;

import java.time.LocalDate;
import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Column;

/**
 * IdCard represents an identity card number. This number includes information about the the owners
 * birthday, the date of expiry and checksums to check if it's a valid card number
 */
@Embeddable
public class IdCard implements Serializable {

    // region Fields

    private static final long serialVersionUID = 1186175025738823198L;

    @Column(name = "IdString")
    private String idString;

    // endregion Field

    // region Getters

    /**
     * Gets the String value of the IdCard.
     * 
     * @return The String value of the IdCard.
     */
    public String getIdString() {
        return this.idString;
    }

    /**
     * Gets the {@link LocalDate birthday} from the IdCard.
     * 
     * @return the {@link LocalDate birthday} from the IdCard.
     */
    public LocalDate getBirthday() {
        int yearOfBirth = Integer.parseInt(this.idString.substring(10, 12));
        int monthOfBirth = Integer.parseInt(this.idString.substring(12, 14));
        int dayOfBirth = Integer.parseInt(this.idString.substring(14, 16));
        int yearOfExpiry = Integer.parseInt(this.idString.substring(17, 19));
        // checks if the person is born in this or the last century
        if (yearOfBirth >= yearOfExpiry) {
            yearOfBirth += 1900;
        } else {
            yearOfBirth += 2000;
        }
        return LocalDate.of(yearOfBirth, monthOfBirth, dayOfBirth);
    }

    // endregion Getters

    // region Setters

    /**
     * Sets the String value of the IdCard.
     * 
     * @param value The String value of the IdCard.
     */
    public void setIdString(String value) {
        this.idString = value;
    }

    // endregion Setters

    // region Constructors

    public IdCard() {
        this.idString = null;
    }

    public IdCard(String id) {
        this.idString = id;
    }

    // endregion Constructors

    // region Methods

    /**
     * Checks if the IdCard is valid (only for german IdCards).
     * 
     * @return true if it is a valid IdCard
     */
    public boolean isValid() {
        if (this.idString.length() != 24) {
            return false;
        }
        // defines the first 3 checknumbers of the german IdCard
        String idToUpperCase = this.idString.toUpperCase();
        char[] idAsArray = idToUpperCase.toCharArray();
        int checkDigit1 = Character.digit(idAsArray[9], 10); // 1. Prüfziffer
        int checkDigit2 = Character.digit(idAsArray[16], 10); // 2. Prüfziffer
        int checkDigit3 = Character.digit(idAsArray[23], 10); // 3. Prüfziffer
        // defines 3 sums where the int values of the idString characters gets added to
        int sum1 = 0;
        int sum2 = 0;
        int sum3 = 0;
        // multiplies the int values of the character depending on their position
        for (int i = 0; i < 9; i++) {
            if (i % 3 == 0) {
                sum1 += 7 * calculateCheckSum(idAsArray[i]);

            } else if (i % 3 == 1) {
                sum1 += 3 * calculateCheckSum(idAsArray[i]);

            } else {
                sum1 += calculateCheckSum(idAsArray[i]);
            }
        }
        // checks if last digit of the first checksum equals the first checknumber
        if (checkDigit1 != (sum1 % 10)) {
            return false;
        }
        // same procedure for the second checksum
        for (int i = 10; i < 16; i++) {

            if (i % 3 == 1) {
                sum2 += 7 * calculateCheckSum(idAsArray[i]);
            } else if (i % 3 == 2) {
                sum2 += 3 * calculateCheckSum(idAsArray[i]);
            } else {
                sum2 += calculateCheckSum(idAsArray[i]);
            }
        }
        if (checkDigit2 != (sum2 % 10)) {
            return false;
        }
        // same procedure for the third checksum
        for (int i = 17; i < 23; i++) {

            if (i % 3 == 2) {
                sum3 += 7 * calculateCheckSum(idAsArray[i]);
            } else if (i % 3 == 0) {
                sum3 += 3 * calculateCheckSum(idAsArray[i]);
            } else {
                sum3 += calculateCheckSum(idAsArray[i]);
            }
        }
        if (checkDigit3 != (sum3 % 10)) {
            return false;
        } else {
            // if all the checks doesn't fail, true will be returned
            return true;
        }
    }

    /**
     * Parses character of the IdCard into integer values.
     * 
     * @return int value of the character
     */
    private static int calculateCheckSum(char c) {
        if (c >= '0' && c <= '9') {
            return Character.digit(c, 10);
        }
        if (c < 'C' || c > 'Z') {
            return -1;
        }
        switch (c) {
            case 'D':
            case 'E':
            case 'I':
            case 'O':
            case 'Q':
            case 'S':
            case 'U':
                return -1;
            default:
                break;
        }
        return ((int) c) - 55;
    }

    /**
     * Checks if the IdCard is expired.
     * 
     * @return true if the IdCard is expired.
     */
    public boolean isExpired() {
        int yearOfExpiry = Integer.parseInt(this.idString.substring(17, 19)) + 2000;
        int monthOfExpiry = Integer.parseInt(this.idString.substring(19, 21));
        int dayOfExpiry = Integer.parseInt(this.idString.substring(21, 23));
        LocalDate dateOfExpiry = LocalDate.of(yearOfExpiry, monthOfExpiry, dayOfExpiry);
        return dateOfExpiry.isBefore(TimeUtils.todayGerman());
    }

    // endregion Methods

}
