package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.UserRepository;
import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock {@link Repository repository} for {@link User users}.
 */
public final class UserMockRepository extends MockRepository<JumbleUnitOfWork, User> implements UserRepository {

    /**
     * Create a mock {@link Repository repository} for {@link User users}.
     * 
     * @param unitOfWork The {@link JumbleUnitOfWork jumble unit of work} to which
     *                   this {@link Repository repository} belongs.
     */
    public UserMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, User.class);
    }

    /**
     * Get a {@link User user} by his e-mail.
     * 
     * @param email The e-mail that the wanted {@link User user} has.
     * @return The {@link User user} that has the given e-mail.
     */
    @Override
    public User getByEmail(String email) {
        for (User user : this.entities.values()) {
            if (user.getEmail() == email) {
                return user;
            }
        }
        return null;
    }

    /**
     * Get a {@link User user} by its name.
     * 
     * @param firstName The first name of the wanted {@link User user}.
     * @param lastName  The last name of the wanted {@link User user}.
     * @return A {@link User user} by its name.
     */
    @Override
    public User getByName(String firstName, String lastName) {
        for (User user : this.entities.values()) {
            if (user.getFirstName() == firstName && user.getLastName() == lastName) {
                return user;
            }
        }
        return null;
    }

}
