package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.JumbleWorkContext;
import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.data.mocks.MockWorkContext;

/**
 * A context for working on a subject. Used for managing {@link UnitOfWork units
 * of work}.
 */
public class JumbleMockWorkContext extends MockWorkContext<JumbleUnitOfWork> implements JumbleWorkContext {

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public JumbleUnitOfWork createUnitOfWork() {
        return new JumbleMockUnitOfWork(this);
    }

    // region Methods

}
