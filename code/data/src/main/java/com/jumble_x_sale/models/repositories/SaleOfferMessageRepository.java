package com.jumble_x_sale.models.repositories;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferMessage;

/**
 * A repository for sale offer messages.
 */
public interface SaleOfferMessageRepository extends Repository<JumbleUnitOfWork, SaleOfferMessage> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a new {@link SaleOfferMessage saleOfferMessage}.
     * 
     * @param saleOffer   The {@link SaleOffer saleOffer} where the message will be assigned to.
     * @param sender      The {@link User sender} who sends the message.
     * @param messageText The text of the message.
     * @return A {@link SaleOfferMessage message}.
     */
    default SaleOfferMessage create(SaleOffer saleOffer, User sender, String messageText) {
        SaleOfferMessage message = new SaleOfferMessage(saleOffer, sender, messageText);
        this.add(message);
        saleOffer.getMessages().add(message);
        return message;
    }

    /**
     * Creates a new {@link SaleOfferMessage saleOfferMessage}.
     * 
     * @param saleOfferId The id of the {@link SaleOffer saleOffer} to which this message will be
     *                    assigned to.
     * @param senderId    The if of the {@link User user} who sends the message.
     * @param messageText The text of the message.
     * @return A {@link SaleOfferMessage message}.
     */
    default SaleOfferMessage create(UUID saleOfferId, UUID senderId, String messageText) {
        SaleOffer saleOffer = this.getUnitOfWork().getSaleOfferRepository().getById(saleOfferId);
        User sender = this.getUnitOfWork().getUserRepository().getById(senderId);
        return this.create(saleOffer, sender, messageText);
    }

    /**
     * Lists {@link SaleOfferMessage sale offer messages} belonging to {@link SaleOffer sale offers}
     * of a particular {@link User user} where the filters match.
     * 
     * @param saleCreatorId       The id of the {@link SaleOffer sale offer} creator to look at.
     * @param filterPagingOptions The filter paging options.
     * @return A page of {@link SaleOfferMessage sale offer messages}.
     */
    public PagingResult<SaleOfferMessage> getBySaleCreator(UUID saleCreatorId,
            FilterPagingOptions filterPagingOptions);

    /**
     * Lists {@link SaleOfferMessage saleOfferMessages} assigned to a particular {@link SaleOffer
     * saleOffer} where the text of the message contains the search text.
     * 
     * @param saleOfferId The id of the {@link SaleOffer saleOffer} to look at.
     * @return A stream of {@link SaleOfferMessage saleOfferMessages}.
     */
    default Stream<SaleOfferMessage> getBySale(UUID saleOfferId) {
        return this.getBySale(saleOfferId, ResultOptions.allRows());
    }

    /**
     * Lists {@link SaleOfferMessage saleOfferMessages} assigned to a particular {@link SaleOffer
     * saleOffer} where the text of the message contains the search text.
     * 
     * @param saleOfferId   The id of the {@link SaleOffer saleOffer} to look at.
     * @param resultOptions The result options.
     * @return A stream of {@link SaleOfferMessage saleOfferMessages}.
     */
    public Stream<SaleOfferMessage> getBySale(UUID saleOfferId, ResultOptions resultOptions);

    /**
     * Lists {@link SaleOfferMessage saleOfferMessages} assigned to a particular {@link SaleOffer
     * saleOffer} where the text of the message contains the search text.
     * 
     * @param saleOfferId   The id of the {@link SaleOffer saleOffer} to look at.
     * @param resultOptions The result options.
     * @return A stream of {@link SaleOfferMessage saleOfferMessages}.
     */
    public PagingResult<SaleOfferMessage> getBySale(UUID saleOfferId,
            FilterPagingOptions resultOptions);

    /**
     * Lists {@link SaleOfferMessage saleOfferMessages} assigned to a particular {@link SaleOffer
     * saleOffer} where the text of the message contains the search text.
     * 
     * @param saleOfferId The id of the {@link SaleOffer saleOffer} to look at.
     * @param text        The search text.
     * @return A stream of {@link SaleOfferMessage saleOfferMessages}.
     */
    default Stream<SaleOfferMessage> getByText(UUID saleOfferId, String text) {
        return this.getByText(saleOfferId, text, ResultOptions.allRows());
    }

    /**
     * Lists {@link SaleOfferMessage saleOfferMessages} assigned to a particular {@link SaleOffer
     * saleOffer} where the text of the message contains the search text.
     * 
     * @param saleOfferId   The id of the {@link SaleOffer saleOffer} to look at.
     * @param text          The search text.
     * @param resultOptions The result options.
     * @return A stream of {@link SaleOfferMessage saleOfferMessages}.
     */
    public Stream<SaleOfferMessage> getByText(UUID saleOfferId, String text,
            ResultOptions resultOptions);

    // endregion Methods

}
