package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * CompletedSale contains information about a SaleOffer and its buyer.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "CompletedSaleId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "CompletedSales")
public class CompletedSale extends Entity {

    // region Fields

    private static final long serialVersionUID = -1267492032261313121L;

    @Embedded
    @AssociationOverrides({
            @AssociationOverride(name = "creator", joinColumns = @JoinColumn(name = "CreatorId"))})
    private PricedItemInfo info;

    @ManyToOne(optional = false)
    @JoinColumn(name = "BuyerId", nullable = false)
    private User buyer;

    @Column(name = "CompletionInstant", nullable = false)
    private Instant completionInstant;

    @OneToMany(mappedBy = "completedSale", orphanRemoval = true, cascade = {CascadeType.ALL})
    private List<CompletedSaleMessage> messages = new ArrayList<>();

    // endregion Fields

    // region Getters

    public PricedItemInfo getInfo() {
        return this.info;
    }

    public User getBuyer() {
        return this.buyer;
    }

    public Instant getCompletionInstant() {
        return this.completionInstant;
    }

    public List<CompletedSaleMessage> getMessages() {
        return this.messages;
    }

    // endregion Getters

    // region Setters

    public void setPricedItemInfo(PricedItemInfo value) {
        this.info = value;
    }

    public void setBuyer(User value) {
        this.buyer = value;
    }

    public void setCompletionInstant(Instant value) {
        this.completionInstant = value;
    }

    public void setMessages(List<CompletedSaleMessage> value) {
        this.messages = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public CompletedSale() {
        this.info = null;
        this.buyer = null;
        this.completionInstant = Instant.now();
    }

    /**
     * Constructor.
     * 
     * @param info  The sale info.
     * @param buyer The member who acts as a buyer.
     */
    public CompletedSale(PricedItemInfo info, User buyer) {
        this.info = info;
        this.buyer = buyer;
        this.completionInstant = Instant.now();
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public CompletedSale(CompletedSale other) {
        this.info = other.info;
        this.buyer = other.buyer;
        this.completionInstant = other.completionInstant;
    }

    // endregion Constructors

}
