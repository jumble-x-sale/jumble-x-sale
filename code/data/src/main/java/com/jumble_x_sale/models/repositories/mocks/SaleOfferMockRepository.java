package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SaleOfferRepository;

import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for sale offers.
 */
public final class SaleOfferMockRepository extends MockRepository<JumbleUnitOfWork, SaleOffer>
        implements SaleOfferRepository {

    public SaleOfferMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, SaleOffer.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(SaleOffer entity) {
        return super.remove(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOffer> getByCategory(String category, ResultOptions options) {
        Stream<SaleOffer> stream = StreamSupport
                .stream(this.getByPredicate(s -> s.getInfo().getCategory().contentEquals(category))
                        .spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOffer> getByCategory(String category, PagingOptions options) {
        Stream<SaleOffer> stream = StreamSupport
                .stream(this.getByPredicate(s -> s.getInfo().getCategory().contentEquals(category))
                        .spliterator(), false);
        options.setRowCount(stream.count());
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return new PagingResult<>(options, stream);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOffer> getByCreator(UUID creatorId, ResultOptions options) {
        Stream<SaleOffer> stream = StreamSupport
                .stream(this.getByPredicate(t -> t.getInfo().getCreator().getId().equals(creatorId))
                        .spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOffer> getByCreator(UUID creatorId, FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> m.getInfo().getCreator().getId().equals(creatorId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOffer> getExceptCreator(UUID creatorId, FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> !m.getInfo().getCreator().getId().equals(creatorId));
    }

}
