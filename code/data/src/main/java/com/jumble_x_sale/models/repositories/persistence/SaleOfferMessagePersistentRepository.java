package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SaleOfferMessageRepository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for sale offer messages.
 */
public final class SaleOfferMessagePersistentRepository
        extends PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, SaleOfferMessage>
        implements SaleOfferMessageRepository {

    // region Inner Classes

    private static final class SaleOfferMessageByTextQuery
            extends QueryBase<SaleOfferMessage, UUID> {
        private ParameterExpression<UUID> saleOfferId = this.b.parameter(UUID.class);
        private ParameterExpression<String> text = this.b.parameter(String.class);

        public Stream<SaleOfferMessage> find(UUID saleOfferId, String containedText,
                ResultOptions resultOptions) {
            prepare(tq -> {
                tq.setParameter(this.saleOfferId, saleOfferId);
                tq.setParameter(text, "%" + containedText + "%");
            });
            return find(resultOptions);
        }

        public SaleOfferMessageByTextQuery(EntityAccess<SaleOfferMessage, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.and(b.equal(r.get(SALE_OFFER).get(ID), saleOfferId),
                    b.like(r.get(MESSAGE), text)));
        }
    }

    private static final class SaleOfferMessageBySaleQuery
            extends QueryBase<SaleOfferMessage, UUID> {
        private SaleOfferMessagePersistentRepository repo;
        private ParameterExpression<UUID> saleOfferId = this.b.parameter(UUID.class);

        public Stream<SaleOfferMessage> find(UUID saleOfferId, ResultOptions resultOptions) {
            prepare(tq -> tq.setParameter(this.saleOfferId, saleOfferId));
            return find(resultOptions);
        }

        public PagingResult<SaleOfferMessage> find(UUID saleOfferId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.saleOfferId, saleOfferId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public SaleOfferMessageBySaleQuery(SaleOfferMessagePersistentRepository repo,
                EntityAccess<SaleOfferMessage, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.equal(r.get(SALE_OFFER).get(ID), saleOfferId));
        }
    }

    private static final class SaleOfferMessageBySaleCreatorQuery
            extends QueryBase<SaleOfferMessage, UUID> {
        private SaleOfferMessagePersistentRepository repo;
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public PagingResult<SaleOfferMessage> find(UUID creatorId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public SaleOfferMessageBySaleCreatorQuery(SaleOfferMessagePersistentRepository repo,
                EntityAccess<SaleOfferMessage, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter(
                    (b, r) -> b.equal(r.get(SALE_OFFER).get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String SALE_OFFER = "saleOffer";
    protected static final String INFO = "info";
    protected static final String CREATOR = "creator";
    protected static final String MESSAGE = "message";

    protected SaleOfferMessageBySaleCreatorQuery saleOfferMessageBySaleCreatorQuery = null;
    protected SaleOfferMessageBySaleQuery saleOfferMessageBySaleQuery = null;
    protected SaleOfferMessageByTextQuery saleOfferMessageByTextQuery = null;

    // endregion Fields

    // region Constructors

    public SaleOfferMessagePersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, SaleOfferMessage.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOfferMessage> getBySaleCreator(UUID saleCreatorId,
            FilterPagingOptions filterPagingOptions) {
        if (this.saleOfferMessageBySaleCreatorQuery != null) {
            return this.saleOfferMessageBySaleCreatorQuery.find(saleCreatorId, filterPagingOptions);
        }
        return (this.saleOfferMessageBySaleCreatorQuery =
                new SaleOfferMessageBySaleCreatorQuery(this, this.entityAccess)).find(saleCreatorId,
                        filterPagingOptions);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOfferMessage> getBySale(UUID saleOfferId, ResultOptions options) {
        if (this.saleOfferMessageBySaleQuery != null) {
            return this.saleOfferMessageBySaleQuery.find(saleOfferId, options);
        }
        return (this.saleOfferMessageBySaleQuery =
                new SaleOfferMessageBySaleQuery(this, this.entityAccess)).find(saleOfferId,
                        options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOfferMessage> getBySale(UUID saleOfferId, FilterPagingOptions options) {
        if (this.saleOfferMessageBySaleQuery != null) {
            return this.saleOfferMessageBySaleQuery.find(saleOfferId, options);
        }
        return (this.saleOfferMessageBySaleQuery =
                new SaleOfferMessageBySaleQuery(this, this.entityAccess)).find(saleOfferId,
                        options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOfferMessage> getByText(UUID saleOfferId, String containedText,
            ResultOptions resultOptions) {
        if (this.saleOfferMessageByTextQuery != null) {
            return this.saleOfferMessageByTextQuery.find(saleOfferId, containedText, resultOptions);
        }
        return (this.saleOfferMessageByTextQuery =
                new SaleOfferMessageByTextQuery(this.entityAccess)).find(saleOfferId, containedText,
                        resultOptions);
    }

    // endregion Methods

}
