package com.jumble_x_sale.models;

import java.math.BigDecimal;
import java.time.Instant;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Contains all the information about an item and its price.
 */
@Embeddable
@Access(AccessType.FIELD)
public class PricedItemInfo extends ItemInfo {

    // region Fields

    private static final long serialVersionUID = 7464215047458138394L;

    @Column(name = "Price", nullable = false)
    private BigDecimal price;

    // endregion Fields

    // region Getters

    public BigDecimal getPrice() {
        return this.price;
    }

    // endregion Getters

    // region Setters

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor
     */
    public PricedItemInfo() {
        super();
        this.price = null;
    }

    /**
     * Constructor
     * 
     * @param creator         The {@link User creator} of the sale offer.
     * @param creationInstant The the point in time when the {@link ItemInfo item} was created
     *                        (should be "now").
     * @param category        The sale offers category.
     * @param title           The sale offers title.
     * @param description     A description about the sale offer.
     * @param imageUrls       An String array which contains the paths to the sale offer images.
     * @param mainImageData   A byte array which contains the main image data.
     * @param price           The price of the sale offer.
     */
    public PricedItemInfo(User creator, Instant creationInstant, String category, String title,
            String description, String[] imageUrls, byte[] mainImageData, BigDecimal price) {
        super(creator, creationInstant, category, title, description, imageUrls, mainImageData);
        this.price = price;
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public PricedItemInfo(PricedItemInfo other) {
        super(other);
        this.price = other.price;
    }

    // endregion Constructors

}
