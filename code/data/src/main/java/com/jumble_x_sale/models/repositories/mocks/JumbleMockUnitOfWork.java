package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.mocks.MockUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSaleMessageRepository;
import com.jumble_x_sale.models.repositories.CompletedSaleRepository;
import com.jumble_x_sale.models.repositories.CompletedSecretSantaRepository;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SaleOfferMessageRepository;
import com.jumble_x_sale.models.repositories.SaleOfferRepository;
import com.jumble_x_sale.models.repositories.SecretSantaOfferRepository;
import com.jumble_x_sale.models.repositories.UserMessageRepository;
import com.jumble_x_sale.models.repositories.UserRepository;
// import com.jumble_x_sale.models.repositories.mocks.CompletedSaleMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.CompletedSaleMessageMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.CompletedSecretSantaMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.SaleOfferMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.SaleOfferMessageMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.SecretSantaOfferMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.UserMessageMockRepository;
// import com.jumble_x_sale.models.repositories.mocks.UserMockRepository;
import com.jumble_x_sale.models.User;

import java.util.logging.Logger;

/**
 * A mock jumble unit of work that holds entity {@link Repository repositories}.
 */
public final class JumbleMockUnitOfWork extends MockUnitOfWork<JumbleUnitOfWork>
        implements JumbleUnitOfWork {

    // region Fields

    private static final Logger LOGGER =
            Logger.getLogger(JumbleMockUnitOfWork.class.getSimpleName());

    private User activeUser = null;
    private UserRepository userRepository = null;
    private UserMessageRepository userMessageRepository = null;
    private SaleOfferRepository saleOfferRepository = null;
    private SaleOfferMessageRepository saleOfferMessageRepository = null;
    private CompletedSaleRepository completedSaleRepository = null;
    private CompletedSaleMessageRepository completedSaleMessageRepository = null;
    private SecretSantaOfferRepository secretSantaOfferRepository = null;
    private CompletedSecretSantaRepository completedSecretSantaRepository = null;

    // endregion

    // region Constructors

    public JumbleMockUnitOfWork(JumbleMockWorkContext workContext) {
        super(workContext);
        LOGGER.info("\"JumbleMockUnitOfWork\" created.");
    }

    // endregion

    // region Getters

    public User getActiveUser() {
        return this.activeUser;
    }

    public UserRepository getUserRepository() {
        if (this.userRepository != null) {
            return this.userRepository;
        }
        return (this.userRepository = this.addRepository(new UserMockRepository(this)));
    }

    public UserMessageRepository getUserMessageRepository() {
        if (this.userMessageRepository != null) {
            return this.userMessageRepository;
        }
        return (this.userMessageRepository =
                this.addRepository(new UserMessageMockRepository(this)));
    }

    public SaleOfferRepository getSaleOfferRepository() {
        if (this.saleOfferRepository != null) {
            return this.saleOfferRepository;
        }
        return (this.saleOfferRepository = this.addRepository(new SaleOfferMockRepository(this)));
    }

    public SaleOfferMessageRepository getSaleOfferMessageRepository() {
        if (this.saleOfferMessageRepository != null) {
            return this.saleOfferMessageRepository;
        }
        return (this.saleOfferMessageRepository =
                this.addRepository(new SaleOfferMessageMockRepository(this)));
    }

    public CompletedSaleRepository getCompletedSaleRepository() {
        if (this.completedSaleRepository != null) {
            return this.completedSaleRepository;
        }
        return (this.completedSaleRepository =
                this.addRepository(new CompletedSaleMockRepository(this)));
    }

    public CompletedSaleMessageRepository getCompletedSaleMessageRepository() {
        if (this.completedSaleMessageRepository != null) {
            return this.completedSaleMessageRepository;
        }
        return (this.completedSaleMessageRepository =
                this.addRepository(new CompletedSaleMessageMockRepository(this)));
    }

    public SecretSantaOfferRepository getSecretSantaOfferRepository() {
        if (this.secretSantaOfferRepository != null) {
            return this.secretSantaOfferRepository;
        }
        return (this.secretSantaOfferRepository =
                this.addRepository(new SecretSantaOfferMockRepository(this)));
    }

    public CompletedSecretSantaRepository getCompletedSecretSantaRepository() {
        if (this.completedSecretSantaRepository != null) {
            return this.completedSecretSantaRepository;
        }
        return (this.completedSecretSantaRepository =
                this.addRepository(new CompletedSecretSantaMockRepository(this)));
    }

    // endregion

    // region Setters

    public void setActiveUser(User value) {
        this.activeUser = value;
    }

    // endregion
}
