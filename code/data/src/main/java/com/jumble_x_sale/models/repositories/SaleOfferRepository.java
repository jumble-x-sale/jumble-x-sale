package com.jumble_x_sale.models.repositories;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.UserType;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.SaleOffer;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * A repository for sale offers.
 */
public interface SaleOfferRepository extends Repository<JumbleUnitOfWork, SaleOffer> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a {@link SaleOffer saleOffer}.
     * 
     * @param category      The category of the {@link SaleOffer saleOffer}.
     * @param title         The tilte of the {@link SaleOffer saleOffer}.
     * @param description   The description of the {@link SaleOffer saleOffer}.
     * @param imageUrls     An String array which contains the paths to the sale offer images.
     * @param mainImageData A byte array which contains the main image data.
     * @param price         The price of the sale offer.
     * @return A {@link SaleOffer saleOffer} or null if the creation failed.
     */
    default SaleOffer create(String category, String title, String description, String[] imageUrls,
            byte[] mainImageData, BigDecimal price) {
        JumbleUnitOfWork uow = this.getUnitOfWork();
        User creator = uow.getActiveUser();
        if (creator != null && creator.getUserType() == UserType.MEMBER) {
            if (creator != null) {
                SaleOffer saleOffer = new SaleOffer(creator, Instant.now(), category, title,
                        description, imageUrls, mainImageData, price);
                uow.getSaleOfferRepository().add(saleOffer);
                return saleOffer;
            }
        }
        return null;
    }

    /**
     * Lists all {@link SaleOffer saleOffers} belonging to a category.
     * 
     * @param category The category where the {@link SaleOffer sale offers} should belong to.
     * @param options  The result options.
     * @return A stream of {@link SaleOffer sale offers}.
     */
    public Stream<SaleOffer> getByCategory(String category, ResultOptions options);

    /**
     * Lists all {@link SaleOffer saleOffers} belonging to a category.
     * 
     * @param category The category where the {@link SaleOffer sale offers} should belong to.
     * @param options  The result options.
     * @return A stream of {@link SaleOffer sale offers}.
     */
    public PagingResult<SaleOffer> getByCategory(String category, PagingOptions options);

    /**
     * Lists all {@link SaleOffer saleOffers} belonging to a category.
     * 
     * @param category The category where the {@link SaleOffer sale offers} should belong to.
     * @return A stream of {@link SaleOffer sale offers}.
     */
    default Stream<SaleOffer> getByCategory(String category) {
        return this.getByCategory(category, ResultOptions.allRows());
    }

    /**
     * Gets a list of all {@link SaleOffer saleOffers} created by a {@link User user}.
     * 
     * @param creatorId The {@link User member} that created the {@link SaleOffer sale offers} to
     *                  get.
     * @return A stream of {@link SaleOffer saleOffers}.
     */
    default Stream<SaleOffer> getByCreator(UUID creatorId) {
        return this.getByCreator(creatorId, ResultOptions.allRows());
    }

    /**
     * Gets a list of all {@link SaleOffer saleOffers} created by a {@link User user}.
     * 
     * @param creatorId The id of the {@link User member} that created the {@link SaleOffer sale
     *                  offers} to get.
     * @param options   The result options.
     * @return A stream of {@link SaleOffer saleOffers}.
     */
    public Stream<SaleOffer> getByCreator(UUID creatorId, ResultOptions options);

    /**
     * Gets a list of {@link SaleOffer saleOffers} created by a {@link User user}.
     * 
     * @param creatorId The id of the {@link User member} that created the {@link SaleOffer sale
     *                  offers} to get.
     * @param options   The result options.
     * @return A stream of {@link SaleOffer saleOffers}.
     */
    public PagingResult<SaleOffer> getByCreator(UUID creatorId, FilterPagingOptions options);

    /**
     * Gets a list of {@link SaleOffer sale offers} not created by a {@link User user}.
     * 
     * @param creatorId The id of the {@link User member} whose {@link SaleOffer sale offers} to
     *                  except.
     * @param options   The result options.
     * @return A stream of {@link SaleOffer sale offers}.
     */
    public PagingResult<SaleOffer> getExceptCreator(UUID creatorId, FilterPagingOptions options);

    // endregion Methods

}
