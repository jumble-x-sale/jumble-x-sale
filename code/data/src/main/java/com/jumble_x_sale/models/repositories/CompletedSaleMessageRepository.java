package com.jumble_x_sale.models.repositories;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.CompletedSale;
import com.jumble_x_sale.models.CompletedSaleMessage;

/**
 * A repository for completed sale messages.
 */
public interface CompletedSaleMessageRepository
        extends Repository<JumbleUnitOfWork, CompletedSaleMessage> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            map.put("completedSaleId", new String[] {"completedSaleId"});
            map.put("senderId", new String[] {"sender", "id"});
            map.put("creationInstant", new String[] {"creationInstant"});
            map.put("message", new String[] {"message"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Assigns a {@link SaleOfferMessage saleOfferMessage} to a {@link CompletedSale
     * completedSaleOffer} and remove its assignment from the {@link SaleOffer saleOffer}.
     * 
     * @param completedSale    The completed sale where the message will be assigned to.
     * @param saleOfferMessage The message itself.
     * @return A completed sale message.
     */
    default CompletedSaleMessage create(CompletedSale completedSale,
            SaleOfferMessage saleOfferMessage) {
        CompletedSaleMessage message = new CompletedSaleMessage(completedSale, saleOfferMessage);
        this.add(message);
        completedSale.getMessages().add(message);
        this.getUnitOfWork().getSaleOfferMessageRepository().remove(saleOfferMessage);
        return message;
    }

    /**
     * Lists {@link CompletedSaleMessage completed sale messages} belonging to {@link CompletedSale
     * completed sales} of a particular {@link User user} where the filters match.
     * 
     * @param creatorId           The id of the {@link CompletedSale completed sales} creator to
     *                            look at.
     * @param filterPagingOptions The filter paging options.
     * @return A page of {@link CompletedSaleMessage completed sale messages}.
     */
    public PagingResult<CompletedSaleMessage> getBySaleCreator(UUID saleCreatorId,
            FilterPagingOptions filterPagingOptions);

    /**
     * Lists {@link CompletedSaleMessage completed sale messages}.
     * 
     * @param completedSaleId The completed sale where the search is executed.
     * @param options         The result options.
     * @return A iterable list of completed sale messages.
     */
    public Stream<CompletedSaleMessage> getBySale(UUID completedSaleId, ResultOptions options);

    /**
     * Lists {@link CompletedSaleMessage completed sale messages}.
     * 
     * @param completedSaleId The completed sale where the search is executed.
     * @param options         The result options.
     * @return A iterable list of completed sale messages.
     */
    public PagingResult<CompletedSaleMessage> getBySale(UUID completedSaleId,
            PagingOptions options);

    /**
     * Lists {@link CompletedSaleMessage completed sale messages}.
     * 
     * @param completedSaleId The completed sale where the search is executed.
     * @return A iterable list of completed sale messages
     */
    default Stream<CompletedSaleMessage> getBySale(UUID completedSaleId) {
        return this.getBySale(completedSaleId, ResultOptions.allRows());
    }

    /**
     * Lists {@link CompletedSaleMessage completed sale messages} by a String text.
     * 
     * @param completedSaleId The completed sale where the search is executed.
     * @param containedText   The String which will be searched for.
     * @param options         The result options.
     * @return A stream of completed sale messages.
     */
    public Stream<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText,
            ResultOptions options);

    /**
     * Lists {@link CompletedSaleMessage completed sale messages} by a String text.
     * 
     * @param completedSaleId The completed sale where the search is executed.
     * @param containedText   The String which will be searched for.
     * @param options         The paging options.
     * @return The paging result with completed sale messages.
     */
    public PagingResult<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText,
            PagingOptions options);

    /**
     * Lists {@link CompletedSaleMessage completed sale messages} by a String text.
     * 
     * @param completedSaleId The completed sale where the search is executed.
     * @param containedText   The String which will be searched for.
     * @return A stream of completed sale messages.
     */
    default Stream<CompletedSaleMessage> getByText(UUID completedSaleId, String containedText) {
        return this.getByText(completedSaleId, containedText, ResultOptions.allRows());
    }

    // endregion Methods

}
