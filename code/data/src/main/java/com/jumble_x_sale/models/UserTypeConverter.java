package com.jumble_x_sale.models;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class UserTypeConverter implements AttributeConverter<UserType, String> {

    @Override
    public String convertToDatabaseColumn(UserType userType) {
        return userType.getCode();
    }

    @Override
    public UserType convertToEntityAttribute(String code) {
        return UserType.tryValueOf(code);
    }

}
