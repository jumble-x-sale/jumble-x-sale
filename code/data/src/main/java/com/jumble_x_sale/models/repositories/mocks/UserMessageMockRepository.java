package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.UserMessage;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.UserMessageRepository;

import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for user messages.
 */
public final class UserMessageMockRepository extends MockRepository<JumbleUnitOfWork, UserMessage>
        implements UserMessageRepository {

    public UserMessageMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, UserMessage.class);
    }

    /**
     * {@inheritDoc}
     */
    public PagingResult<UserMessage> getByMember(UUID memberId, FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> (m.getReceiver().getId().equals(memberId)
                        || m.getSender().getId().equals(memberId)));
    }

    /**
     * {@inheritDoc}
     */
    public Stream<UserMessage> getByReceiver(UUID receiverId, ResultOptions options) {
        Stream<UserMessage> stream = StreamSupport.stream(this
                .getByPredicate(m -> (m.getReceiver().getId().equals(receiverId))).spliterator(),
                false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    public PagingResult<UserMessage> getByReceiver(UUID receiverId, FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> (m.getReceiver().getId().equals(receiverId)));
    }

    /**
     * {@inheritDoc}
     */
    public Stream<UserMessage> getByText(UUID receiverId, String containedText,
            ResultOptions resultOptions) {
        Stream<UserMessage> stream = StreamSupport
                .stream(this.getByPredicate(m -> (m.getSender().getId().equals(receiverId)
                        || m.getReceiver().getId().equals(receiverId))
                        && m.getMessage().contains(containedText)).spliterator(), false);
        if (resultOptions.getStartPosition() > 0) {
            stream.skip(resultOptions.getStartPosition());
        }
        if (resultOptions.getResultCount() > 0) {
            stream.limit(resultOptions.getResultCount());
        }
        return stream;
    }

}
