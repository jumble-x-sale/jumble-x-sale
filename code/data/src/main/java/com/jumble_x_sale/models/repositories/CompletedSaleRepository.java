package com.jumble_x_sale.models.repositories;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.CompletedSale;

/**
 * A repository for completed sales.
 */
public interface CompletedSaleRepository extends Repository<JumbleUnitOfWork, CompletedSale> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;
        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            map.put("buyerId", new String[] {"buyer", "id"});
            map.put("completionInstant", new String[] {"completionInstant"});
            map.put("creatorId", new String[] {"info", "creator", "id"});
            map.put("creationInstant", new String[] {"info", "creationInstant"});
            map.put("category", new String[] {"info", "category"});
            map.put("title", new String[] {"info", "title"});
            map.put("description", new String[] {"info", "description"});
            map.put("imageUrls", new String[] {"info", "imageUrls"});
            map.put("price", new String[] {"info", "price"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a {@link CompletedSale completed sales}.
     * 
     * @param saleOffer The {@link SaleOffer saleOffer} to be finished.
     * @param buyer     The member who acts as a buyer.
     * @return A completed sale.
     */
    default CompletedSale create(SaleOffer saleOffer, User buyer) {
        CompletedSale completedSale = new CompletedSale(saleOffer.getInfo(), buyer);
        this.add(completedSale);
        JumbleUnitOfWork uow = this.getUnitOfWork();
        CompletedSaleMessageRepository completedSaleMessageRepository =
                uow.getCompletedSaleMessageRepository();
        for (SaleOfferMessage saleOfferMessage : saleOffer.getMessages()) {
            completedSaleMessageRepository.create(completedSale, saleOfferMessage);
        }
        uow.getSaleOfferRepository().remove(saleOffer);
        return completedSale;
    }

    /**
     * Gets all {@link CompletedSale completed sales} where the member is the {@link User buyer}.
     * 
     * @param buyer   A member.
     * @param options The result options.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    public Stream<CompletedSale> getByBuyer(User buyer, ResultOptions options);

    /**
     * Gets all {@link CompletedSale completed sales} where the member is the {@link User buyer}.
     * 
     * @param buyer   A member.
     * @param options The result options.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    public PagingResult<CompletedSale> getByBuyer(User buyer, PagingOptions options);

    /**
     * Gets all {@link CompletedSale completed sales} where the member is the {@link User buyer}.
     * 
     * @param buyer A member.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    default Stream<CompletedSale> getByBuyer(User buyer) {
        return this.getByBuyer(buyer, ResultOptions.allRows());
    }

    /**
     * Gets all {@link CompletedSale completed sales} where the member is the {@link User creator}.
     * 
     * @param creatorId The id of the {@link User creator}.
     * @param options   The result options.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    public Stream<CompletedSale> getByCreator(UUID creatorId, ResultOptions options);

    /**
     * Gets all {@link CompletedSale completed sales} where the member is the {@link User creator}.
     * 
     * @param creatorId The id of the {@link User creator}.
     * @param options   The result options.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    public PagingResult<CompletedSale> getByCreator(UUID creatorId, FilterPagingOptions options);

    /**
     * Gets a list of {@link CompletedSale completed sales} not created by a {@link User user}.
     * 
     * @param creatorId The id of the {@link User member} whose {@link CompletedSale completed
     *                  sales} to except.
     * @param options   The result options.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    public PagingResult<CompletedSale> getExceptCreator(UUID creatorId,
            FilterPagingOptions options);

    /**
     * Gets all {@link CompletedSale completed sales} where the member is the {@link User creator}.
     * 
     * @param creatorId The id of the {@link User creator}.
     * @return A stream of {@link CompletedSale completed sales}.
     */
    default Stream<CompletedSale> getByCreator(UUID creatorId) {
        return this.getByCreator(creatorId, ResultOptions.allRows());
    }

    // endregion Methods

}
