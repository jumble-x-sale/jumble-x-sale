package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.SaleOffer;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SaleOfferRepository;
import com.jumble_x_sale.util.Utils;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for sale offers.
 */
public final class SaleOfferPersistentRepository
        extends PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, SaleOffer>
        implements SaleOfferRepository {

    // region Inner Classes

    private static final class SaleOfferByCategoryQuery extends QueryBase<SaleOffer, UUID> {
        private ParameterExpression<String> category = this.b.parameter(String.class);

        public Stream<SaleOffer> find(String category, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.category, category));
            return find(options);
        }

        public PagingResult<SaleOffer> find(String category, PagingOptions options) {
            prepare(tq -> tq.setParameter(this.category, category));
            return find(options);
        }

        public SaleOfferByCategoryQuery(EntityAccess<SaleOffer, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CATEGORY), category));
        }
    }

    private static final class SaleOfferByCreatorQuery extends QueryBase<SaleOffer, UUID> {
        private SaleOfferRepository repo;
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public Stream<SaleOffer> find(UUID creatorId, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId));
            return find(options);
        }

        public PagingResult<SaleOffer> find(UUID creatorId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public SaleOfferByCreatorQuery(SaleOfferRepository repo,
                EntityAccess<SaleOffer, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    private static final class SaleOfferExceptCreatorQuery extends QueryBase<SaleOffer, UUID> {
        private SaleOfferRepository repo;
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public PagingResult<SaleOffer> find(UUID creatorId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public SaleOfferExceptCreatorQuery(SaleOfferRepository repo,
                EntityAccess<SaleOffer, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String INFO = "info";
    protected static final String CREATOR = "creator";
    protected static final String CATEGORY = "category";

    protected SaleOfferByCategoryQuery saleOfferByCategoryQuery = null;
    protected SaleOfferByCreatorQuery saleOfferByCreatorQuery = null;
    protected SaleOfferExceptCreatorQuery saleOfferExceptCreatorQuery = null;

    // endregion Fields

    // region Constructors

    public SaleOfferPersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, SaleOffer.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(SaleOffer entity) {
        return super.remove(entity);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOffer> getByCategory(String category, ResultOptions options) {
        if (Utils.isNullOrWhiteSpace(category)) {
            return this.emptyResult;
        }
        if (this.saleOfferByCategoryQuery != null) {
            return this.saleOfferByCategoryQuery.find(category, options);
        }
        return (this.saleOfferByCategoryQuery = new SaleOfferByCategoryQuery(this.entityAccess))
                .find(category, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOffer> getByCategory(String category, PagingOptions options) {
        if (Utils.isNullOrWhiteSpace(category)) {
            return new PagingResult<>(options, this.emptyResult);
        }
        if (this.saleOfferByCategoryQuery != null) {
            return this.saleOfferByCategoryQuery.find(category, options);
        }
        return (this.saleOfferByCategoryQuery = new SaleOfferByCategoryQuery(this.entityAccess))
                .find(category, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOffer> getByCreator(UUID creatorId, ResultOptions options) {
        if (creatorId == null) {
            return this.emptyResult;
        }
        if (this.saleOfferByCreatorQuery != null) {
            return this.saleOfferByCreatorQuery.find(creatorId, options);
        }
        return (this.saleOfferByCreatorQuery = new SaleOfferByCreatorQuery(this, this.entityAccess))
                .find(creatorId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOffer> getByCreator(UUID creatorId, FilterPagingOptions options) {
        if (this.saleOfferByCreatorQuery != null) {
            return this.saleOfferByCreatorQuery.find(creatorId, options);
        }
        return (this.saleOfferByCreatorQuery = new SaleOfferByCreatorQuery(this, this.entityAccess))
                .find(creatorId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOffer> getExceptCreator(UUID creatorId, FilterPagingOptions options) {
        if (this.saleOfferExceptCreatorQuery != null) {
            return this.saleOfferExceptCreatorQuery.find(creatorId, options);
        }
        return (this.saleOfferExceptCreatorQuery =
                new SaleOfferExceptCreatorQuery(this, this.entityAccess)).find(creatorId, options);
    }

    // endregion Methods

}
