package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

import java.time.Instant;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * A class for messages assigned to completed sales.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "CompletedSaleMessageId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "CompletedSaleMessages")
public class CompletedSaleMessage extends Entity {

    // region Fields

    private static final long serialVersionUID = 8599481363760493408L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "CompletedSaleId", nullable = false)
    private CompletedSale completedSale;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SenderId", nullable = false)
    private User sender;

    @Column(name = "CreationInstant", nullable = false)
    private Instant creationInstant;

    @Column(name = "Message", nullable = false)
    private String message;

    // endregion Fields

    // region Getters

    public CompletedSale getCompletedSale() {
        return this.completedSale;
    }

    public User getSender() {
        return this.sender;
    }

    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getMessage() {
        return this.message;
    }

    // endregion Getters

    // region Setters

    public void setCompletedSale(CompletedSale value) {
        this.completedSale = value;
    }

    public void setSender(User value) {
        this.sender = value;
    }

    public void setCreationInstant(Instant value) {
        this.creationInstant = value;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public CompletedSaleMessage() {
        this.completedSale = null;
        this.sender = null;
        this.message = null;
        this.creationInstant = null;
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public CompletedSaleMessage(CompletedSaleMessage other) {
        this.completedSale = other.completedSale;
        this.sender = other.sender;
        this.message = other.message;
        this.creationInstant = other.creationInstant;
    }

    /**
     * Constructor.
     * 
     * @param completedSale    The completed sale where the message will be assigned to.
     * @param saleOfferMessage The message itself.
     */
    public CompletedSaleMessage(CompletedSale completedSale, SaleOfferMessage saleOfferMessage) {
        this.completedSale = completedSale;
        this.sender = saleOfferMessage.getSender();
        this.message = saleOfferMessage.getMessage();
        this.creationInstant = saleOfferMessage.getCreationInstant();
    }

    // endregion Constructors

}
