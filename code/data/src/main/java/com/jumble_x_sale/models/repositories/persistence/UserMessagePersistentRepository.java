package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.UserMessage;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.UserMessageRepository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for user messages.
 */
public final class UserMessagePersistentRepository
        extends PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, UserMessage>
        implements UserMessageRepository {

    // region Inner Classes

    private static final class UserMessageByMemberQuery extends QueryBase<UserMessage, UUID> {
        private UserMessagePersistentRepository repo;
        private ParameterExpression<UUID> memberId = this.b.parameter(UUID.class);

        public PagingResult<UserMessage> find(UUID memberId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.memberId, memberId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public UserMessageByMemberQuery(UserMessagePersistentRepository repo,
                EntityAccess<UserMessage, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.or(b.equal(r.get(RECEIVER).get(ID), memberId),
                    b.equal(r.get(SENDER).get(ID), memberId)));
        }
    }

    private static final class UserMessageByReceiverQuery extends QueryBase<UserMessage, UUID> {
        private UserMessagePersistentRepository repo;
        private ParameterExpression<UUID> receiverId = this.b.parameter(UUID.class);

        public Stream<UserMessage> find(UUID receiverId, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.receiverId, receiverId));
            return find(options);
        }

        public PagingResult<UserMessage> find(UUID receiverId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.receiverId, receiverId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public UserMessageByReceiverQuery(UserMessagePersistentRepository repo,
                EntityAccess<UserMessage, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.equal(r.get(RECEIVER).get(ID), receiverId));
        }
    }

    private static final class UserMessageByTextQuery extends QueryBase<UserMessage, UUID> {
        private ParameterExpression<UUID> receiverId = this.b.parameter(UUID.class);
        private ParameterExpression<String> text = this.b.parameter(String.class);

        public Stream<UserMessage> find(UUID receiverId, String containedText,
                ResultOptions resultOptions) {
            prepare(tq -> {
                tq.setParameter(this.receiverId, receiverId);
                tq.setParameter(text, "%" + containedText + "%");
            });
            return find(resultOptions);
        }

        public UserMessageByTextQuery(EntityAccess<UserMessage, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.and(b.equal(r.get(RECEIVER).get(ID), receiverId),
                    b.like(r.get(MESSAGE), text)));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String RECEIVER = "receiver";
    protected static final String SENDER = "sender";
    protected static final String MESSAGE = "message";
    protected static final String FIRST_NAME = "firstName";
    protected static final String LAST_NAME = "lastName";

    protected UserMessageByMemberQuery userMessageByMemberQuery = null;
    protected UserMessageByReceiverQuery userMessageByReceiverQuery = null;
    protected UserMessageByTextQuery userMessageByTextQuery = null;

    // endregion Fields

    // region Constructors

    public UserMessagePersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, UserMessage.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<UserMessage> getByMember(UUID memberId, FilterPagingOptions options) {
        if (this.userMessageByMemberQuery != null) {
            return this.userMessageByMemberQuery.find(memberId, options);
        }
        return (this.userMessageByMemberQuery =
                new UserMessageByMemberQuery(this, this.entityAccess)).find(memberId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<UserMessage> getByReceiver(UUID receiverId, ResultOptions options) {
        if (this.userMessageByReceiverQuery != null) {
            return this.userMessageByReceiverQuery.find(receiverId, options);
        }
        return (this.userMessageByReceiverQuery =
                new UserMessageByReceiverQuery(this, this.entityAccess)).find(receiverId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<UserMessage> getByReceiver(UUID receiverId, FilterPagingOptions options) {
        if (this.userMessageByReceiverQuery != null) {
            return this.userMessageByReceiverQuery.find(receiverId, options);
        }
        return (this.userMessageByReceiverQuery =
                new UserMessageByReceiverQuery(this, this.entityAccess)).find(receiverId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<UserMessage> getByText(UUID receiverId, String containedText,
            ResultOptions options) {
        if (this.userMessageByTextQuery != null) {
            return this.userMessageByTextQuery.find(receiverId, containedText, options);
        }
        return (this.userMessageByTextQuery = new UserMessageByTextQuery(this.entityAccess))
                .find(receiverId, containedText, options);
    }

    // endregion Methods

}
