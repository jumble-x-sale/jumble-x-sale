package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

// import java.io.Serializable;
import java.time.Instant;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * SecretSanta contains information about the {@link User creator}, the category, the title and the
 * description of a secret santa.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "SecretSantaOfferId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "SecretSantaOffers")
public class SecretSantaOffer extends Entity {

    // region Fields

    private static final long serialVersionUID = -3624216806197333391L;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "category", column = @Column(name = "Category")),
            @AttributeOverride(name = "title", column = @Column(name = "Title")),
            @AttributeOverride(name = "description", column = @Column(name = "Description"))})
    @AssociationOverrides({
            @AssociationOverride(name = "creator", joinColumns = @JoinColumn(name = "CreatorId"))})
    private ItemInfo info;

    // endregion Fields

    // region Getters

    public ItemInfo getInfo() {
        return this.info;
    }

    // endregion Getters

    // region Setters

    public void setInfo(ItemInfo value) {
        this.info = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public SecretSantaOffer() {
        this.info = null;
    }

    /**
     * Constructor.
     * 
     * @param info The info for this secret santa offer.
     */
    public SecretSantaOffer(ItemInfo info) {
        this.info = info;
    }

    /**
     * Constructor.
     * 
     * @param creator         A {@link User creator} who creates the {@link SecretSantaOffer secret
     *                        santa offer}.
     * @param creationInstant The the point in time when the {@link SecretSantaOffer secret santa
     *                        offer} was created (should be "now").
     * @param category        The category of the {@link SecretSantaOffer secret santa offer}.
     * @param title           The title of the {@link SecretSantaOffer secret santa offer}.
     * @param description     The description of the {@link SecretSantaOffer secret santa offer}.
     * @param imageUrls       An String array which contains the paths to the sale offer images.
     * @param mainImageData   A byte array which contains the main image data.
     */
    public SecretSantaOffer(User creator, Instant creationInstant, String category, String title,
            String description, String[] imageUrls, byte[] mainImageData) {
        this(new ItemInfo(creator, creationInstant, category, title, description, imageUrls,
                mainImageData));
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public SecretSantaOffer(SecretSantaOffer other) {
        this.info = other.info;
    }

    // endregion Constructors

}
