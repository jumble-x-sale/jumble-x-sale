package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.SaleOfferMessage;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.SaleOfferMessageRepository;

import java.util.UUID;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for sale offer messages.
 */
public final class SaleOfferMessageMockRepository extends
        MockRepository<JumbleUnitOfWork, SaleOfferMessage> implements SaleOfferMessageRepository {

    public SaleOfferMessageMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, SaleOfferMessage.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOfferMessage> getBySaleCreator(UUID saleCreatorId,
            FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> m.getSaleOffer().getInfo().getCreator().getId().equals(saleCreatorId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOfferMessage> getBySale(UUID saleOfferId, ResultOptions options) {
        Stream<SaleOfferMessage> stream = StreamSupport.stream(this
                .getByPredicate(m -> m.getSaleOffer().getId().equals(saleOfferId)).spliterator(),
                false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<SaleOfferMessage> getBySale(UUID saleOfferId, FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> m.getSaleOffer().getId().equals(saleOfferId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<SaleOfferMessage> getByText(UUID saleOfferId, String text,
            ResultOptions resultOptions) {
        Stream<SaleOfferMessage> stream = StreamSupport.stream(this.getByPredicate(
                m -> m.getSaleOffer().getId().equals(saleOfferId) && m.getMessage().contains(text))
                .spliterator(), false);
        if (resultOptions.getStartPosition() > 0) {
            stream.skip(resultOptions.getStartPosition());
        }
        if (resultOptions.getResultCount() > 0) {
            stream.limit(resultOptions.getResultCount());
        }
        return stream;
    }

}
