package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

import java.time.Instant;
// import java.io.Serializable;

import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

/**
 * CompletedSecretSanta contains information about a completed secret santa including 2 matches and
 * the time they matched.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "CompletedSecretSantaId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "CompletedSecretSantas")
public class CompletedSecretSanta extends Entity {// implements Serializable {

    // region Fields

    private static final long serialVersionUID = -4627210949547527876L;

    @Column(name = "CompletionInstant", nullable = false)
    private Instant completionInstant;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "creationInstant",
                    column = @Column(name = "CreationInstant1")),
            @AttributeOverride(name = "category", column = @Column(name = "Category1")),
            @AttributeOverride(name = "title", column = @Column(name = "Title1")),
            @AttributeOverride(name = "description", column = @Column(name = "Description1")),
            @AttributeOverride(name = "imageUrls", column = @Column(name = "ImageUrls1")),
            @AttributeOverride(name = "mainImageData", column = @Column(name = "MainImageData1"))})
    @AssociationOverrides({
            @AssociationOverride(name = "creator", joinColumns = @JoinColumn(name = "CreatorId1"))})
    private ItemInfo match1;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "creationInstant",
                    column = @Column(name = "CreationInstant2")),
            @AttributeOverride(name = "category", column = @Column(name = "Category2")),
            @AttributeOverride(name = "title", column = @Column(name = "Title2")),
            @AttributeOverride(name = "description", column = @Column(name = "Description2")),
            @AttributeOverride(name = "imageUrls", column = @Column(name = "ImageUrls2")),
            @AttributeOverride(name = "mainImageData", column = @Column(name = "MainImageData2"))})
    @AssociationOverrides({
            @AssociationOverride(name = "creator", joinColumns = @JoinColumn(name = "CreatorId2"))})
    private ItemInfo match2;

    // endregion Fields

    // region Getters

    public Instant getCompletionInstant() {
        return this.completionInstant;
    }

    public ItemInfo getMatch1() {
        return this.match1;
    }

    public ItemInfo getMatch2() {
        return this.match2;
    }

    // endregion Getters

    // region Setters

    public void setCompletionInstant(Instant value) {
        this.completionInstant = value;
    }

    public void setMatch1(ItemInfo value) {
        this.match1 = value;
    }

    public void setMatch2(ItemInfo value) {
        this.match2 = value;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public CompletedSecretSanta() {
        this.completionInstant = null;
        this.match1 = null;
        this.match2 = null;
    }

    /**
     * Constructor.
     * 
     * @param completionInstant The time when the secret santa was completed.
     * @param match1            The first match.
     * @param match2            The second match.
     */
    public CompletedSecretSanta(Instant completionInstant, ItemInfo match1, ItemInfo match2) {
        this.completionInstant = completionInstant;
        this.match1 = match1;
        this.match2 = match2;
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public CompletedSecretSanta(CompletedSecretSanta other) {
        this.completionInstant = other.completionInstant;
        this.match1 = other.match1;
        this.match2 = other.match2;
    }

    // endregion Constructors

}
