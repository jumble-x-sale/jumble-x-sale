package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.CompletedSale;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.util.Lazy;
import com.jumble_x_sale.models.repositories.CompletedSaleRepository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for completed sales.
 */
public final class CompletedSalePersistentRepository
        extends PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, CompletedSale>
        implements CompletedSaleRepository {

    // region Inner Classes

    private static final class CompletedSaleByCreatorQuery extends QueryBase<CompletedSale, UUID> {
        private CompletedSalePersistentRepository repo;
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public Stream<CompletedSale> find(UUID creatorId, ResultOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId));
            return find(options);
        }

        public PagingResult<CompletedSale> find(UUID creatorId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public CompletedSaleByCreatorQuery(CompletedSalePersistentRepository repo,
                EntityAccess<CompletedSale, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    private static final class CompletedSaleExceptCreatorQuery
            extends QueryBase<CompletedSale, UUID> {
        private CompletedSalePersistentRepository repo;
        private ParameterExpression<UUID> creatorId = this.b.parameter(UUID.class);

        public PagingResult<CompletedSale> find(UUID creatorId, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(this.creatorId, creatorId),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public CompletedSaleExceptCreatorQuery(CompletedSalePersistentRepository repo,
                EntityAccess<CompletedSale, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter((b, r) -> b.equal(r.get(INFO).get(CREATOR).get(ID), creatorId));
        }
    }

    private static final class CompletedSaleByBuyerQuery extends QueryBase<CompletedSale, UUID> {
        private ParameterExpression<UUID> buyerId = this.b.parameter(UUID.class);

        public Stream<CompletedSale> find(User member, ResultOptions options) {
            prepare(tq -> tq.setParameter(buyerId, member.getId()));
            return find(options);
        }

        public PagingResult<CompletedSale> find(User member, PagingOptions options) {
            prepare(tq -> tq.setParameter(buyerId, member.getId()));
            return find(options);
        }

        public CompletedSaleByBuyerQuery(EntityAccess<CompletedSale, UUID> access) {
            super(access);
            setMainPredicateGetter((b, r) -> b.equal(r.get(BUYER).get(ID), buyerId));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String INFO = "info";
    protected static final String CREATOR = "creator";
    protected static final String BUYER = "buyer";

    protected Lazy<CompletedSaleByCreatorQuery> completedSaleByCreatorQuery =
            new Lazy<>(() -> new CompletedSaleByCreatorQuery(this, this.entityAccess));
    protected Lazy<CompletedSaleExceptCreatorQuery> completedSaleExceptCreatorQuery =
            new Lazy<>(() -> new CompletedSaleExceptCreatorQuery(this, this.entityAccess));
    protected Lazy<CompletedSaleByBuyerQuery> completedSaleByBuyerQuery =
            new Lazy<>(() -> new CompletedSaleByBuyerQuery(this.entityAccess));

    // endregion Fields

    // region Constructors

    public CompletedSalePersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, CompletedSale.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(CompletedSale entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSale> getByCreator(UUID creatorId, ResultOptions options) {
        if (creatorId == null) {
            return this.emptyResult;
        }
        return this.completedSaleByCreatorQuery.get().find(creatorId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSale> getByCreator(UUID creatorId, FilterPagingOptions options) {
        if (creatorId == null) {
            return new PagingResult<>(options, this.emptyResult);
        }
        return this.completedSaleByCreatorQuery.get().find(creatorId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSale> getExceptCreator(UUID creatorId,
            FilterPagingOptions options) {
        if (creatorId == null) {
            return new PagingResult<>(options, this.emptyResult);
        }
        return this.completedSaleExceptCreatorQuery.get().find(creatorId, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSale> getByBuyer(User buyer, ResultOptions options) {
        if (buyer == null) {
            return this.emptyResult;
        }
        return this.completedSaleByBuyerQuery.get().find(buyer, options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSale> getByBuyer(User buyer, PagingOptions options) {
        if (buyer == null) {
            return new PagingResult<>(options, this.emptyResult);
        }
        return this.completedSaleByBuyerQuery.get().find(buyer, options);
    }

    // endregion Methods

}
