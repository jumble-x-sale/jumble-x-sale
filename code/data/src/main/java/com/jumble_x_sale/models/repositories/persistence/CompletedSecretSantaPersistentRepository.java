package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.CompletedSecretSanta;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSecretSantaRepository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.persistence.EntityAccess;
import com.jumble_x_sale.data.persistence.EntitySession;
import com.jumble_x_sale.data.persistence.PersistentRepository;
import com.jumble_x_sale.data.persistence.QueryBase;

import java.util.UUID;
import java.util.stream.Stream;

import javax.persistence.criteria.ParameterExpression;

/**
 * A persistent repository for completed secret santas.
 */
public final class CompletedSecretSantaPersistentRepository extends
        PersistentRepository<JumblePersistentUnitOfWork, JumbleUnitOfWork, CompletedSecretSanta>
        implements CompletedSecretSantaRepository {

    // region Inner Classes

    private static final class CompletedSecretSantaByMatchCreatorQuery
            extends QueryBase<CompletedSecretSanta, UUID> {
        private CompletedSecretSantaPersistentRepository repo;
        private ParameterExpression<UUID> matchCreatorId = this.b.parameter(UUID.class);

        public Stream<CompletedSecretSanta> find(User creator, ResultOptions options) {
            prepare(tq -> tq.setParameter(matchCreatorId, creator.getId()));
            return find(options);
        }

        public PagingResult<CompletedSecretSanta> find(User creator, FilterPagingOptions options) {
            prepare(tq -> tq.setParameter(matchCreatorId, creator.getId()),
                    r -> predicate(repo.getPropertyNameMappings(), options.getFilters()));
            return find(options);
        }

        public CompletedSecretSantaByMatchCreatorQuery(
                CompletedSecretSantaPersistentRepository repo,
                EntityAccess<CompletedSecretSanta, UUID> access) {
            super(access);
            this.repo = repo;
            setMainPredicateGetter(
                    (b, r) -> b.or(b.equal(r.get(MATCH1).get(CREATOR).get(ID), matchCreatorId),
                            b.equal(r.get(MATCH2).get(CREATOR).get(ID), matchCreatorId)));
        }
    }

    // endregion Inner Classes

    // region Fields

    protected static final String ID = "id";
    protected static final String MATCH1 = "match1";
    protected static final String MATCH2 = "match2";
    protected static final String CREATOR = "creator";

    protected CompletedSecretSantaByMatchCreatorQuery completedSecretSantaByMatchCreatorQuery =
            null;

    // endregion Fields

    // region Constructors

    public CompletedSecretSantaPersistentRepository(JumblePersistentUnitOfWork unitOfWork,
            EntitySession entitySession) {
        super(unitOfWork, entitySession, CompletedSecretSanta.class);
    }

    // endregion Constructors

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(CompletedSecretSanta entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSecretSanta> getByMember(User member, ResultOptions options) {
        if (member == null) {
            return this.emptyResult;
        }
        if (this.completedSecretSantaByMatchCreatorQuery != null) {
            return this.completedSecretSantaByMatchCreatorQuery.find(member, options);
        }
        return (this.completedSecretSantaByMatchCreatorQuery =
                new CompletedSecretSantaByMatchCreatorQuery(this, this.entityAccess)).find(member,
                        options);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSecretSanta> getByMember(User member,
            FilterPagingOptions options) {
        if (member == null) {
            return new PagingResult<>(options, this.emptyResult);
        }
        if (this.completedSecretSantaByMatchCreatorQuery != null) {
            return this.completedSecretSantaByMatchCreatorQuery.find(member, options);
        }
        return (this.completedSecretSantaByMatchCreatorQuery =
                new CompletedSecretSantaByMatchCreatorQuery(this, this.entityAccess)).find(member,
                        options);
    }

    // endregion Methods

}
