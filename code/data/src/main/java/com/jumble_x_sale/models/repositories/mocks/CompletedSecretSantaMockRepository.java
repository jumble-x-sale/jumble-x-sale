package com.jumble_x_sale.models.repositories.mocks;

import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.CompletedSecretSanta;
import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.CompletedSecretSantaRepository;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.data.mocks.MockRepository;

/**
 * A mock repository for completed secret santas.
 */
public final class CompletedSecretSantaMockRepository
        extends MockRepository<JumbleUnitOfWork, CompletedSecretSanta>
        implements CompletedSecretSantaRepository {

    public CompletedSecretSantaMockRepository(JumbleUnitOfWork unitOfWork) {
        super(unitOfWork, CompletedSecretSanta.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(CompletedSecretSanta entity) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSecretSanta> getByMember(User member) {
        return this.getByPredicate(s -> s.getMatch1().getCreator().getId().equals(member.getId())
                || s.getMatch2().getCreator().getId().equals(member.getId()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Stream<CompletedSecretSanta> getByMember(User member, ResultOptions options) {
        Stream<CompletedSecretSanta> stream =
                StreamSupport.stream(this.getByMember(member).spliterator(), false);
        if (options.getStartPosition() > 0) {
            stream.skip(options.getStartPosition());
        }
        if (options.getResultCount() > 0) {
            stream.limit(options.getResultCount());
        }
        return stream;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PagingResult<CompletedSecretSanta> getByMember(User member,
            FilterPagingOptions options) {
        return this.get(options.getFilters(), options,
                m -> m.getMatch1().getCreator().getId().equals(member.getId())
                        || m.getMatch2().getCreator().getId().equals(member.getId()));
    }

}
