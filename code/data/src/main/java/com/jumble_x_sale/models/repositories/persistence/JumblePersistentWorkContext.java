package com.jumble_x_sale.models.repositories.persistence;

import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.JumbleWorkContext;
import com.jumble_x_sale.data.UnitOfWork;
import com.jumble_x_sale.data.persistence.EntityContext;
import com.jumble_x_sale.data.persistence.PersistentWorkContext;

/**
 * A context for working on a subject. Used for managing {@link UnitOfWork units of work}.
 */
public class JumblePersistentWorkContext extends PersistentWorkContext<JumbleUnitOfWork>
        implements JumbleWorkContext {

    // region Fields

    public static final String MAIN_PERSISTENCE_UNIT_NAME =
            JumbleWorkContext.getPersistenceUnitName();

    private final EntityContext mainEntityContext = this.createContext(MAIN_PERSISTENCE_UNIT_NAME);

    // endregion Fields

    // region Methods

    /**
     * {@inheritDoc}
     */
    @Override
    public JumbleUnitOfWork createUnitOfWork() {
        return new JumblePersistentUnitOfWork(this, mainEntityContext);
    }

    // region Methods

}
