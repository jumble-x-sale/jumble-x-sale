package com.jumble_x_sale.models.repositories;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

import com.jumble_x_sale.data.Repository;
import com.jumble_x_sale.data.util.FilterPagingOptions;
import com.jumble_x_sale.data.util.PagingResult;
import com.jumble_x_sale.data.util.ResultOptions;
import com.jumble_x_sale.models.User;
import com.jumble_x_sale.models.UserMessage;

/**
 * A repository for user messages.
 */
public interface UserMessageRepository extends Repository<JumbleUnitOfWork, UserMessage> {

    // region Inner Classes

    /**
     * Hides static fields that have to stay private.
     */
    public static class PrivateStaticFields {
        private static final Map<String, String[]> propertyNameMappings;

        static {
            Map<String, String[]> map = new HashMap<>();
            map.put("id", new String[] {"id"});
            map.put("senderId", new String[] {"sender", "id"});
            map.put("receiverId", new String[] {"receiver", "id"});
            map.put("creationInstant", new String[] {"creationInstant"});
            map.put("message", new String[] {"message"});
            propertyNameMappings = Collections.unmodifiableMap(map);
        }
    }

    // endregion Inner Classes

    // region Getters

    /**
     * Get the path mappings for the properties.
     * 
     * @return The path mappings for the properties.
     */
    default Map<String, String[]> getPropertyNameMappings() {
        return PrivateStaticFields.propertyNameMappings;
    }

    // endregion Getters

    // region Methods

    /**
     * Creates a {@link UserMessage userMessage}.
     * 
     * @param receiver    The {@link User user} who receives the message.
     * @param sender      The {@link User user} who sends the message.
     * @param messageText The text of the message.
     * @return A {@link UserMessage userMessage}.
     */
    default UserMessage create(User receiver, User sender, String messageText) {
        UserMessage message = new UserMessage(receiver, sender, messageText);
        this.add(message);
        receiver.getReceivedMessages().add(message);
        sender.getSentMessages().add(message);
        return message;
    }

    /**
     * Creates a {@link UserMessage userMessage}.
     * 
     * @param receiverId  The id of the {@link User user} who receives the message.
     * @param senderId    The id of the {@link User user} who sends the message.
     * @param messageText The text of the message.
     * @return A {@link UserMessage userMessage}.
     */
    default UserMessage create(UUID receiverId, UUID senderId, String messageText) {
        User receiver = this.getUnitOfWork().getUserRepository().getById(receiverId);
        User sender = this.getUnitOfWork().getUserRepository().getById(senderId);
        return this.create(receiver, sender, messageText);
    }

    /**
     * Get {@link UserMessage user message} for a {@link User member}.
     *
     * @param memberId The {@link User receiver or sender} of the message.
     * @param options  The filter paging options.
     * @return A {@link UserMessage user message} by a part of its text.
     */
    public PagingResult<UserMessage> getByMember(UUID memberId, FilterPagingOptions options);

    /**
     * Get {@link UserMessage user message} for a {@link User receiver}.
     *
     * @param receiverId The id of the user to which the message belongs, that we're looking for.
     * @param options    The result options.
     * @return A {@link UserMessage user message} by a part of its text.
     */
    public Stream<UserMessage> getByReceiver(UUID receiverId, ResultOptions options);

    /**
     * Get {@link UserMessage user message} for a {@link User receiver}.
     *
     * @param receiverId The id of the user to which the message belongs, that we're looking for.
     * @param options    The filter paging options.
     * @return A {@link UserMessage user message} by a part of its text.
     */
    public PagingResult<UserMessage> getByReceiver(UUID receiverId, FilterPagingOptions options);

    /**
     * Get a {@link UserMessage user message} by a part of its text.
     * 
     * @param receiverId The id of the user to which the message belongs, that we're looking for.
     * @return A {@link UserMessage user message} by a part of its text.
     */
    default Stream<UserMessage> getByReceiver(UUID receiverId) {
        return this.getByReceiver(receiverId, ResultOptions.allRows());
    }

    /**
     * Get a {@link UserMessage user message} by a part of its text.
     * 
     * @param receiverId    The id of the user to which the message belongs, that we're looking for.
     * @param containedText A text that is part of the user message to look for.
     * @return A {@link UserMessage user message} by a part of its text.
     */
    default Stream<UserMessage> getByText(UUID receiverId, String containedText) {
        return this.getByText(receiverId, containedText, ResultOptions.allRows());
    }

    /**
     * Get a {@link UserMessage user message} by a part of its text.
     * 
     * @param receiverId    The id of the user to which the message belongs, that we're looking for.
     * @param containedText A text that is part of the user message to look for.
     * @param options       The result options.
     * @return A {@link UserMessage user message} by a part of its text.
     */
    public Stream<UserMessage> getByText(UUID receiverId, String containedText,
            ResultOptions options);

    // endregion Methods

}
