package com.jumble_x_sale.models;

import com.jumble_x_sale.data.Entity;

import java.time.Instant;
// import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 * A class for messages assigned to {@link SaleOffer saleOffers}.
 */
@javax.persistence.Entity
@AttributeOverride(name = "id", column = @Column(name = "SaleOfferMessageId",
        columnDefinition = "binary(16)", nullable = false, updatable = false))
@Table(name = "SaleOfferMessages")
public class SaleOfferMessage extends Entity {

    // region Fields

    private static final long serialVersionUID = 8153181585213621055L;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SaleOfferId", nullable = false)
    private SaleOffer saleOffer;

    @ManyToOne(optional = false)
    @JoinColumn(name = "SenderId", nullable = false)
    private User sender;

    @Column(name = "CreationInstant", nullable = false)
    private Instant creationInstant;

    @Column(name = "Message", nullable = false)
    private String message;

    // endregion Fields

    // region Getters

    public SaleOffer getSaleOffer() {
        return this.saleOffer;
    }

    public User getSender() {
        return this.sender;
    }

    public Instant getCreationInstant() {
        return this.creationInstant;
    }

    public String getMessage() {
        return this.message;
    }

    // endregion Getters

    // region Setters

    public void setSaleOffer(SaleOffer value) {
        this.saleOffer = value;
    }

    public void setSender(User value) {
        this.sender = value;
    }

    public void setCreationInstant(Instant value) {
        this.creationInstant = value;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    // endregion Setters

    // region Constructors

    /**
     * Constructor.
     */
    public SaleOfferMessage() {
        this.saleOffer = null;
        this.sender = null;
        this.message = null;
        this.creationInstant = Instant.now();
    }

    /**
     * Constructor.
     * 
     * @param saleOffer The {@link SaleOffer saleOffer} where the messages will stick to.
     * @param sender    The {@link User sender} who created the message.
     * @param message   Text of the message.
     */
    public SaleOfferMessage(SaleOffer saleOffer, User sender, String message) {
        this.saleOffer = saleOffer;
        this.sender = sender;
        this.message = message;
        this.creationInstant = Instant.now();
    }

    /**
     * Copy constructor.
     * 
     * @param other The object to copy from.
     */
    public SaleOfferMessage(SaleOfferMessage other) {
        this.saleOffer = other.saleOffer;
        this.sender = other.sender;
        this.message = other.message;
        this.creationInstant = other.creationInstant;
    }

    // endregion Constructors

}
