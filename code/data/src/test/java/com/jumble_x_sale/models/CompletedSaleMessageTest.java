package com.jumble_x_sale.models;

import java.math.BigDecimal;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for {@link CompletedSaleMessage completed sale message}.
 */
public class CompletedSaleMessageTest extends JumbleTest {

    @Test
    public void testCompletedSaleMessage() {
        User seller = this.uow.getUserRepository().create(UserType.MEMBER, "Colin", "Meng",
                "colin.meng@fh-erfurt.de", "123abc", new IdCard("lhm310k1g398092282302096"), true);

        User buyer = this.uow.getUserRepository().create(UserType.MEMBER, "John", "Maynard",
                "john.maynard@sea.us", "cde456", new IdCard("lhm310k1g398092282302096"), true);

        this.uow.setActiveUser(seller);

        SaleOffer saleOffer = this.uow.getSaleOfferRepository().create("Books", "Some book title",
                "Some book description", null, null, new BigDecimal(3.50));

        SaleOfferMessage saleOfferMessage = this.uow.getSaleOfferMessageRepository()
                .create(saleOffer, buyer, "I think I'll buy it!");

        CompletedSale completedSaleOffer =
                this.uow.getCompletedSaleRepository().create(saleOffer, buyer);

        CompletedSaleMessage completedSaleOfferMessage = this.uow
                .getCompletedSaleMessageRepository().create(completedSaleOffer, saleOfferMessage);

        assertTrue(completedSaleOfferMessage.getMessage().equals("I think I'll buy it!"));
    }

}
