package com.jumble_x_sale.models;

import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for class {@link User User}.
 */
public class UserTest extends JumbleTest {

    /**
     * Test the class {@link User User}.
     */
    @Test
    public void testUser() {
        User user = this.uow.getUserRepository().create(UserType.ADMIN, "Stefan", "Woyde", "stefan.woyde@fh-erfurt.de",
                "1234", new IdCard("lhm310k1g398092282302096"), true);

        assertFalse(this.uow.getUserRepository().login("stefan.woyde@fh-erfurt.de", "ABCD"));
        assertFalse(this.uow.getActiveUser() != null);
        assertTrue(this.uow.getUserRepository().login("stefan.woyde@fh-erfurt.de", "1234"));
        assertTrue(this.uow.getActiveUser().getId().equals(user.getId()));
    }

}
