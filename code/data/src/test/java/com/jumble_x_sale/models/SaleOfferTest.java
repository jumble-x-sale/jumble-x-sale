package com.jumble_x_sale.models;

import java.math.BigDecimal;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for {@link SaleOffer saleOffer }.
 */
public class SaleOfferTest extends JumbleTest {

    @Test
    public void test() {
        User member1 = this.uow.getUserRepository().create(UserType.MEMBER, "Hans", "Zimmer",
                "hans.zimmer@web.de", "123", new IdCard("lhm310k1g398092282302096"), true);

        User member2 = this.uow.getUserRepository().create(UserType.MEMBER, "Peter", "Enis",
                "p.enis@web.de", "123", new IdCard("lhm310k1g398092282302096"), true);

        String[] text = {"Nachts ist kälter als draußen!",
                "Mein Lamborghini steht draußen vor der Tür!",
                "Der Lamborghini kostet nur drei Geld fuffzich!", "Das hört sich sinnlos an!",
                "Dann komm ich mal morgen vorbei.", "Dann komm ich den Lambo morgen abholen ;-)"};

        this.uow.setActiveUser(member1);
        SaleOffer saleOffer1 =
                this.uow.getSaleOfferRepository().create("auto", "Lamborghini Aventador",
                        "Supersportwagen, gebraucht, gelb", null, null, new BigDecimal(3.50));
        SaleOfferMessage saleOfferMessage1 =
                this.uow.getSaleOfferMessageRepository().create(saleOffer1, member1, text[2]);
        SaleOfferMessage saleOfferMessage2 =
                this.uow.getSaleOfferMessageRepository().create(saleOffer1, member2, text[5]);

        assertTrue(saleOfferMessage1 != null);
        assertTrue(saleOfferMessage2 != null);
    }

}
