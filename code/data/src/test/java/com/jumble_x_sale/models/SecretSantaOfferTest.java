package com.jumble_x_sale.models;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for class {@link SecretSantaOffer SecretSantaOffer}.
 */
public class SecretSantaOfferTest extends JumbleTest {

    @Test
    public void testSecretSantaOffer() {
        User creator = this.uow.getUserRepository().create(UserType.MEMBER, "Max", "Mustermann",
                "max.mustermann@web.de", "123", new IdCard("lhm310k1g398092282302096"), true);

        SecretSantaOffer secretSantaOffer1 = this.uow.getSecretSantaOfferRepository().create(
                creator, "Krimi", "Hans im Glück", "Gutes Buch", new String[0], new byte[0]);
        SecretSantaOffer secretSantaOffer2 =
                this.uow.getSecretSantaOfferRepository().create(creator, "Krimi", "Hans im Glück 2",
                        "Auch ein gutes Buch", new String[0], new byte[0]);

        assertTrue(secretSantaOffer1.getInfo().getCreator().getLastName().equals("Mustermann"));
        assertTrue(secretSantaOffer1.getInfo().getCategory().equals("Krimi"));
        assertTrue(this.uow.getSecretSantaOfferRepository().match(secretSantaOffer1) != null
                && this.uow.getSecretSantaOfferRepository().match(secretSantaOffer1).getId()
                        .equals(secretSantaOffer2.getId()));
    }

}
