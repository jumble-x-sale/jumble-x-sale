package com.jumble_x_sale.models;

import java.time.LocalDate;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Unit test for class {@link IdCard IdCard}.
 */
public class IdCardTest {

    @Test
    public void testIdCard() {
        IdCard test1 = new IdCard("lhm310k1g398092282302096");
        assertTrue(test1.isValid());
        assertTrue(test1.getBirthday().equals(LocalDate.of(1998, 9, 22)));
        assertFalse(test1.isExpired());
    }

}
