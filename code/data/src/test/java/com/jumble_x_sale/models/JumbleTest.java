package com.jumble_x_sale.models;

import com.jumble_x_sale.models.repositories.JumbleUnitOfWork;
import com.jumble_x_sale.models.repositories.JumbleWorkContext;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * A base for unit tests with a {@link JumbleWorkContext work context}.
 */
public abstract class JumbleTest {

    // region Fields

    /**
     * A static work context for all deriving test classes. Works as long as we
     * don't run test classes simultaneously.
     */
    private static JumbleWorkContext wc;
    protected JumbleUnitOfWork uow;

    // endregion Fields

    // region Methods

    @BeforeClass
    public static void runBeforeClass() {
        wc = JumbleWorkContext.create();
    }

    @AfterClass
    public static void runAfterClass() {
        wc.tryClose();
    }

    @Before
    public void runBefore() {
        this.uow = wc.createUnitOfWork();
    }

    @After
    public void runAfter() {
        this.uow.tryClose();
    }

    // endregion Methods

}
