package com.jumble_x_sale.models;

import com.jumble_x_sale.util.Iterables;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit test for class {@link UserMessage UserMessage}.
 */
public class UserMessageTest extends JumbleTest {

    @Test
    public void testUserMessage() {
        User user1 = this.uow.getUserRepository().create(UserType.MEMBER, "Hans", "Zimmer", "hans@zimmer.de", "123",
                new IdCard("lhm310k1g398092282302096"), true);

        User user2 = this.uow.getUserRepository().create(UserType.MEMBER, "Peter", "Stark", "p.stark@web.de", "123",
                new IdCard("lhm310k1g398092282302096"), true);

        String[] text = { "Nachts ist es kälter als draußen!", "Mein Lamborghini steht draußen vor der Tür!",
                "Der Lamborghini kostet nur drei Geld fuffzich!", "Das hört sich sinnlos an!",
                "Dann komm ich mal morgen vorbei.", "Dann komm ich den Lambo morgen abholen ;-)" };
        UserMessage message1 = this.uow.getUserMessageRepository().create(user2, user1, text[0]);
        UserMessage message2 = this.uow.getUserMessageRepository().create(user2, user1, text[1]);
        UserMessage message3 = this.uow.getUserMessageRepository().create(user2, user1, text[2]);

        UserMessage message4 = this.uow.getUserMessageRepository().create(user1, user2, text[3]);
        UserMessage message5 = this.uow.getUserMessageRepository().create(user1, user2, text[4]);
        UserMessage message6 = this.uow.getUserMessageRepository().create(user1, user2, text[5]);

        assertEquals(user1.getId(), message1.getSender().getId());
        assertEquals(user2.getId(), message2.getReceiver().getId());
        assertEquals(user2.getId(), message4.getSender().getId());
        assertEquals(user1.getId(), message5.getReceiver().getId());
        assertEquals("Dann komm ich den Lambo morgen abholen ;-)", message6.getMessage());
        assertEquals("Stark", message5.getSender().getLastName());
        assertEquals("Stark", message3.getReceiver().getLastName());
        UserMessage msg = Iterables.singleOrDefault(uow.getUserMessageRepository().getByText(user2.getId(), "kälter"),
                null);
        if (msg == null) {
            fail();
        } else {
            assertEquals("Nachts ist es kälter als draußen!", msg.getMessage());
        }
    }

}
