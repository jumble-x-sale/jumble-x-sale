package com.jumble_x_sale.models;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Optional;

/**
 * Unit test for class {@link CompletedSecretSanta CompletedSecretSanta}.
 */
public class CompletedSecretSantaTest extends JumbleTest {

    @Test
    public void testCompletedSecretSanta() {
        User member1 = this.uow.getUserRepository().create(UserType.MEMBER, "Max", "Mustermann",
                "max.mustermann@web.de", "123", new IdCard("lhm310k1g398092282302096"), true);
        User member2 = this.uow.getUserRepository().create(UserType.MEMBER, "Hans", "Zimmer",
                "hans.zimmer@web.de", "123", new IdCard("lhm310k1g398092282302096"), true);

        SecretSantaOffer match1 = this.uow.getSecretSantaOfferRepository().create(member1, "Krimi",
                "Hans im Glück", "Gutes Buch", new String[0], new byte[0]);
        SecretSantaOffer match2 = this.uow.getSecretSantaOfferRepository().create(member2, "Krimi",
                "Hans im Glück 2", "Auch ein gutes Buch", new String[0], new byte[0]);
        CompletedSecretSanta test1 =
                this.uow.getCompletedSecretSantaRepository().create(match1, match2);

        Optional<CompletedSecretSanta> completedSecretSanta =
                this.uow.getCompletedSecretSantaRepository().getByMember(member1).findFirst();

        assertEquals("Mustermann", test1.getMatch1().getCreator().getLastName());
        assertEquals("Krimi", test1.getMatch2().getCategory());
        assertEquals("Krimi", test1.getMatch2().getCategory());
        if (completedSecretSanta == null) {
            fail();
        } else {
            assertEquals("Mustermann",
                    completedSecretSanta.get().getMatch1().getCreator().getLastName());
        }
    }

}
