# Jumble-X-Sale

Jumble-X-Sale is an electronic platform that offers a jumble sale for individuals and professionals and acting as secret santa between individuals.

***The release version of the project is tagged as "Release-2.0" and can be found at [https://gitlab.com/jumble-x-sale/jumble-x-sale/tags/Release-1.0](https://gitlab.com/jumble-x-sale/jumble-x-sale/tags/Release-1.0)***

## Documentation

The following links lead to the different types of documentation (in german):
* JavaDoc: [doc/JavaDoc/](doc/JavaDoc/)
* General Documentation: [doc/Documentation/Jumble-X-Sale.pdf](doc/Documentation/Jumble-X-Sale.pdf)
* Presentations: [doc/Presentations/](doc/Presentations/)

## Developer-Guides

Git-Repository-URL for Cloning:
* https://gitlab.com/jumble-x-sale/jumble-x-sale.git

Prerequisites for Development:
* [Git](./doc/Dev-Guides/Git.md)
* [Java 1.8](./doc/Dev-Guides/Java.md)
* [Maven 3.6.0](./doc/Dev-Guides/Maven.md)
* [PlantUML](./doc/Dev-Guides/PlantUML.md)
* [UnitOfWork, Repository, Entity](./doc/Dev-Guides/Patterns.md)
* [REST-Server (VS-Code & Maven: Debugging)](./doc/Dev-Guides/REST-Server.md)
* VS-Code Extension "REST Client" for using HTTP-Test-Request files (typically named "Requests.http")

External Online Information:
* Coding Style (Google):
    * https://google.github.io/styleguide/javaguide.html
    * https://github.com/google/styleguide/blob/gh-pages/eclipse-java-google-style.xml
    * Most of the requirements are automatically applied when saving a file in VS-Code:
        * VS-Code-Setting: `"java.format.settings.profile": "./code/java-style.xml"`
        * Style-Description: [./code/java-style.xml](./code/java-style.xml)
* Maven:
    * [Introduction to the Build Lifecycle](http://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)
    * [More than compilation steps (Stack Overflow)](https://stackoverflow.com/questions/13082763/more-than-compilation-steps-in-pom-xml)
* JAX-RS:
    * [Authentication (Stack Overflow)](https://stackoverflow.com/questions/26777083/best-practice-for-rest-token-based-authentication-with-jax-rs-and-jersey)
    * [Jersey REST API Security Example](https://howtodoinjava.com/jersey/jersey-rest-security/)
    * [Securing JAX-RS Endpoints with JWT](https://antoniogoncalves.org/2016/10/03/securing-jax-rs-endpoints-with-jwt/)
* JPA:
    * Composite primary keys:
        * [Standardized schema generation and data loading with JPA 2.1](https://thoughts-on-java.org/standardized-schema-generation-data-loading-jpa-2-1/)
        * [JPA Primary Key](https://www.objectdb.com/java/jpa/entity/id)
        * [How to map (Stack Overflow)](https://stackoverflow.com/questions/3585034/how-to-map-a-composite-key-with-hibernate)
        * [How to create and handle (Stack Overflow)](https://stackoverflow.com/questions/13032948/how-to-create-and-handle-composite-primary-key-in-jpa)
    * [Embeddable with relationships](https://www.logicbig.com/tutorials/java-ee-tutorial/jpa/embeddable-with-entity-relation.html)
    * [JSON data (Stack Overflow)](https://stackoverflow.com/questions/25738569/jpa-map-json-column-to-java-object)
    * [UUID (Stack Overflow)](https://stackoverflow.com/questions/43606386/store-uuid-as-binary-in-mysql-database-with-hibernate-jpa-for-improving-the-perf)
* JSON Web Token:
    * [Java JWT](https://github.com/jwtk/jjwt)
    * [Best HTTP Authorization header type for JWT](https://stackoverflow.com/questions/33265812/best-http-authorization-header-type-for-jwt)
* [Reflecting generic parameter types](https://www.artima.com/weblogs/viewpost.jsp?thread=208860)
* [How to generate a random alpha-numeric string](https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string/41156#41156)
* Concurrency:
    * Maps:
        * [HashMap Vs. ConcurrentHashMap Vs. SynchronizedMap – How a HashMap can be Synchronized in Java](https://crunchify.com/hashmap-vs-concurrenthashmap-vs-synchronizedmap-how-a-hashmap-can-be-synchronized-in-java/)
        * [Java 7: HashMap vs ConcurrentHashMap](https://dzone.com/articles/java-7-hashmap-vs)
* [REST-Server](./doc/Dev-Guides/REST-Server.md)
* Deployment:
    * [Azure with GitLab-Container-Registry](https://stackoverflow.com/questions/56109448/azure-app-service-not-picking-up-gitlab-container-registry-configuration-as-priv)
